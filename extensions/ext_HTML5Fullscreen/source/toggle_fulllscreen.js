function ToggleFullScreen() {
	var fs = false;
    if ((document.fullScreenElement && document.fullScreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
        if (document.documentElement.requestFullScreen) {
            document.documentElement.requestFullScreen();
			fs = true;
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
			fs = true;
        } else if (document.documentElement.webkitRequestFullScreen) {
            document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
			fs = true;
        }
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
			fs = false;
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
			fs = false;
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
			fs = false;
        }
    }
	return fs.toString();
}