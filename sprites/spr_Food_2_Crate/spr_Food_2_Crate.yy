{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 31,
  "bbox_top": 16,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"96e3bd90-867a-43a0-bbfe-90e67b751acb","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"96e3bd90-867a-43a0-bbfe-90e67b751acb","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"96e3bd90-867a-43a0-bbfe-90e67b751acb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4d53b84e-4c0d-46c8-ae54-ccdf29545cc5","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4d53b84e-4c0d-46c8-ae54-ccdf29545cc5","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"4d53b84e-4c0d-46c8-ae54-ccdf29545cc5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e0249f1b-dc15-4eaf-b998-d300b6525eb8","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e0249f1b-dc15-4eaf-b998-d300b6525eb8","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"e0249f1b-dc15-4eaf-b998-d300b6525eb8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0e775790-9e65-493e-a53a-42adde830e4d","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0e775790-9e65-493e-a53a-42adde830e4d","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"0e775790-9e65-493e-a53a-42adde830e4d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e8a0df00-9c57-434e-b0cd-69f544171a37","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e8a0df00-9c57-434e-b0cd-69f544171a37","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"e8a0df00-9c57-434e-b0cd-69f544171a37","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"345d31a2-317b-4d00-9098-818b7fb108ff","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"345d31a2-317b-4d00-9098-818b7fb108ff","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"345d31a2-317b-4d00-9098-818b7fb108ff","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b1975007-931e-4238-924e-5149eb432886","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b1975007-931e-4238-924e-5149eb432886","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"b1975007-931e-4238-924e-5149eb432886","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"928a1036-e495-4718-bc68-227bcacd133a","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"928a1036-e495-4718-bc68-227bcacd133a","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"928a1036-e495-4718-bc68-227bcacd133a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"24ae01b5-fe82-4c82-8d0e-248f95daf6e0","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"24ae01b5-fe82-4c82-8d0e-248f95daf6e0","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"24ae01b5-fe82-4c82-8d0e-248f95daf6e0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2a462fba-818f-4b9a-8e5e-9ca8e7a5dcfe","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2a462fba-818f-4b9a-8e5e-9ca8e7a5dcfe","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"2a462fba-818f-4b9a-8e5e-9ca8e7a5dcfe","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"35f1ef37-123d-4085-b4fc-53ffba62faa7","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"35f1ef37-123d-4085-b4fc-53ffba62faa7","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"35f1ef37-123d-4085-b4fc-53ffba62faa7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"775253f9-9e42-4679-b1e0-5c7c61ef37b9","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"775253f9-9e42-4679-b1e0-5c7c61ef37b9","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"775253f9-9e42-4679-b1e0-5c7c61ef37b9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"33da83d0-1ef9-486a-bd92-497da934d9c1","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"33da83d0-1ef9-486a-bd92-497da934d9c1","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"33da83d0-1ef9-486a-bd92-497da934d9c1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dd003334-b648-452c-bebd-0aff7406de17","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dd003334-b648-452c-bebd-0aff7406de17","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"dd003334-b648-452c-bebd-0aff7406de17","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5136a8e6-00a1-455d-8d0d-379e2b68c491","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5136a8e6-00a1-455d-8d0d-379e2b68c491","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"5136a8e6-00a1-455d-8d0d-379e2b68c491","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"06acfd8b-7587-4125-9094-fee80c13d249","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"06acfd8b-7587-4125-9094-fee80c13d249","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"06acfd8b-7587-4125-9094-fee80c13d249","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"97863158-6083-4160-958e-07529c39de72","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"97863158-6083-4160-958e-07529c39de72","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"97863158-6083-4160-958e-07529c39de72","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"415dfaa1-b8b5-4e9b-bfd7-7a717fd08c8b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"415dfaa1-b8b5-4e9b-bfd7-7a717fd08c8b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"415dfaa1-b8b5-4e9b-bfd7-7a717fd08c8b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1bd073ea-1cd1-47b5-a1d6-e870144daa2f","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1bd073ea-1cd1-47b5-a1d6-e870144daa2f","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"1bd073ea-1cd1-47b5-a1d6-e870144daa2f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"00979e74-6065-4f7d-8060-5176dc4abd25","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"00979e74-6065-4f7d-8060-5176dc4abd25","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"00979e74-6065-4f7d-8060-5176dc4abd25","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1f13b08e-d736-4035-8590-1b1311f89d63","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1f13b08e-d736-4035-8590-1b1311f89d63","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"1f13b08e-d736-4035-8590-1b1311f89d63","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"06bfb5e3-0498-4403-87d2-0ea264998790","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"06bfb5e3-0498-4403-87d2-0ea264998790","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"06bfb5e3-0498-4403-87d2-0ea264998790","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4229e954-48b7-4aba-9882-523cb4fd7cc6","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4229e954-48b7-4aba-9882-523cb4fd7cc6","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"4229e954-48b7-4aba-9882-523cb4fd7cc6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f3f014a2-5209-40ff-92c5-e8ce76438959","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f3f014a2-5209-40ff-92c5-e8ce76438959","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"f3f014a2-5209-40ff-92c5-e8ce76438959","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cf66e74b-b438-41c5-8099-0a1a52cfd24c","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cf66e74b-b438-41c5-8099-0a1a52cfd24c","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"cf66e74b-b438-41c5-8099-0a1a52cfd24c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a7c3eac7-de36-4236-b417-4f7b515db523","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a7c3eac7-de36-4236-b417-4f7b515db523","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"a7c3eac7-de36-4236-b417-4f7b515db523","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cc9f1294-f6e0-46b4-ba9c-b118f95afbc5","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cc9f1294-f6e0-46b4-ba9c-b118f95afbc5","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"cc9f1294-f6e0-46b4-ba9c-b118f95afbc5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0fd7f763-0c34-4a0c-bf95-ec2fc96f040a","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0fd7f763-0c34-4a0c-bf95-ec2fc96f040a","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"0fd7f763-0c34-4a0c-bf95-ec2fc96f040a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"53d55d70-a887-4f70-a420-e4d5d19e6d64","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"53d55d70-a887-4f70-a420-e4d5d19e6d64","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"53d55d70-a887-4f70-a420-e4d5d19e6d64","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"29599884-0cac-4e00-b70e-eec880e4111b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"29599884-0cac-4e00-b70e-eec880e4111b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"29599884-0cac-4e00-b70e-eec880e4111b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"946c3aac-53be-42bd-be82-a5df32e65cf8","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"946c3aac-53be-42bd-be82-a5df32e65cf8","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"946c3aac-53be-42bd-be82-a5df32e65cf8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"289a2a0e-7d37-4265-8025-1fdc4384c254","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"289a2a0e-7d37-4265-8025-1fdc4384c254","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"LayerId":{"name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","name":"289a2a0e-7d37-4265-8025-1fdc4384c254","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 32.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"964696dd-7e35-4f93-8771-2e53d0c71e67","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"96e3bd90-867a-43a0-bbfe-90e67b751acb","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f97ec6af-9910-4a47-a0da-66e4f59b49ad","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4d53b84e-4c0d-46c8-ae54-ccdf29545cc5","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5332183c-3c62-4614-ae92-66301f9b3eef","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e0249f1b-dc15-4eaf-b998-d300b6525eb8","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6b221f36-8dfa-4631-9acf-847561b0c7c3","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0e775790-9e65-493e-a53a-42adde830e4d","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d063ba8b-ec49-4886-8c88-e0584245b2f0","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e8a0df00-9c57-434e-b0cd-69f544171a37","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1d80c93f-1f2d-4d71-94fb-b906174079cc","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"345d31a2-317b-4d00-9098-818b7fb108ff","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8bad8df3-f479-4115-8d0e-f7bec1c4cd75","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b1975007-931e-4238-924e-5149eb432886","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c6b6f948-6130-48e1-a8f7-9d1038574224","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"928a1036-e495-4718-bc68-227bcacd133a","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"84390d4c-76eb-4600-8406-eb6bdee129d0","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"24ae01b5-fe82-4c82-8d0e-248f95daf6e0","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1e287406-3fbe-47db-b37d-179f4fe0b494","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2a462fba-818f-4b9a-8e5e-9ca8e7a5dcfe","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"80752d1a-af5f-49a9-a50b-045477d190a0","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"35f1ef37-123d-4085-b4fc-53ffba62faa7","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3256cb72-ffee-483a-8d6c-1777c6bc1054","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"775253f9-9e42-4679-b1e0-5c7c61ef37b9","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4b28104f-2c94-4dd0-bb66-139d8444ae95","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"33da83d0-1ef9-486a-bd92-497da934d9c1","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f0a982c3-7e3d-4915-9178-853e0a03baaa","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dd003334-b648-452c-bebd-0aff7406de17","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b05f4710-b62b-44fd-b34c-3368281b679f","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5136a8e6-00a1-455d-8d0d-379e2b68c491","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d07a3212-4c6e-4ae3-ada3-1027552ace36","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"06acfd8b-7587-4125-9094-fee80c13d249","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"03f5914b-752e-4ad0-aa5d-c904ba3b1011","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"97863158-6083-4160-958e-07529c39de72","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"db430308-41ec-4b19-bbdf-134487bab82e","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"415dfaa1-b8b5-4e9b-bfd7-7a717fd08c8b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"53aacb4e-17da-44a0-9c7b-27c98443509c","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1bd073ea-1cd1-47b5-a1d6-e870144daa2f","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f92fd728-d042-4e99-b731-073687d26a4e","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"00979e74-6065-4f7d-8060-5176dc4abd25","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9f5a8810-d926-4156-98c3-339d2ac2156f","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1f13b08e-d736-4035-8590-1b1311f89d63","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bc9d5f05-2758-4977-8ffe-78e17eae3e61","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"06bfb5e3-0498-4403-87d2-0ea264998790","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5951a7e7-ad0f-422c-bcdb-4d347a139b88","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4229e954-48b7-4aba-9882-523cb4fd7cc6","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"961e5894-c5c6-41b7-9cfa-58c126b6ab87","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f3f014a2-5209-40ff-92c5-e8ce76438959","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7d3c1d33-e7e0-4012-9603-1cdf7e6a45cd","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cf66e74b-b438-41c5-8099-0a1a52cfd24c","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"666eac36-6576-472c-82ff-c2b3d4de08ac","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a7c3eac7-de36-4236-b417-4f7b515db523","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2c3d2c1d-97e3-4566-bb58-e3e783ebe9b9","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cc9f1294-f6e0-46b4-ba9c-b118f95afbc5","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"73bf8e70-ba45-4d90-9108-c016f71d0d4a","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0fd7f763-0c34-4a0c-bf95-ec2fc96f040a","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e001d47f-06c7-4b0b-b5a3-bdf675df08b4","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"53d55d70-a887-4f70-a420-e4d5d19e6d64","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"eaa70603-1525-440d-997e-7a96d3dea1fd","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"29599884-0cac-4e00-b70e-eec880e4111b","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c8d9ae20-42de-4262-805b-dce9a35349dc","Key":30.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"946c3aac-53be-42bd-be82-a5df32e65cf8","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d59b5932-8d82-4ca3-a397-abbd82434f8a","Key":31.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"289a2a0e-7d37-4265-8025-1fdc4384c254","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 32,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Food_2_Crate","path":"sprites/spr_Food_2_Crate/spr_Food_2_Crate.yy",},
    "resourceVersion": "1.3",
    "name": "spr_Food_2_Crate",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"cfd004a4-22bf-4264-ac9a-fb94e8204b0b","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Trees and Crops",
    "path": "folders/Sprites/Trees and Crops.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Food_2_Crate",
  "tags": [],
  "resourceType": "GMSprite",
}