{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 31,
  "bbox_top": 0,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"9dd64221-2993-4c3c-ac2b-d369ee800460","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9dd64221-2993-4c3c-ac2b-d369ee800460","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"9dd64221-2993-4c3c-ac2b-d369ee800460","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8949f1dd-91f2-4ef9-97fa-d29555d782ef","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8949f1dd-91f2-4ef9-97fa-d29555d782ef","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"8949f1dd-91f2-4ef9-97fa-d29555d782ef","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4340c963-d9d0-477b-a129-89892e9a755e","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4340c963-d9d0-477b-a129-89892e9a755e","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"4340c963-d9d0-477b-a129-89892e9a755e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6d225a95-4dab-43f3-a7d5-965cb54c4c89","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6d225a95-4dab-43f3-a7d5-965cb54c4c89","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"6d225a95-4dab-43f3-a7d5-965cb54c4c89","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f28f0973-559c-4fe3-a360-62829a6f06b5","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f28f0973-559c-4fe3-a360-62829a6f06b5","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"f28f0973-559c-4fe3-a360-62829a6f06b5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f42ba509-a13a-4e99-8fb5-f2c6bbd68d7f","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f42ba509-a13a-4e99-8fb5-f2c6bbd68d7f","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"f42ba509-a13a-4e99-8fb5-f2c6bbd68d7f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7a458c9d-322b-40d5-b600-33ea3ad58c2e","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7a458c9d-322b-40d5-b600-33ea3ad58c2e","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"7a458c9d-322b-40d5-b600-33ea3ad58c2e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cc1e2abb-f5f4-4ab4-9a81-1f538c90fd98","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cc1e2abb-f5f4-4ab4-9a81-1f538c90fd98","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"cc1e2abb-f5f4-4ab4-9a81-1f538c90fd98","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cc60b220-45db-4565-ac20-e9e1a6613962","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cc60b220-45db-4565-ac20-e9e1a6613962","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"cc60b220-45db-4565-ac20-e9e1a6613962","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e9305a4f-bf62-4070-98f6-665b6347d3eb","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e9305a4f-bf62-4070-98f6-665b6347d3eb","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"e9305a4f-bf62-4070-98f6-665b6347d3eb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2cb54229-1ffa-4c77-8b1b-93a2c10cd62f","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2cb54229-1ffa-4c77-8b1b-93a2c10cd62f","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"2cb54229-1ffa-4c77-8b1b-93a2c10cd62f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"69bd0083-be46-4f0d-b536-c8ad45e258be","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"69bd0083-be46-4f0d-b536-c8ad45e258be","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"69bd0083-be46-4f0d-b536-c8ad45e258be","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"351c9fb2-a461-464a-9284-99ecc083a6bb","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"351c9fb2-a461-464a-9284-99ecc083a6bb","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"351c9fb2-a461-464a-9284-99ecc083a6bb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"39f89b18-9d22-48ee-9554-4b9924fa1128","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"39f89b18-9d22-48ee-9554-4b9924fa1128","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"39f89b18-9d22-48ee-9554-4b9924fa1128","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"88b45460-998b-44e0-abcf-2b4a3a90fe82","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"88b45460-998b-44e0-abcf-2b4a3a90fe82","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"88b45460-998b-44e0-abcf-2b4a3a90fe82","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"39b52fc8-ea1a-421e-a0a4-00ef639c92fd","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"39b52fc8-ea1a-421e-a0a4-00ef639c92fd","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"39b52fc8-ea1a-421e-a0a4-00ef639c92fd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6b9b6848-6e3a-4eea-9d9a-4c495dd629a0","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6b9b6848-6e3a-4eea-9d9a-4c495dd629a0","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"6b9b6848-6e3a-4eea-9d9a-4c495dd629a0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a23ea5a5-24de-4a58-a9a1-e3d068e6e59a","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a23ea5a5-24de-4a58-a9a1-e3d068e6e59a","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"a23ea5a5-24de-4a58-a9a1-e3d068e6e59a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e66e5df1-4bf5-4f04-a1f6-5eb60d8359e9","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e66e5df1-4bf5-4f04-a1f6-5eb60d8359e9","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"e66e5df1-4bf5-4f04-a1f6-5eb60d8359e9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8fc59067-9714-425b-a1ef-540a480b6b0d","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8fc59067-9714-425b-a1ef-540a480b6b0d","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"8fc59067-9714-425b-a1ef-540a480b6b0d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dabd258e-0684-4c9f-bc5a-7469611414f1","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dabd258e-0684-4c9f-bc5a-7469611414f1","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"dabd258e-0684-4c9f-bc5a-7469611414f1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b3036b43-768c-4ce9-ba88-61fbeb5e5ebd","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b3036b43-768c-4ce9-ba88-61fbeb5e5ebd","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"b3036b43-768c-4ce9-ba88-61fbeb5e5ebd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0e2dafd7-1154-42f7-9d73-38e01d7f4cbd","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0e2dafd7-1154-42f7-9d73-38e01d7f4cbd","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"0e2dafd7-1154-42f7-9d73-38e01d7f4cbd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9962422e-0802-4e8f-9cb9-097b6cebc386","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9962422e-0802-4e8f-9cb9-097b6cebc386","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"9962422e-0802-4e8f-9cb9-097b6cebc386","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"43f156b8-8d52-4a9a-9fbe-73f2c80ac372","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"43f156b8-8d52-4a9a-9fbe-73f2c80ac372","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"43f156b8-8d52-4a9a-9fbe-73f2c80ac372","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1ea88a65-3ce4-41e5-968c-178efc8d8dba","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1ea88a65-3ce4-41e5-968c-178efc8d8dba","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"1ea88a65-3ce4-41e5-968c-178efc8d8dba","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f21b3770-644b-49ab-83a0-92c6253a5d01","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f21b3770-644b-49ab-83a0-92c6253a5d01","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"f21b3770-644b-49ab-83a0-92c6253a5d01","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1c17116c-0ea1-4e3d-ab0e-002be4b814a0","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1c17116c-0ea1-4e3d-ab0e-002be4b814a0","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"1c17116c-0ea1-4e3d-ab0e-002be4b814a0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"978ce908-e3e5-4b91-b22f-4e3f9efcc920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"978ce908-e3e5-4b91-b22f-4e3f9efcc920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"978ce908-e3e5-4b91-b22f-4e3f9efcc920","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"29442db2-8a86-4988-83d7-edb702892c98","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"29442db2-8a86-4988-83d7-edb702892c98","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"29442db2-8a86-4988-83d7-edb702892c98","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a6e9ed70-e5c4-4264-8742-a84c43e59506","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a6e9ed70-e5c4-4264-8742-a84c43e59506","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"a6e9ed70-e5c4-4264-8742-a84c43e59506","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"160530c0-6792-4453-9dd5-5ffaa3b489f1","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"160530c0-6792-4453-9dd5-5ffaa3b489f1","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"LayerId":{"name":"aa8cf621-3d00-4af2-9ed2-670016173920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","name":"160530c0-6792-4453-9dd5-5ffaa3b489f1","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 32.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"e13b1997-effb-4791-81db-b64cb8b49bbe","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9dd64221-2993-4c3c-ac2b-d369ee800460","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d4da5462-3523-4b2e-996b-e743d572a5d8","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8949f1dd-91f2-4ef9-97fa-d29555d782ef","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c9aaeb69-ee81-4ff6-8e61-ec3e19ec8e12","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4340c963-d9d0-477b-a129-89892e9a755e","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"611f9d8d-97b6-4296-b5fc-09b7c19ea7fd","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6d225a95-4dab-43f3-a7d5-965cb54c4c89","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7b507e0d-3244-471c-9398-620c564c28c9","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f28f0973-559c-4fe3-a360-62829a6f06b5","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4fd0372e-9186-440a-9ba3-33d815afda62","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f42ba509-a13a-4e99-8fb5-f2c6bbd68d7f","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fcb6d681-452e-4c5c-ba6d-42bd99ce307f","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7a458c9d-322b-40d5-b600-33ea3ad58c2e","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ea7320c7-af81-4bc5-9ae3-57091f898b7f","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cc1e2abb-f5f4-4ab4-9a81-1f538c90fd98","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ae20384f-02e9-4d36-a035-40acdf89b6af","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cc60b220-45db-4565-ac20-e9e1a6613962","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c1405704-3d70-4436-925a-7c17fd9d0e0a","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e9305a4f-bf62-4070-98f6-665b6347d3eb","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"001ee3fd-a18a-4542-842c-d53fabaa41af","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2cb54229-1ffa-4c77-8b1b-93a2c10cd62f","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4d850567-fff1-40b5-a148-e045831193c1","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"69bd0083-be46-4f0d-b536-c8ad45e258be","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c55d858f-820e-4458-8ca0-3138e99116f8","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"351c9fb2-a461-464a-9284-99ecc083a6bb","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3aad4bc1-391a-47e9-9866-4d397574b9e3","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"39f89b18-9d22-48ee-9554-4b9924fa1128","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"229ea6c7-c950-4dd5-b7be-a450e19c5a43","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"88b45460-998b-44e0-abcf-2b4a3a90fe82","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f9aa9f46-8218-46fa-8d4d-0969bc5fc0e0","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"39b52fc8-ea1a-421e-a0a4-00ef639c92fd","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b873a167-bfc1-4736-bfcb-82ba2b2d1d0f","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6b9b6848-6e3a-4eea-9d9a-4c495dd629a0","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a1c5ac18-5cf8-4c79-8044-a7bf4fe27bc8","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a23ea5a5-24de-4a58-a9a1-e3d068e6e59a","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4811feae-d931-436c-8337-ae5c0eaf1ff8","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e66e5df1-4bf5-4f04-a1f6-5eb60d8359e9","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2b70f98a-2c1f-4756-8e9d-b5146896e4b9","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8fc59067-9714-425b-a1ef-540a480b6b0d","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6b629533-cd27-4be6-89cd-5e18b8b03fa8","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dabd258e-0684-4c9f-bc5a-7469611414f1","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3abe1a5c-fbf1-4137-80db-6b366ef59961","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b3036b43-768c-4ce9-ba88-61fbeb5e5ebd","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f16fda2d-64dc-40ee-a4ca-9ee3189733b5","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0e2dafd7-1154-42f7-9d73-38e01d7f4cbd","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c7547365-ccd7-4900-9dee-a4a0c5182abc","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9962422e-0802-4e8f-9cb9-097b6cebc386","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7d4e1637-d643-4a1c-8150-c644d7392944","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"43f156b8-8d52-4a9a-9fbe-73f2c80ac372","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"750e323b-8cfa-48a7-b22d-1d13d3d40695","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1ea88a65-3ce4-41e5-968c-178efc8d8dba","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7c2f459b-b104-4277-b391-3d9903baa6e2","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f21b3770-644b-49ab-83a0-92c6253a5d01","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f75e90c3-852d-4813-8093-060dcb098ef5","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1c17116c-0ea1-4e3d-ab0e-002be4b814a0","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"514d809f-f4d7-4c06-aa8b-9711dec6fe75","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"978ce908-e3e5-4b91-b22f-4e3f9efcc920","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dc175296-664b-4791-bf7c-b57d01b792af","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"29442db2-8a86-4988-83d7-edb702892c98","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"36b5ee33-f0cc-4cd7-81fa-9b3dd296fadc","Key":30.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a6e9ed70-e5c4-4264-8742-a84c43e59506","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f17f0891-5738-4f85-a94f-cac70457050a","Key":31.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"160530c0-6792-4453-9dd5-5ffaa3b489f1","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 32,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Food_5_Crate","path":"sprites/spr_Food_5_Crate/spr_Food_5_Crate.yy",},
    "resourceVersion": "1.3",
    "name": "spr_Food_5_Crate",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"aa8cf621-3d00-4af2-9ed2-670016173920","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Trees and Crops",
    "path": "folders/Sprites/Trees and Crops.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Food_5_Crate",
  "tags": [],
  "resourceType": "GMSprite",
}