{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 31,
  "bbox_top": 16,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"70aafe38-7cf7-4d3a-a513-dadade2b881d","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"70aafe38-7cf7-4d3a-a513-dadade2b881d","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"70aafe38-7cf7-4d3a-a513-dadade2b881d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a6f85870-c89c-4676-adaa-af6df0dafa90","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a6f85870-c89c-4676-adaa-af6df0dafa90","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"a6f85870-c89c-4676-adaa-af6df0dafa90","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"24ef8465-d87c-4bfe-a45d-600aea8b4587","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"24ef8465-d87c-4bfe-a45d-600aea8b4587","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"24ef8465-d87c-4bfe-a45d-600aea8b4587","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0ed5f66e-c212-4b8c-8c7c-1d0569e27e3d","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0ed5f66e-c212-4b8c-8c7c-1d0569e27e3d","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"0ed5f66e-c212-4b8c-8c7c-1d0569e27e3d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c7f71572-c3a0-4d24-b2a5-c5931b4d235e","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c7f71572-c3a0-4d24-b2a5-c5931b4d235e","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"c7f71572-c3a0-4d24-b2a5-c5931b4d235e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f8ddb3db-215c-4059-8392-c4de6dc9e1f9","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f8ddb3db-215c-4059-8392-c4de6dc9e1f9","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"f8ddb3db-215c-4059-8392-c4de6dc9e1f9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ec71bca3-6f4c-4594-a68f-30ddffaf0cd3","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ec71bca3-6f4c-4594-a68f-30ddffaf0cd3","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"ec71bca3-6f4c-4594-a68f-30ddffaf0cd3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"792f0cec-4756-47e2-b30d-ec6343bd2ece","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"792f0cec-4756-47e2-b30d-ec6343bd2ece","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"792f0cec-4756-47e2-b30d-ec6343bd2ece","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"36138d38-70df-45fb-a390-29a7472d4602","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"36138d38-70df-45fb-a390-29a7472d4602","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"36138d38-70df-45fb-a390-29a7472d4602","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"74930a2c-947b-4950-bb9f-7f51cb9f8f25","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"74930a2c-947b-4950-bb9f-7f51cb9f8f25","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"74930a2c-947b-4950-bb9f-7f51cb9f8f25","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ab0f5640-8778-4d5f-82c2-4be29cbf336b","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ab0f5640-8778-4d5f-82c2-4be29cbf336b","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"ab0f5640-8778-4d5f-82c2-4be29cbf336b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3206e81e-16b6-4f71-9e4f-b474ee20dbba","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3206e81e-16b6-4f71-9e4f-b474ee20dbba","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"3206e81e-16b6-4f71-9e4f-b474ee20dbba","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"63a1da51-4577-4442-ad4f-f4a2333af967","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"63a1da51-4577-4442-ad4f-f4a2333af967","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"63a1da51-4577-4442-ad4f-f4a2333af967","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8fb742fe-eccc-4eac-9a30-640a6e4ea536","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8fb742fe-eccc-4eac-9a30-640a6e4ea536","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"8fb742fe-eccc-4eac-9a30-640a6e4ea536","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3e7a260d-e4a8-4b7c-ac65-4f28a39e5ac8","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3e7a260d-e4a8-4b7c-ac65-4f28a39e5ac8","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"3e7a260d-e4a8-4b7c-ac65-4f28a39e5ac8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8b403699-216d-4536-99f1-008a0c89b08e","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8b403699-216d-4536-99f1-008a0c89b08e","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"8b403699-216d-4536-99f1-008a0c89b08e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b9be240b-b440-4a1b-8ab6-5fc2435a3525","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b9be240b-b440-4a1b-8ab6-5fc2435a3525","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"b9be240b-b440-4a1b-8ab6-5fc2435a3525","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"23e7623e-c8f7-43c8-88dc-45f3bfd92490","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"23e7623e-c8f7-43c8-88dc-45f3bfd92490","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"23e7623e-c8f7-43c8-88dc-45f3bfd92490","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9f2c2399-0e6b-4cf7-9ec8-df327db3e4fb","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9f2c2399-0e6b-4cf7-9ec8-df327db3e4fb","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"9f2c2399-0e6b-4cf7-9ec8-df327db3e4fb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"057d68a6-6556-43e6-ac50-7358e735cdbc","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"057d68a6-6556-43e6-ac50-7358e735cdbc","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"057d68a6-6556-43e6-ac50-7358e735cdbc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e7e8bf22-e696-4fa9-a8ae-994d7448739b","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e7e8bf22-e696-4fa9-a8ae-994d7448739b","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"e7e8bf22-e696-4fa9-a8ae-994d7448739b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"be9a2972-fef6-4eb0-99d7-3dcdd8b296ff","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"be9a2972-fef6-4eb0-99d7-3dcdd8b296ff","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"be9a2972-fef6-4eb0-99d7-3dcdd8b296ff","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0732f275-c4ec-4951-9b57-ba297b4bfa2d","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0732f275-c4ec-4951-9b57-ba297b4bfa2d","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"0732f275-c4ec-4951-9b57-ba297b4bfa2d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5f8e8ff5-f5e3-46ec-a0df-724835dcc2e6","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5f8e8ff5-f5e3-46ec-a0df-724835dcc2e6","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"5f8e8ff5-f5e3-46ec-a0df-724835dcc2e6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f99babc7-8094-48d4-9e6a-6b579a6092f5","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f99babc7-8094-48d4-9e6a-6b579a6092f5","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"f99babc7-8094-48d4-9e6a-6b579a6092f5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1d9ade75-0add-46a2-a114-6742857b5f02","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1d9ade75-0add-46a2-a114-6742857b5f02","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"1d9ade75-0add-46a2-a114-6742857b5f02","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2dc020e3-57f2-478f-b473-e23eadbf6961","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2dc020e3-57f2-478f-b473-e23eadbf6961","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"2dc020e3-57f2-478f-b473-e23eadbf6961","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"435f8ca4-c8b6-4a1b-a882-0b2992ba8e6c","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"435f8ca4-c8b6-4a1b-a882-0b2992ba8e6c","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"435f8ca4-c8b6-4a1b-a882-0b2992ba8e6c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b9561826-214d-4752-8f88-42f8179cc30b","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b9561826-214d-4752-8f88-42f8179cc30b","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"b9561826-214d-4752-8f88-42f8179cc30b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e9e2f083-53ad-47b7-8398-2509ae631a69","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e9e2f083-53ad-47b7-8398-2509ae631a69","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"e9e2f083-53ad-47b7-8398-2509ae631a69","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cf9d7c10-dbf8-4471-ba43-3a294ce999e7","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cf9d7c10-dbf8-4471-ba43-3a294ce999e7","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"cf9d7c10-dbf8-4471-ba43-3a294ce999e7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f00f5620-5100-4af5-a5cc-b03031c58170","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f00f5620-5100-4af5-a5cc-b03031c58170","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"LayerId":{"name":"83e47749-beb6-4b26-aac4-332337c05ef4","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","name":"f00f5620-5100-4af5-a5cc-b03031c58170","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 32.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"be1c6b39-363b-4b54-9fc1-4f9c0679af61","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"70aafe38-7cf7-4d3a-a513-dadade2b881d","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"80e30c1a-10a7-4b12-be7e-f40a9ce6efaf","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a6f85870-c89c-4676-adaa-af6df0dafa90","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2eedf85f-da84-4f1a-ab19-d4be652020be","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"24ef8465-d87c-4bfe-a45d-600aea8b4587","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d891d64c-f639-43ad-bc16-e73d640bc7fd","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0ed5f66e-c212-4b8c-8c7c-1d0569e27e3d","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ba2b164b-256e-40d6-8f72-ba0b4e84c813","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c7f71572-c3a0-4d24-b2a5-c5931b4d235e","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"459b9444-f2f5-44f7-a93e-8e8e5a3db38b","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f8ddb3db-215c-4059-8392-c4de6dc9e1f9","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a814a20a-1ddb-4141-a776-4fdcfd39d1b9","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ec71bca3-6f4c-4594-a68f-30ddffaf0cd3","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3bf2f153-1f05-4f3d-910f-0e53cc207fc6","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"792f0cec-4756-47e2-b30d-ec6343bd2ece","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"747b40dd-d8f6-424f-b94a-6e959ca29021","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"36138d38-70df-45fb-a390-29a7472d4602","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ab39376f-d0fc-4ecd-be63-7592e4ff1b22","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"74930a2c-947b-4950-bb9f-7f51cb9f8f25","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"33b994d1-90b0-462a-834a-ea5f44f4f5f8","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ab0f5640-8778-4d5f-82c2-4be29cbf336b","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0508683c-f54f-4df0-bf30-f5f872c794a1","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3206e81e-16b6-4f71-9e4f-b474ee20dbba","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0cc82d95-975e-4155-a7d4-cbcef3440c65","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"63a1da51-4577-4442-ad4f-f4a2333af967","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9cb62f83-1e4b-4e75-be28-4472df4db2bf","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8fb742fe-eccc-4eac-9a30-640a6e4ea536","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"64ee829a-4356-436e-952f-742e8ce71fbc","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3e7a260d-e4a8-4b7c-ac65-4f28a39e5ac8","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cdbb9299-ac11-47d4-9377-5282146f5423","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8b403699-216d-4536-99f1-008a0c89b08e","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"69cbcbf1-78ff-4f9a-b9e2-3882e32c8fae","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b9be240b-b440-4a1b-8ab6-5fc2435a3525","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c1f266f2-343d-444e-9a4c-bff80224d9af","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"23e7623e-c8f7-43c8-88dc-45f3bfd92490","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b2595677-5a5c-4bf7-a20c-ba82da88da51","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9f2c2399-0e6b-4cf7-9ec8-df327db3e4fb","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"68274ebf-55f8-4d31-9a55-366dac20e5bb","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"057d68a6-6556-43e6-ac50-7358e735cdbc","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"90bf99b9-c98c-441f-a616-8f9af5da27a5","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e7e8bf22-e696-4fa9-a8ae-994d7448739b","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0f5b3666-ae3c-4041-9e92-cd4f0b808c5e","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"be9a2972-fef6-4eb0-99d7-3dcdd8b296ff","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"185a2206-ff43-46e9-9a2e-88c59fe91fbd","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0732f275-c4ec-4951-9b57-ba297b4bfa2d","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4de274e2-f9e0-4f6a-adfb-5f5072e15684","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5f8e8ff5-f5e3-46ec-a0df-724835dcc2e6","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"89daaecc-24aa-470f-ba64-cd1ce2a493fa","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f99babc7-8094-48d4-9e6a-6b579a6092f5","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5d8386a9-9faf-493c-9f9a-8bfcb966915a","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1d9ade75-0add-46a2-a114-6742857b5f02","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2797874a-2c7c-44ad-8b50-164b0031d7de","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2dc020e3-57f2-478f-b473-e23eadbf6961","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7b6579e0-e50d-4951-a718-c5d3e24cf45d","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"435f8ca4-c8b6-4a1b-a882-0b2992ba8e6c","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d4690b3f-7988-4932-b3c5-41317690e5bc","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b9561826-214d-4752-8f88-42f8179cc30b","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7bafac36-6229-4db4-bc8f-d7bfb8a2ca16","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e9e2f083-53ad-47b7-8398-2509ae631a69","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ae688bd0-60d3-4077-80f0-e70a30eca855","Key":30.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cf9d7c10-dbf8-4471-ba43-3a294ce999e7","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a32b7840-307e-4705-a84e-5fc468995774","Key":31.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f00f5620-5100-4af5-a5cc-b03031c58170","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 32,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Food_7_Crate","path":"sprites/spr_Food_7_Crate/spr_Food_7_Crate.yy",},
    "resourceVersion": "1.3",
    "name": "spr_Food_7_Crate",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"83e47749-beb6-4b26-aac4-332337c05ef4","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Trees and Crops",
    "path": "folders/Sprites/Trees and Crops.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Food_7_Crate",
  "tags": [],
  "resourceType": "GMSprite",
}