{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 3,
  "bbox_right": 1148,
  "bbox_top": 45,
  "bbox_bottom": 671,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 1152,
  "height": 672,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"eb5568b1-0b94-4159-bc67-0b03c6e070a9","path":"sprites/slice_Outfit_30_48x48_02/slice_Outfit_30_48x48_02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"eb5568b1-0b94-4159-bc67-0b03c6e070a9","path":"sprites/slice_Outfit_30_48x48_02/slice_Outfit_30_48x48_02.yy",},"LayerId":{"name":"3a661462-cfbc-4229-b6c6-2bb12086b7c1","path":"sprites/slice_Outfit_30_48x48_02/slice_Outfit_30_48x48_02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"slice_Outfit_30_48x48_02","path":"sprites/slice_Outfit_30_48x48_02/slice_Outfit_30_48x48_02.yy",},"resourceVersion":"1.0","name":"eb5568b1-0b94-4159-bc67-0b03c6e070a9","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"slice_Outfit_30_48x48_02","path":"sprites/slice_Outfit_30_48x48_02/slice_Outfit_30_48x48_02.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"19a3ea0c-b5bd-4dd9-a3f7-b72e5f0fd2e6","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"eb5568b1-0b94-4159-bc67-0b03c6e070a9","path":"sprites/slice_Outfit_30_48x48_02/slice_Outfit_30_48x48_02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 576,
    "yorigin": 336,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"slice_Outfit_30_48x48_02","path":"sprites/slice_Outfit_30_48x48_02/slice_Outfit_30_48x48_02.yy",},
    "resourceVersion": "1.3",
    "name": "slice_Outfit_30_48x48_02",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"3a661462-cfbc-4229-b6c6-2bb12086b7c1","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "People",
    "path": "folders/Sprites/People.yy",
  },
  "resourceVersion": "1.0",
  "name": "slice_Outfit_30_48x48_02",
  "tags": [],
  "resourceType": "GMSprite",
}