{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 2,
  "bbox_right": 15,
  "bbox_top": 1,
  "bbox_bottom": 21,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 16,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"8c575ab4-8133-44a6-99b5-fb6f4c4ba7c6","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8c575ab4-8133-44a6-99b5-fb6f4c4ba7c6","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"8c575ab4-8133-44a6-99b5-fb6f4c4ba7c6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8a6c4d4f-f6da-4d4f-8dc9-9891105dc016","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8a6c4d4f-f6da-4d4f-8dc9-9891105dc016","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"8a6c4d4f-f6da-4d4f-8dc9-9891105dc016","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a7630124-61a2-4329-b744-cda0ed6dc60f","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a7630124-61a2-4329-b744-cda0ed6dc60f","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"a7630124-61a2-4329-b744-cda0ed6dc60f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6dd19108-2977-42cf-b6ec-5f580ec0969f","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6dd19108-2977-42cf-b6ec-5f580ec0969f","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"6dd19108-2977-42cf-b6ec-5f580ec0969f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e8a1f802-a839-49bc-b498-5edee69ce51f","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e8a1f802-a839-49bc-b498-5edee69ce51f","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"e8a1f802-a839-49bc-b498-5edee69ce51f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"deb98f84-20f0-43ac-8da5-79b9f86644ba","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"deb98f84-20f0-43ac-8da5-79b9f86644ba","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"deb98f84-20f0-43ac-8da5-79b9f86644ba","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"edbec80c-71f3-45c1-a19f-f3d426022dfd","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"edbec80c-71f3-45c1-a19f-f3d426022dfd","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"edbec80c-71f3-45c1-a19f-f3d426022dfd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3b51624d-1229-460f-a6ba-e294b3ce6061","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3b51624d-1229-460f-a6ba-e294b3ce6061","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"3b51624d-1229-460f-a6ba-e294b3ce6061","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"52c99826-9ea5-4df6-bebc-75f0ba1cbf73","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"52c99826-9ea5-4df6-bebc-75f0ba1cbf73","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"52c99826-9ea5-4df6-bebc-75f0ba1cbf73","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6cf02bd6-9857-44e1-88a7-a723cfbf029c","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6cf02bd6-9857-44e1-88a7-a723cfbf029c","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"6cf02bd6-9857-44e1-88a7-a723cfbf029c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5847eb7d-e25f-4790-b90c-5f0cee74db48","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5847eb7d-e25f-4790-b90c-5f0cee74db48","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"5847eb7d-e25f-4790-b90c-5f0cee74db48","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"90606775-6669-46ca-9340-a651b4d950f8","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"90606775-6669-46ca-9340-a651b4d950f8","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"90606775-6669-46ca-9340-a651b4d950f8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b5b7d27a-f09e-4a77-9d77-73627be425a9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b5b7d27a-f09e-4a77-9d77-73627be425a9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"b5b7d27a-f09e-4a77-9d77-73627be425a9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"78f9a928-2c71-4f5a-a168-6d1e3c051892","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"78f9a928-2c71-4f5a-a168-6d1e3c051892","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"78f9a928-2c71-4f5a-a168-6d1e3c051892","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5d00bcb4-adf3-4b00-836d-77a9314c3af2","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5d00bcb4-adf3-4b00-836d-77a9314c3af2","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"5d00bcb4-adf3-4b00-836d-77a9314c3af2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"41c4d69e-43b1-47aa-a87f-3998ba16decb","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"41c4d69e-43b1-47aa-a87f-3998ba16decb","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"41c4d69e-43b1-47aa-a87f-3998ba16decb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4e43bc14-f883-4ca2-8e36-dfbdabed9f13","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4e43bc14-f883-4ca2-8e36-dfbdabed9f13","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"4e43bc14-f883-4ca2-8e36-dfbdabed9f13","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1808cd60-1a44-4912-b95a-71c5c372d215","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1808cd60-1a44-4912-b95a-71c5c372d215","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"1808cd60-1a44-4912-b95a-71c5c372d215","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3704f727-7f2e-4660-bbd0-45ea1b1eb44a","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3704f727-7f2e-4660-bbd0-45ea1b1eb44a","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"3704f727-7f2e-4660-bbd0-45ea1b1eb44a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7f582739-efaa-4a22-a224-6a17008764be","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7f582739-efaa-4a22-a224-6a17008764be","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"7f582739-efaa-4a22-a224-6a17008764be","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bf293377-5958-4a38-94f5-75e9de000ca0","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bf293377-5958-4a38-94f5-75e9de000ca0","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"bf293377-5958-4a38-94f5-75e9de000ca0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"64304df8-58ae-429a-9f3f-fe2377a75c86","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"64304df8-58ae-429a-9f3f-fe2377a75c86","path":"sprites/spr_Toppings/spr_Toppings.yy",},"LayerId":{"name":"58b2340e-feb6-45e0-8104-07b080a99ac9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","name":"64304df8-58ae-429a-9f3f-fe2377a75c86","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 22.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"b324234c-f0d9-496c-8105-b04ebe9be9b3","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8c575ab4-8133-44a6-99b5-fb6f4c4ba7c6","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3f604fcb-c848-4116-816c-105a38c4b55b","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8a6c4d4f-f6da-4d4f-8dc9-9891105dc016","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1f99c22b-2169-4c79-b1c3-b8ee0210c575","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a7630124-61a2-4329-b744-cda0ed6dc60f","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fd48985c-9559-4050-8433-1cea9262ee64","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6dd19108-2977-42cf-b6ec-5f580ec0969f","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a38e68e8-fa98-4f8f-bb39-a19a47c43dfd","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e8a1f802-a839-49bc-b498-5edee69ce51f","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"660c0648-f331-4d57-ba83-a6c97d28c940","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"deb98f84-20f0-43ac-8da5-79b9f86644ba","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"afa22e90-f3fb-40f8-8e3e-ce8e2c5dfc46","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"edbec80c-71f3-45c1-a19f-f3d426022dfd","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"eaf4adc9-6093-46c9-aaac-76b51d31bcc5","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3b51624d-1229-460f-a6ba-e294b3ce6061","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e266c45e-9ad0-4639-8b12-e881f9dda238","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"52c99826-9ea5-4df6-bebc-75f0ba1cbf73","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"44e3cc3a-0f97-43e9-947a-c5f914ef0e74","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6cf02bd6-9857-44e1-88a7-a723cfbf029c","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"486f0c05-9950-4bc0-8f86-d0e972c11f22","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5847eb7d-e25f-4790-b90c-5f0cee74db48","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ed793ce2-b6d7-4e94-965c-783453b0e24a","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"90606775-6669-46ca-9340-a651b4d950f8","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"caf3052c-ed7d-414d-9445-b534293391cb","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b5b7d27a-f09e-4a77-9d77-73627be425a9","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e390d107-6b85-4cb2-94e7-55694b79f40c","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"78f9a928-2c71-4f5a-a168-6d1e3c051892","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"42d17f79-69ab-4b29-a6d3-176e52b4f72f","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5d00bcb4-adf3-4b00-836d-77a9314c3af2","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f6e653ac-6a60-44b6-b5ab-02bacf019441","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"41c4d69e-43b1-47aa-a87f-3998ba16decb","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"df8d16f3-b1b5-4cb0-91a1-af3e7393945e","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4e43bc14-f883-4ca2-8e36-dfbdabed9f13","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c59af88f-4ebf-4694-a8f7-d33c603363a3","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1808cd60-1a44-4912-b95a-71c5c372d215","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8d5bb0ca-afc4-410a-8c2c-830690f002e0","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3704f727-7f2e-4660-bbd0-45ea1b1eb44a","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0ac4e8f2-05a2-4895-bf0c-31b4fa45924c","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7f582739-efaa-4a22-a224-6a17008764be","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"eb29a4d8-4c34-4c20-a64a-3acb1ebc8969","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bf293377-5958-4a38-94f5-75e9de000ca0","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a806253f-33f5-454a-9b39-b07fb4641a59","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"64304df8-58ae-429a-9f3f-fe2377a75c86","path":"sprites/spr_Toppings/spr_Toppings.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 8,
    "yorigin": 16,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Toppings","path":"sprites/spr_Toppings/spr_Toppings.yy",},
    "resourceVersion": "1.3",
    "name": "spr_Toppings",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"58b2340e-feb6-45e0-8104-07b080a99ac9","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Ice Creams",
    "path": "folders/Sprites/Ice Creams.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Toppings",
  "tags": [],
  "resourceType": "GMSprite",
}