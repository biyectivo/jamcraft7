{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 31,
  "bbox_top": 16,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"547f514a-6a84-45a5-8725-9f9609673a18","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"547f514a-6a84-45a5-8725-9f9609673a18","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"547f514a-6a84-45a5-8725-9f9609673a18","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"be28834d-d004-4038-9f66-38f3ac644db9","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"be28834d-d004-4038-9f66-38f3ac644db9","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"be28834d-d004-4038-9f66-38f3ac644db9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8ef2f3d6-5b6a-427d-a4ca-1ade4d6a55a2","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8ef2f3d6-5b6a-427d-a4ca-1ade4d6a55a2","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"8ef2f3d6-5b6a-427d-a4ca-1ade4d6a55a2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"de2d6c6f-3c02-41c6-91c4-b75664464cf1","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"de2d6c6f-3c02-41c6-91c4-b75664464cf1","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"de2d6c6f-3c02-41c6-91c4-b75664464cf1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bc5f978d-4079-4ab1-ba6f-b67a19f975cd","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bc5f978d-4079-4ab1-ba6f-b67a19f975cd","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"bc5f978d-4079-4ab1-ba6f-b67a19f975cd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e8ecbec0-93cb-4f88-ade9-f1429a2a9d81","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e8ecbec0-93cb-4f88-ade9-f1429a2a9d81","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"e8ecbec0-93cb-4f88-ade9-f1429a2a9d81","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"81ab5043-f720-468d-b4a4-bca8577bd939","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"81ab5043-f720-468d-b4a4-bca8577bd939","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"81ab5043-f720-468d-b4a4-bca8577bd939","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"23889a75-9113-4b47-a81d-e16e3ec33548","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"23889a75-9113-4b47-a81d-e16e3ec33548","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"23889a75-9113-4b47-a81d-e16e3ec33548","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"32396b75-60e2-4fe5-b389-f421e8b1b489","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"32396b75-60e2-4fe5-b389-f421e8b1b489","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"32396b75-60e2-4fe5-b389-f421e8b1b489","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5197c385-0e7e-493f-adcc-144d52553851","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5197c385-0e7e-493f-adcc-144d52553851","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"5197c385-0e7e-493f-adcc-144d52553851","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"56e700d6-4b70-4795-aaf7-313a0af2bbed","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"56e700d6-4b70-4795-aaf7-313a0af2bbed","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"56e700d6-4b70-4795-aaf7-313a0af2bbed","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bb071021-0d08-4532-acf1-e8587d66696e","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bb071021-0d08-4532-acf1-e8587d66696e","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"bb071021-0d08-4532-acf1-e8587d66696e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2ad5333a-cfc6-40ac-a3d6-48baa4e6bde9","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2ad5333a-cfc6-40ac-a3d6-48baa4e6bde9","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"2ad5333a-cfc6-40ac-a3d6-48baa4e6bde9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b21dd218-9458-4d3d-a106-249a395ca74a","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b21dd218-9458-4d3d-a106-249a395ca74a","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"b21dd218-9458-4d3d-a106-249a395ca74a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dca6ccf3-5960-49aa-ac59-70e0193b1aae","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dca6ccf3-5960-49aa-ac59-70e0193b1aae","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"dca6ccf3-5960-49aa-ac59-70e0193b1aae","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e85acb22-554a-47bb-ac77-903876fb2b43","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e85acb22-554a-47bb-ac77-903876fb2b43","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"e85acb22-554a-47bb-ac77-903876fb2b43","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fc12891e-b045-4d13-9660-99a5405deaf6","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fc12891e-b045-4d13-9660-99a5405deaf6","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"fc12891e-b045-4d13-9660-99a5405deaf6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9eb73c77-132c-4af6-b811-33f12d304e2e","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9eb73c77-132c-4af6-b811-33f12d304e2e","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"9eb73c77-132c-4af6-b811-33f12d304e2e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3f442a7e-cb44-4592-8d86-acd0dac08fa7","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3f442a7e-cb44-4592-8d86-acd0dac08fa7","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"3f442a7e-cb44-4592-8d86-acd0dac08fa7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"682cf3e0-2573-4bf3-ab78-dfbf58429a70","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"682cf3e0-2573-4bf3-ab78-dfbf58429a70","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"682cf3e0-2573-4bf3-ab78-dfbf58429a70","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"21e874de-130e-42db-a127-eef8502a25cc","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"21e874de-130e-42db-a127-eef8502a25cc","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"21e874de-130e-42db-a127-eef8502a25cc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7b3b8dd1-29b4-48b2-8e61-0ac97a16149b","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7b3b8dd1-29b4-48b2-8e61-0ac97a16149b","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"7b3b8dd1-29b4-48b2-8e61-0ac97a16149b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"647c1bc5-40c4-4fee-9156-b1c77e78c6ca","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"647c1bc5-40c4-4fee-9156-b1c77e78c6ca","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"647c1bc5-40c4-4fee-9156-b1c77e78c6ca","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1274aff1-4a21-4ea5-a34f-61ab75414542","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1274aff1-4a21-4ea5-a34f-61ab75414542","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"1274aff1-4a21-4ea5-a34f-61ab75414542","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c8952557-8eb6-42fb-a36a-fff8ecf9bd95","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c8952557-8eb6-42fb-a36a-fff8ecf9bd95","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"c8952557-8eb6-42fb-a36a-fff8ecf9bd95","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"949b9bee-92e1-49c6-aee4-04235b2dd0b4","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"949b9bee-92e1-49c6-aee4-04235b2dd0b4","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"949b9bee-92e1-49c6-aee4-04235b2dd0b4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d0e8e1fd-3072-4f93-b863-35badc9bfbd0","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d0e8e1fd-3072-4f93-b863-35badc9bfbd0","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"d0e8e1fd-3072-4f93-b863-35badc9bfbd0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"eef6f516-28c3-4f94-a69c-e077cc812b71","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"eef6f516-28c3-4f94-a69c-e077cc812b71","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"eef6f516-28c3-4f94-a69c-e077cc812b71","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d155db19-ab65-4ea2-802d-32efe9d4d03a","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d155db19-ab65-4ea2-802d-32efe9d4d03a","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"d155db19-ab65-4ea2-802d-32efe9d4d03a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5877f1c5-b56a-4c6a-acaa-d14b11645cc7","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5877f1c5-b56a-4c6a-acaa-d14b11645cc7","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"5877f1c5-b56a-4c6a-acaa-d14b11645cc7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c6f30d2c-45a2-4b71-b31d-1e093d50af39","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c6f30d2c-45a2-4b71-b31d-1e093d50af39","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"c6f30d2c-45a2-4b71-b31d-1e093d50af39","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0d52a66f-4980-4ad0-a01e-a9c8fdeb548b","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0d52a66f-4980-4ad0-a01e-a9c8fdeb548b","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"LayerId":{"name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","name":"0d52a66f-4980-4ad0-a01e-a9c8fdeb548b","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 32.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"a12f9b21-2a8f-409c-9cf1-2f4e7663b921","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"547f514a-6a84-45a5-8725-9f9609673a18","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"235af70d-9bb7-4be3-b9f0-5ac5d4c8021a","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"be28834d-d004-4038-9f66-38f3ac644db9","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"74ff949d-1365-4957-b7ba-b8aa3194949c","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8ef2f3d6-5b6a-427d-a4ca-1ade4d6a55a2","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d4755864-aca5-4671-ba01-b5f432ac9d60","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"de2d6c6f-3c02-41c6-91c4-b75664464cf1","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0ffd4a33-9b75-4f1d-a219-b7b9554ca043","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bc5f978d-4079-4ab1-ba6f-b67a19f975cd","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a3a823a5-54d8-472d-ad0b-ab7fb4a0cd34","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e8ecbec0-93cb-4f88-ade9-f1429a2a9d81","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"44333dc6-49b9-4941-be0b-6c18e826b4fb","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"81ab5043-f720-468d-b4a4-bca8577bd939","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"daf83b4b-98cc-4569-baa2-16dc71a707d6","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"23889a75-9113-4b47-a81d-e16e3ec33548","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"60e0f100-121b-4ee3-a921-076f59e1682e","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"32396b75-60e2-4fe5-b389-f421e8b1b489","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d71e56b6-7f88-4b8e-b3f2-d791c489c64b","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5197c385-0e7e-493f-adcc-144d52553851","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6eb65633-b82e-4145-b415-5f57905ac290","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"56e700d6-4b70-4795-aaf7-313a0af2bbed","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"77cc1da6-ef10-4101-95b6-59b731782635","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bb071021-0d08-4532-acf1-e8587d66696e","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"231e06d5-a096-42d9-b6b8-ff963edd9a08","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2ad5333a-cfc6-40ac-a3d6-48baa4e6bde9","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a2135d34-c994-4d55-b644-8bc3530289db","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b21dd218-9458-4d3d-a106-249a395ca74a","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"02ce74be-cd8c-47a1-b8fe-39b57578f4a7","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dca6ccf3-5960-49aa-ac59-70e0193b1aae","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b76ea288-a737-43a2-baa6-fd0fc2a63410","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e85acb22-554a-47bb-ac77-903876fb2b43","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1d238f64-cbab-461d-bae2-3dd4439e4059","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fc12891e-b045-4d13-9660-99a5405deaf6","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"168d45d9-1e17-4166-9d58-c0bb76fdd418","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9eb73c77-132c-4af6-b811-33f12d304e2e","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3ab59601-3fca-4a8e-a482-206aa773b61b","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3f442a7e-cb44-4592-8d86-acd0dac08fa7","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"42f9834b-7715-4014-b40c-aebb9d9813c4","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"682cf3e0-2573-4bf3-ab78-dfbf58429a70","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"63799f90-39f1-4273-b20d-e0497e915928","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"21e874de-130e-42db-a127-eef8502a25cc","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3f0d896d-640e-44ab-aef2-33aeab321494","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7b3b8dd1-29b4-48b2-8e61-0ac97a16149b","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"eb391e95-f22e-41b6-91f6-edea4806616e","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"647c1bc5-40c4-4fee-9156-b1c77e78c6ca","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"49f0337c-893f-4acb-9686-4a22baa75c0b","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1274aff1-4a21-4ea5-a34f-61ab75414542","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"82c1ab9f-98d2-42f1-b6ea-361f0a7ba0b9","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c8952557-8eb6-42fb-a36a-fff8ecf9bd95","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6c7b3cae-c03a-4484-a63d-502779086d61","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"949b9bee-92e1-49c6-aee4-04235b2dd0b4","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"98220d5e-63a0-4d33-ae2b-f6b3ef4a72ff","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d0e8e1fd-3072-4f93-b863-35badc9bfbd0","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7d6a02a8-fb5d-4c92-8059-2d19339b1916","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"eef6f516-28c3-4f94-a69c-e077cc812b71","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2ef3e109-38c2-47ff-ab29-a922be0b9822","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d155db19-ab65-4ea2-802d-32efe9d4d03a","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2aa5322b-e97e-4c9d-8887-657b67430e7d","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5877f1c5-b56a-4c6a-acaa-d14b11645cc7","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d83743b7-7562-4261-873a-4e24918e2fa4","Key":30.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c6f30d2c-45a2-4b71-b31d-1e093d50af39","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9aad56de-58d9-4bce-bdb1-d413200e6048","Key":31.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0d52a66f-4980-4ad0-a01e-a9c8fdeb548b","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 32,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Food_6_Crate","path":"sprites/spr_Food_6_Crate/spr_Food_6_Crate.yy",},
    "resourceVersion": "1.3",
    "name": "spr_Food_6_Crate",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"beaaabed-a0db-4167-82e0-2cc9ae5aa6fb","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Trees and Crops",
    "path": "folders/Sprites/Trees and Crops.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Food_6_Crate",
  "tags": [],
  "resourceType": "GMSprite",
}