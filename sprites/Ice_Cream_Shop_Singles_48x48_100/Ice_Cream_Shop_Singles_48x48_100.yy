{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 6,
  "bbox_right": 86,
  "bbox_top": 84,
  "bbox_bottom": 143,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 96,
  "height": 144,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"b7fcf365-d4da-4c32-a313-0b2ed3928213","path":"sprites/Ice_Cream_Shop_Singles_48x48_100/Ice_Cream_Shop_Singles_48x48_100.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b7fcf365-d4da-4c32-a313-0b2ed3928213","path":"sprites/Ice_Cream_Shop_Singles_48x48_100/Ice_Cream_Shop_Singles_48x48_100.yy",},"LayerId":{"name":"15ac85eb-a418-4a72-825f-b19c9dadaa58","path":"sprites/Ice_Cream_Shop_Singles_48x48_100/Ice_Cream_Shop_Singles_48x48_100.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Ice_Cream_Shop_Singles_48x48_100","path":"sprites/Ice_Cream_Shop_Singles_48x48_100/Ice_Cream_Shop_Singles_48x48_100.yy",},"resourceVersion":"1.0","name":"b7fcf365-d4da-4c32-a313-0b2ed3928213","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"Ice_Cream_Shop_Singles_48x48_100","path":"sprites/Ice_Cream_Shop_Singles_48x48_100/Ice_Cream_Shop_Singles_48x48_100.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"73022349-a357-40d6-b9cc-aa5b36cfd0d1","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b7fcf365-d4da-4c32-a313-0b2ed3928213","path":"sprites/Ice_Cream_Shop_Singles_48x48_100/Ice_Cream_Shop_Singles_48x48_100.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 48,
    "yorigin": 72,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"Ice_Cream_Shop_Singles_48x48_100","path":"sprites/Ice_Cream_Shop_Singles_48x48_100/Ice_Cream_Shop_Singles_48x48_100.yy",},
    "resourceVersion": "1.3",
    "name": "Ice_Cream_Shop_Singles_48x48_100",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"15ac85eb-a418-4a72-825f-b19c9dadaa58","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Shops",
    "path": "folders/Sprites/Shops.yy",
  },
  "resourceVersion": "1.0",
  "name": "Ice_Cream_Shop_Singles_48x48_100",
  "tags": [],
  "resourceType": "GMSprite",
}