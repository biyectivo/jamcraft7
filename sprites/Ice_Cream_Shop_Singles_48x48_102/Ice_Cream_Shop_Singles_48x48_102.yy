{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 9,
  "bbox_right": 89,
  "bbox_top": 48,
  "bbox_bottom": 143,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 96,
  "height": 144,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"7de322e2-d8f4-4fe0-9286-ab9f520597b2","path":"sprites/Ice_Cream_Shop_Singles_48x48_102/Ice_Cream_Shop_Singles_48x48_102.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7de322e2-d8f4-4fe0-9286-ab9f520597b2","path":"sprites/Ice_Cream_Shop_Singles_48x48_102/Ice_Cream_Shop_Singles_48x48_102.yy",},"LayerId":{"name":"32c4db27-88a8-4371-9f9d-771839bf1218","path":"sprites/Ice_Cream_Shop_Singles_48x48_102/Ice_Cream_Shop_Singles_48x48_102.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Ice_Cream_Shop_Singles_48x48_102","path":"sprites/Ice_Cream_Shop_Singles_48x48_102/Ice_Cream_Shop_Singles_48x48_102.yy",},"resourceVersion":"1.0","name":"7de322e2-d8f4-4fe0-9286-ab9f520597b2","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"Ice_Cream_Shop_Singles_48x48_102","path":"sprites/Ice_Cream_Shop_Singles_48x48_102/Ice_Cream_Shop_Singles_48x48_102.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"68ef2a69-b2c1-4aee-94a3-2133d1c344ec","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7de322e2-d8f4-4fe0-9286-ab9f520597b2","path":"sprites/Ice_Cream_Shop_Singles_48x48_102/Ice_Cream_Shop_Singles_48x48_102.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 48,
    "yorigin": 72,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"Ice_Cream_Shop_Singles_48x48_102","path":"sprites/Ice_Cream_Shop_Singles_48x48_102/Ice_Cream_Shop_Singles_48x48_102.yy",},
    "resourceVersion": "1.3",
    "name": "Ice_Cream_Shop_Singles_48x48_102",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"32c4db27-88a8-4371-9f9d-771839bf1218","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Shops",
    "path": "folders/Sprites/Shops.yy",
  },
  "resourceVersion": "1.0",
  "name": "Ice_Cream_Shop_Singles_48x48_102",
  "tags": [],
  "resourceType": "GMSprite",
}