{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 31,
  "bbox_top": 19,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"f47e34cd-3943-4911-902b-4e70e710e452","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f47e34cd-3943-4911-902b-4e70e710e452","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"f47e34cd-3943-4911-902b-4e70e710e452","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6f73d901-27cd-4306-968d-9f1a8280c011","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6f73d901-27cd-4306-968d-9f1a8280c011","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"6f73d901-27cd-4306-968d-9f1a8280c011","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"329b77ad-8cbb-42c2-873e-49a003369a0d","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"329b77ad-8cbb-42c2-873e-49a003369a0d","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"329b77ad-8cbb-42c2-873e-49a003369a0d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"926b3cbb-1955-41d2-be11-9b5f1ca4aeaa","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"926b3cbb-1955-41d2-be11-9b5f1ca4aeaa","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"926b3cbb-1955-41d2-be11-9b5f1ca4aeaa","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c57ed5a7-89fb-4424-ae0c-df99d476a023","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c57ed5a7-89fb-4424-ae0c-df99d476a023","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"c57ed5a7-89fb-4424-ae0c-df99d476a023","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2d9a7c10-72a6-4839-9601-e8e2ca51be31","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2d9a7c10-72a6-4839-9601-e8e2ca51be31","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"2d9a7c10-72a6-4839-9601-e8e2ca51be31","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cc83ddfd-6f05-4641-ac05-0b3c677aa4b7","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cc83ddfd-6f05-4641-ac05-0b3c677aa4b7","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"cc83ddfd-6f05-4641-ac05-0b3c677aa4b7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d9759603-8f6a-4b6f-935e-58d7ab4e2038","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d9759603-8f6a-4b6f-935e-58d7ab4e2038","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"d9759603-8f6a-4b6f-935e-58d7ab4e2038","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"492b957f-a9f7-4241-932b-ae063a610c03","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"492b957f-a9f7-4241-932b-ae063a610c03","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"492b957f-a9f7-4241-932b-ae063a610c03","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"46d34866-619b-4db5-accf-4e81bc8682ef","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"46d34866-619b-4db5-accf-4e81bc8682ef","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"46d34866-619b-4db5-accf-4e81bc8682ef","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6dc08ec7-b3fd-4b91-a71f-6190bca75c5e","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6dc08ec7-b3fd-4b91-a71f-6190bca75c5e","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"6dc08ec7-b3fd-4b91-a71f-6190bca75c5e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f0e5506e-d3f3-4026-a240-7f5a0d4610d0","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f0e5506e-d3f3-4026-a240-7f5a0d4610d0","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"f0e5506e-d3f3-4026-a240-7f5a0d4610d0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"32032c3e-cc07-42a5-aba0-f74787d14edf","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"32032c3e-cc07-42a5-aba0-f74787d14edf","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"32032c3e-cc07-42a5-aba0-f74787d14edf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2f60196c-cef5-449c-8c53-c82fa4de2dfa","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2f60196c-cef5-449c-8c53-c82fa4de2dfa","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"2f60196c-cef5-449c-8c53-c82fa4de2dfa","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"79190280-1ac8-47a5-968f-a35e97d19db4","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"79190280-1ac8-47a5-968f-a35e97d19db4","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"79190280-1ac8-47a5-968f-a35e97d19db4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0cb26c22-26bf-4a4c-ae79-dc8c3dcd6531","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0cb26c22-26bf-4a4c-ae79-dc8c3dcd6531","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"0cb26c22-26bf-4a4c-ae79-dc8c3dcd6531","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"84a10742-88a6-465d-988a-2cde3e1a6424","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"84a10742-88a6-465d-988a-2cde3e1a6424","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"84a10742-88a6-465d-988a-2cde3e1a6424","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"878ab36d-bc6d-4a94-b56e-cf52aae5dbf9","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"878ab36d-bc6d-4a94-b56e-cf52aae5dbf9","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"878ab36d-bc6d-4a94-b56e-cf52aae5dbf9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3b489e2b-c55c-4534-b295-feda731a778d","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3b489e2b-c55c-4534-b295-feda731a778d","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"3b489e2b-c55c-4534-b295-feda731a778d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5142d1b3-93e8-4bb0-ac70-c193894f92da","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5142d1b3-93e8-4bb0-ac70-c193894f92da","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"5142d1b3-93e8-4bb0-ac70-c193894f92da","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"355c6763-6b7f-47fb-a1fc-b193af6e550a","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"355c6763-6b7f-47fb-a1fc-b193af6e550a","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"355c6763-6b7f-47fb-a1fc-b193af6e550a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0cd41068-9c90-47e4-b553-7fea9be62acb","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0cd41068-9c90-47e4-b553-7fea9be62acb","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"0cd41068-9c90-47e4-b553-7fea9be62acb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7f4ade06-4ba6-46d5-9ecf-56e6889def9e","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7f4ade06-4ba6-46d5-9ecf-56e6889def9e","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"7f4ade06-4ba6-46d5-9ecf-56e6889def9e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1ac6e781-52cc-445b-9da4-82a25634720a","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1ac6e781-52cc-445b-9da4-82a25634720a","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"1ac6e781-52cc-445b-9da4-82a25634720a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"54459782-9b88-455a-a95d-a2a44c5a95d6","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"54459782-9b88-455a-a95d-a2a44c5a95d6","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"54459782-9b88-455a-a95d-a2a44c5a95d6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1d5bbe86-0931-4242-a488-f8335fdb1c7a","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1d5bbe86-0931-4242-a488-f8335fdb1c7a","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"1d5bbe86-0931-4242-a488-f8335fdb1c7a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"05a4ab5e-7e23-4a5f-bec5-7a8fa181efc0","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"05a4ab5e-7e23-4a5f-bec5-7a8fa181efc0","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"05a4ab5e-7e23-4a5f-bec5-7a8fa181efc0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4e758d3d-d829-4428-beee-66cbcaa07f5b","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4e758d3d-d829-4428-beee-66cbcaa07f5b","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"4e758d3d-d829-4428-beee-66cbcaa07f5b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"89368e0d-0d14-4f6c-a0fb-830f158e853d","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"89368e0d-0d14-4f6c-a0fb-830f158e853d","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"89368e0d-0d14-4f6c-a0fb-830f158e853d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f3de8b90-771f-42b9-bc39-8a04770c9abf","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f3de8b90-771f-42b9-bc39-8a04770c9abf","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"f3de8b90-771f-42b9-bc39-8a04770c9abf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fb00bc65-73ab-43e9-85ec-80d1e0054979","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fb00bc65-73ab-43e9-85ec-80d1e0054979","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"fb00bc65-73ab-43e9-85ec-80d1e0054979","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"872ecdb2-3ba1-4232-afd5-007ad392ac40","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"872ecdb2-3ba1-4232-afd5-007ad392ac40","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"LayerId":{"name":"96fa0da3-eb30-498a-8d63-772b054370cd","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","name":"872ecdb2-3ba1-4232-afd5-007ad392ac40","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 32.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"55f34b91-8cc1-465e-8c0e-9df8a154d616","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f47e34cd-3943-4911-902b-4e70e710e452","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"332245e3-05b3-4587-9a14-d47e9b3d5b65","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6f73d901-27cd-4306-968d-9f1a8280c011","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2bc3300a-342f-48e3-8565-04ad998e4089","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"329b77ad-8cbb-42c2-873e-49a003369a0d","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d39a1a3a-e053-438a-a633-65dfa2cbdc40","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"926b3cbb-1955-41d2-be11-9b5f1ca4aeaa","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"21be52ea-57f7-446b-a474-64ec020b9e1e","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c57ed5a7-89fb-4424-ae0c-df99d476a023","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8defe2ee-ef56-4200-ba1d-29ac3adc1b4a","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2d9a7c10-72a6-4839-9601-e8e2ca51be31","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3cea8fa4-f50b-4e35-9617-8f6c866eade0","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cc83ddfd-6f05-4641-ac05-0b3c677aa4b7","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"807cfd4a-efa1-476f-a7fa-79793a529b05","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d9759603-8f6a-4b6f-935e-58d7ab4e2038","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"276cbaf8-eaab-4aac-8bcf-1b3adda2ef76","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"492b957f-a9f7-4241-932b-ae063a610c03","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"90540fbc-56dc-4a8c-bf71-81a4f87367ef","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"46d34866-619b-4db5-accf-4e81bc8682ef","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9c4bfc3b-3570-4331-82a1-3d2ebfb9f11b","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6dc08ec7-b3fd-4b91-a71f-6190bca75c5e","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e3afc3dc-e58d-4680-8cb5-ad1b7ce744cd","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f0e5506e-d3f3-4026-a240-7f5a0d4610d0","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"41b2870f-1ce9-4277-b6e6-36360d793d5c","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"32032c3e-cc07-42a5-aba0-f74787d14edf","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"170c66f4-23eb-4641-8e9a-ac4bc1eef6ae","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2f60196c-cef5-449c-8c53-c82fa4de2dfa","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9f169df6-38d4-4365-b13a-ec8353e577e3","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"79190280-1ac8-47a5-968f-a35e97d19db4","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"40536dc8-e872-49d6-acda-efed2742a3e0","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0cb26c22-26bf-4a4c-ae79-dc8c3dcd6531","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"61a66d81-7ed5-4b18-9689-72226b87aa7d","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"84a10742-88a6-465d-988a-2cde3e1a6424","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0169bec8-8ec0-423f-96eb-f0f76fe33112","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"878ab36d-bc6d-4a94-b56e-cf52aae5dbf9","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a6b66434-0e0e-44a8-ae51-e95d6ee894bd","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3b489e2b-c55c-4534-b295-feda731a778d","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dd12298d-da0b-4017-b34d-e3d64aaf0c22","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5142d1b3-93e8-4bb0-ac70-c193894f92da","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"326d998b-8177-47c7-9f90-763b9a694bc9","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"355c6763-6b7f-47fb-a1fc-b193af6e550a","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dc2c1cbd-9187-4579-9b5d-62cd3c2c6c20","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0cd41068-9c90-47e4-b553-7fea9be62acb","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0bf00802-333c-4758-9019-ec69d400af74","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7f4ade06-4ba6-46d5-9ecf-56e6889def9e","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5ef856e2-9548-42c3-b2b5-3d2b7a4421bd","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1ac6e781-52cc-445b-9da4-82a25634720a","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b21e3c71-0693-451b-86b0-67f3338ba61b","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"54459782-9b88-455a-a95d-a2a44c5a95d6","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"83001f1f-fdad-4c4f-9d07-a608bb7660a7","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1d5bbe86-0931-4242-a488-f8335fdb1c7a","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ddb461b7-7ac0-4317-9805-c3f1dfaac208","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"05a4ab5e-7e23-4a5f-bec5-7a8fa181efc0","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0e4beeb9-efc1-48a8-ae20-71629b825875","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4e758d3d-d829-4428-beee-66cbcaa07f5b","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5b55bac2-3712-4168-b5d0-03cf82c572cf","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"89368e0d-0d14-4f6c-a0fb-830f158e853d","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"462ec806-921c-4e4d-9df3-7b1967ed434d","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f3de8b90-771f-42b9-bc39-8a04770c9abf","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"22cc76ea-fc0a-4a4c-ab96-67dbc0673535","Key":30.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fb00bc65-73ab-43e9-85ec-80d1e0054979","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e3872786-0c2e-4c34-8210-78cc3f3b1615","Key":31.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"872ecdb2-3ba1-4232-afd5-007ad392ac40","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 32,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Food_1_Crate","path":"sprites/spr_Food_1_Crate/spr_Food_1_Crate.yy",},
    "resourceVersion": "1.3",
    "name": "spr_Food_1_Crate",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"96fa0da3-eb30-498a-8d63-772b054370cd","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Trees and Crops",
    "path": "folders/Sprites/Trees and Crops.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Food_1_Crate",
  "tags": [],
  "resourceType": "GMSprite",
}