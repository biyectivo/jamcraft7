{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 9,
  "bbox_right": 1142,
  "bbox_top": 54,
  "bbox_bottom": 644,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 1152,
  "height": 672,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"dd578aa8-d668-4e26-95a9-c0f6152d626e","path":"sprites/slice_Accessory_15_48x48_04/slice_Accessory_15_48x48_04.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dd578aa8-d668-4e26-95a9-c0f6152d626e","path":"sprites/slice_Accessory_15_48x48_04/slice_Accessory_15_48x48_04.yy",},"LayerId":{"name":"aa82bf87-cd57-4775-baae-5a9d1f81ff74","path":"sprites/slice_Accessory_15_48x48_04/slice_Accessory_15_48x48_04.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"slice_Accessory_15_48x48_04","path":"sprites/slice_Accessory_15_48x48_04/slice_Accessory_15_48x48_04.yy",},"resourceVersion":"1.0","name":"dd578aa8-d668-4e26-95a9-c0f6152d626e","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"slice_Accessory_15_48x48_04","path":"sprites/slice_Accessory_15_48x48_04/slice_Accessory_15_48x48_04.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"872871c7-9074-4694-b8a9-645020826aa9","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dd578aa8-d668-4e26-95a9-c0f6152d626e","path":"sprites/slice_Accessory_15_48x48_04/slice_Accessory_15_48x48_04.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 576,
    "yorigin": 336,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"slice_Accessory_15_48x48_04","path":"sprites/slice_Accessory_15_48x48_04/slice_Accessory_15_48x48_04.yy",},
    "resourceVersion": "1.3",
    "name": "slice_Accessory_15_48x48_04",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"aa82bf87-cd57-4775-baae-5a9d1f81ff74","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "People",
    "path": "folders/Sprites/People.yy",
  },
  "resourceVersion": "1.0",
  "name": "slice_Accessory_15_48x48_04",
  "tags": [],
  "resourceType": "GMSprite",
}