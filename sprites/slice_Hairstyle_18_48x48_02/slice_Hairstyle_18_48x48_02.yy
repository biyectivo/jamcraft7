{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 3,
  "bbox_right": 1148,
  "bbox_top": 27,
  "bbox_bottom": 641,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 1152,
  "height": 672,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"501e25f8-90cc-485f-a6fb-3c5423042400","path":"sprites/slice_Hairstyle_18_48x48_02/slice_Hairstyle_18_48x48_02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"501e25f8-90cc-485f-a6fb-3c5423042400","path":"sprites/slice_Hairstyle_18_48x48_02/slice_Hairstyle_18_48x48_02.yy",},"LayerId":{"name":"8d9ffaa2-55c6-419a-a112-3358c5417dfe","path":"sprites/slice_Hairstyle_18_48x48_02/slice_Hairstyle_18_48x48_02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"slice_Hairstyle_18_48x48_02","path":"sprites/slice_Hairstyle_18_48x48_02/slice_Hairstyle_18_48x48_02.yy",},"resourceVersion":"1.0","name":"501e25f8-90cc-485f-a6fb-3c5423042400","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"slice_Hairstyle_18_48x48_02","path":"sprites/slice_Hairstyle_18_48x48_02/slice_Hairstyle_18_48x48_02.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"0699e2b6-668d-4ab2-a9f5-3765856cbccc","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"501e25f8-90cc-485f-a6fb-3c5423042400","path":"sprites/slice_Hairstyle_18_48x48_02/slice_Hairstyle_18_48x48_02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 576,
    "yorigin": 336,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"slice_Hairstyle_18_48x48_02","path":"sprites/slice_Hairstyle_18_48x48_02/slice_Hairstyle_18_48x48_02.yy",},
    "resourceVersion": "1.3",
    "name": "slice_Hairstyle_18_48x48_02",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"8d9ffaa2-55c6-419a-a112-3358c5417dfe","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "People",
    "path": "folders/Sprites/People.yy",
  },
  "resourceVersion": "1.0",
  "name": "slice_Hairstyle_18_48x48_02",
  "tags": [],
  "resourceType": "GMSprite",
}