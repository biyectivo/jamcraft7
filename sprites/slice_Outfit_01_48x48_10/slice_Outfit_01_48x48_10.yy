{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 3,
  "bbox_right": 1148,
  "bbox_top": 63,
  "bbox_bottom": 671,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 1152,
  "height": 672,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"72ca5d57-7a57-4f28-8950-ce66a0f57a4c","path":"sprites/slice_Outfit_01_48x48_10/slice_Outfit_01_48x48_10.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"72ca5d57-7a57-4f28-8950-ce66a0f57a4c","path":"sprites/slice_Outfit_01_48x48_10/slice_Outfit_01_48x48_10.yy",},"LayerId":{"name":"8c53f84e-3e71-45e8-825e-e3b0ecad600e","path":"sprites/slice_Outfit_01_48x48_10/slice_Outfit_01_48x48_10.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"slice_Outfit_01_48x48_10","path":"sprites/slice_Outfit_01_48x48_10/slice_Outfit_01_48x48_10.yy",},"resourceVersion":"1.0","name":"72ca5d57-7a57-4f28-8950-ce66a0f57a4c","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"slice_Outfit_01_48x48_10","path":"sprites/slice_Outfit_01_48x48_10/slice_Outfit_01_48x48_10.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"5b74a809-2565-40d8-957a-e1e923a5d405","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"72ca5d57-7a57-4f28-8950-ce66a0f57a4c","path":"sprites/slice_Outfit_01_48x48_10/slice_Outfit_01_48x48_10.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 576,
    "yorigin": 336,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"slice_Outfit_01_48x48_10","path":"sprites/slice_Outfit_01_48x48_10/slice_Outfit_01_48x48_10.yy",},
    "resourceVersion": "1.3",
    "name": "slice_Outfit_01_48x48_10",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"8c53f84e-3e71-45e8-825e-e3b0ecad600e","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "People",
    "path": "folders/Sprites/People.yy",
  },
  "resourceVersion": "1.0",
  "name": "slice_Outfit_01_48x48_10",
  "tags": [],
  "resourceType": "GMSprite",
}