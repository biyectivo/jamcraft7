{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 31,
  "bbox_top": 18,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"a12a76c5-5525-4b59-951b-86d28644b98e","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a12a76c5-5525-4b59-951b-86d28644b98e","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"a12a76c5-5525-4b59-951b-86d28644b98e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c624f417-46b4-4ca7-a919-6ab03f626849","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c624f417-46b4-4ca7-a919-6ab03f626849","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"c624f417-46b4-4ca7-a919-6ab03f626849","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"23c5b3c0-4a00-4dea-85f9-cf8208e57b8a","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"23c5b3c0-4a00-4dea-85f9-cf8208e57b8a","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"23c5b3c0-4a00-4dea-85f9-cf8208e57b8a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9437ee6e-f62e-41d9-8217-1b8ed316ab1e","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9437ee6e-f62e-41d9-8217-1b8ed316ab1e","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"9437ee6e-f62e-41d9-8217-1b8ed316ab1e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"55f0986b-60c9-4c4c-bf9f-72a8e4516ada","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"55f0986b-60c9-4c4c-bf9f-72a8e4516ada","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"55f0986b-60c9-4c4c-bf9f-72a8e4516ada","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"48030b2d-d146-4fb0-8ea1-d9ade4fcc36d","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"48030b2d-d146-4fb0-8ea1-d9ade4fcc36d","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"48030b2d-d146-4fb0-8ea1-d9ade4fcc36d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6093ba38-fd20-49c4-8766-8f57f9335a3d","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6093ba38-fd20-49c4-8766-8f57f9335a3d","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"6093ba38-fd20-49c4-8766-8f57f9335a3d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8ef0840a-5c22-4da7-8f3f-1b3e4bc41b55","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8ef0840a-5c22-4da7-8f3f-1b3e4bc41b55","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"8ef0840a-5c22-4da7-8f3f-1b3e4bc41b55","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d01b0cb9-2e4d-4668-97f2-50a0850a80d9","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d01b0cb9-2e4d-4668-97f2-50a0850a80d9","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"d01b0cb9-2e4d-4668-97f2-50a0850a80d9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2d31c0fa-a11d-448e-9438-895ca5fd0eee","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2d31c0fa-a11d-448e-9438-895ca5fd0eee","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"2d31c0fa-a11d-448e-9438-895ca5fd0eee","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6261e63a-2152-4e84-bd18-210aa518d86e","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6261e63a-2152-4e84-bd18-210aa518d86e","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"6261e63a-2152-4e84-bd18-210aa518d86e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d9e7e60a-3101-451e-92cc-1e3a7642d2cf","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d9e7e60a-3101-451e-92cc-1e3a7642d2cf","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"d9e7e60a-3101-451e-92cc-1e3a7642d2cf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"685cd5b7-5503-4522-92c7-119484a95a78","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"685cd5b7-5503-4522-92c7-119484a95a78","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"685cd5b7-5503-4522-92c7-119484a95a78","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b30d76a9-2199-447b-a22a-b4c562fce922","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b30d76a9-2199-447b-a22a-b4c562fce922","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"b30d76a9-2199-447b-a22a-b4c562fce922","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e586870f-0322-4f27-9e2d-10625ef8fe51","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e586870f-0322-4f27-9e2d-10625ef8fe51","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"e586870f-0322-4f27-9e2d-10625ef8fe51","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c7d4ee1c-2d94-4d99-9966-d94d49e82402","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c7d4ee1c-2d94-4d99-9966-d94d49e82402","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"c7d4ee1c-2d94-4d99-9966-d94d49e82402","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a5be3efa-844e-4c0a-b503-934b0b6ca527","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a5be3efa-844e-4c0a-b503-934b0b6ca527","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"a5be3efa-844e-4c0a-b503-934b0b6ca527","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bc20f128-6229-40da-a872-e8bf1c39ec52","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bc20f128-6229-40da-a872-e8bf1c39ec52","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"bc20f128-6229-40da-a872-e8bf1c39ec52","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"545a072c-fb4b-4e41-967f-25ab6506caed","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"545a072c-fb4b-4e41-967f-25ab6506caed","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"545a072c-fb4b-4e41-967f-25ab6506caed","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"af1e3092-adcf-47a7-9105-162b469d62a2","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"af1e3092-adcf-47a7-9105-162b469d62a2","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"af1e3092-adcf-47a7-9105-162b469d62a2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"033840da-c16c-47fc-87c1-c83169855621","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"033840da-c16c-47fc-87c1-c83169855621","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"033840da-c16c-47fc-87c1-c83169855621","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f750b38b-fca0-4057-a65e-82283ae91af3","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f750b38b-fca0-4057-a65e-82283ae91af3","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"f750b38b-fca0-4057-a65e-82283ae91af3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b48febbb-7654-4f5e-b9de-e53b8b5d88be","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b48febbb-7654-4f5e-b9de-e53b8b5d88be","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"b48febbb-7654-4f5e-b9de-e53b8b5d88be","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f496389d-0ce3-4d15-9032-27862bd43791","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f496389d-0ce3-4d15-9032-27862bd43791","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"f496389d-0ce3-4d15-9032-27862bd43791","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1025c47a-8d32-4e3d-aa4b-99b2e4cd8fcc","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1025c47a-8d32-4e3d-aa4b-99b2e4cd8fcc","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"1025c47a-8d32-4e3d-aa4b-99b2e4cd8fcc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a632fbbb-d662-448e-965f-6f8ac3716e58","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a632fbbb-d662-448e-965f-6f8ac3716e58","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"a632fbbb-d662-448e-965f-6f8ac3716e58","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ce7d8f5f-21c2-4f54-b14a-df0b34dd8d72","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ce7d8f5f-21c2-4f54-b14a-df0b34dd8d72","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"ce7d8f5f-21c2-4f54-b14a-df0b34dd8d72","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e77045f1-f2c9-40f6-a565-623c6edd874b","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e77045f1-f2c9-40f6-a565-623c6edd874b","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"e77045f1-f2c9-40f6-a565-623c6edd874b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5cde21b6-52fd-4afc-a227-c871eb3500cd","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5cde21b6-52fd-4afc-a227-c871eb3500cd","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"5cde21b6-52fd-4afc-a227-c871eb3500cd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"51170641-4acb-4746-845e-a500f41462cc","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"51170641-4acb-4746-845e-a500f41462cc","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"51170641-4acb-4746-845e-a500f41462cc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"83093716-1ef5-45da-9526-8dc6bc4842f6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"83093716-1ef5-45da-9526-8dc6bc4842f6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"83093716-1ef5-45da-9526-8dc6bc4842f6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"46d0e8e9-2496-4eb6-96f7-1ec5accb8155","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"46d0e8e9-2496-4eb6-96f7-1ec5accb8155","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"LayerId":{"name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","name":"46d0e8e9-2496-4eb6-96f7-1ec5accb8155","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 32.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"92b3935e-e203-4030-b5fa-dc3830ce6b55","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a12a76c5-5525-4b59-951b-86d28644b98e","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c734eda9-fcd9-4bb8-bf0e-4ed18fac2b8d","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c624f417-46b4-4ca7-a919-6ab03f626849","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b71229be-ffe5-4032-926c-cfb691281468","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"23c5b3c0-4a00-4dea-85f9-cf8208e57b8a","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"27bc727a-2d74-40bf-97a7-4f99928b0c3f","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9437ee6e-f62e-41d9-8217-1b8ed316ab1e","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"594305c7-4457-4fdc-a0c1-58efaff68aed","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"55f0986b-60c9-4c4c-bf9f-72a8e4516ada","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"be83b472-2f3e-4893-a391-7ea4c502c0cd","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"48030b2d-d146-4fb0-8ea1-d9ade4fcc36d","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3a6b1808-9e63-4380-95e7-ee7b6155e780","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6093ba38-fd20-49c4-8766-8f57f9335a3d","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5f105e2d-d021-40ac-81fe-96a983248cb0","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8ef0840a-5c22-4da7-8f3f-1b3e4bc41b55","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c9f10211-f469-4ca9-9de2-6ba0e5f0669e","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d01b0cb9-2e4d-4668-97f2-50a0850a80d9","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"08fe327d-767d-4079-b24e-fb65b9765d90","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2d31c0fa-a11d-448e-9438-895ca5fd0eee","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"25777ce7-c294-447e-9cec-ed85f5b9f10b","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6261e63a-2152-4e84-bd18-210aa518d86e","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"21c6770b-05e9-4efa-a48f-8e07936ad448","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d9e7e60a-3101-451e-92cc-1e3a7642d2cf","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6683ab9b-e90c-4521-b31b-b3d4acaf8c5b","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"685cd5b7-5503-4522-92c7-119484a95a78","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e9607fab-a9fa-4e9b-a44b-dc405a83cd68","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b30d76a9-2199-447b-a22a-b4c562fce922","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f5160728-1068-4efb-8130-3c2211cf8e88","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e586870f-0322-4f27-9e2d-10625ef8fe51","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"02c9a4b8-d848-46ad-b5b3-9b7baf221741","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c7d4ee1c-2d94-4d99-9966-d94d49e82402","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0356a98f-edad-42a3-8dc2-0126fbfbd407","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a5be3efa-844e-4c0a-b503-934b0b6ca527","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f58c74cc-340c-418a-8536-15a237d98ac2","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bc20f128-6229-40da-a872-e8bf1c39ec52","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b1f33ea8-a4eb-4771-bb11-1431eae0430b","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"545a072c-fb4b-4e41-967f-25ab6506caed","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"993163ac-fa4c-43c6-81c0-1aceadcd837b","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"af1e3092-adcf-47a7-9105-162b469d62a2","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c67b6c69-6d51-434a-81fd-456d908f1143","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"033840da-c16c-47fc-87c1-c83169855621","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4a838d3e-cfef-4283-9f05-0ae47c66f982","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f750b38b-fca0-4057-a65e-82283ae91af3","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"200b1eaf-6fe6-494d-80c9-c4f6fb155733","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b48febbb-7654-4f5e-b9de-e53b8b5d88be","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"358a46f0-a141-4016-b860-4e46c1276e30","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f496389d-0ce3-4d15-9032-27862bd43791","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"934ec07f-f882-4687-bf29-514efc4f598e","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1025c47a-8d32-4e3d-aa4b-99b2e4cd8fcc","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2953d37d-ebce-43f8-adbe-3daecc1b7c6e","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a632fbbb-d662-448e-965f-6f8ac3716e58","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1593a553-807e-4587-bd57-f802663df891","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ce7d8f5f-21c2-4f54-b14a-df0b34dd8d72","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bb3ae0cf-bf5a-43ba-9301-2029f876a82e","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e77045f1-f2c9-40f6-a565-623c6edd874b","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ecf87169-d826-48af-be36-0391961f1e37","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5cde21b6-52fd-4afc-a227-c871eb3500cd","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"63d0ca6b-c2bd-44e1-9cab-939312ec1f41","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"51170641-4acb-4746-845e-a500f41462cc","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c671c4a3-bb0b-408b-b0b5-da91b018defa","Key":30.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"83093716-1ef5-45da-9526-8dc6bc4842f6","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"669dddb1-e98a-4f2d-8162-f2457b24fe8e","Key":31.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"46d0e8e9-2496-4eb6-96f7-1ec5accb8155","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 32,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Food_4_Crate","path":"sprites/spr_Food_4_Crate/spr_Food_4_Crate.yy",},
    "resourceVersion": "1.3",
    "name": "spr_Food_4_Crate",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"7cf2c63f-c615-4a8c-b9cc-3b4b8c7f44e6","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Trees and Crops",
    "path": "folders/Sprites/Trees and Crops.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Food_4_Crate",
  "tags": [],
  "resourceType": "GMSprite",
}