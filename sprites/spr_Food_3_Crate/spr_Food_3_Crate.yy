{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 31,
  "bbox_top": 19,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"d354b9f6-313a-458e-99c1-3ca9be3d669f","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d354b9f6-313a-458e-99c1-3ca9be3d669f","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"d354b9f6-313a-458e-99c1-3ca9be3d669f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2fc1ddb8-ddcc-491b-b8a1-47c7e8bfb4ed","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2fc1ddb8-ddcc-491b-b8a1-47c7e8bfb4ed","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"2fc1ddb8-ddcc-491b-b8a1-47c7e8bfb4ed","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5c40643c-610a-4069-ae27-8ec8e9639eb8","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5c40643c-610a-4069-ae27-8ec8e9639eb8","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"5c40643c-610a-4069-ae27-8ec8e9639eb8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8690fad7-9e4e-4b01-a567-84bf850f4f44","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8690fad7-9e4e-4b01-a567-84bf850f4f44","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"8690fad7-9e4e-4b01-a567-84bf850f4f44","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1935997f-c995-4681-bb8a-2d2fa6a1028d","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1935997f-c995-4681-bb8a-2d2fa6a1028d","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"1935997f-c995-4681-bb8a-2d2fa6a1028d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"426cbde9-1453-47d1-a2ee-0f50ce7cf813","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"426cbde9-1453-47d1-a2ee-0f50ce7cf813","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"426cbde9-1453-47d1-a2ee-0f50ce7cf813","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"559e6670-f835-4476-bad6-506c95158722","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"559e6670-f835-4476-bad6-506c95158722","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"559e6670-f835-4476-bad6-506c95158722","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"94624d3a-6050-4947-8869-1ab279d54546","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"94624d3a-6050-4947-8869-1ab279d54546","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"94624d3a-6050-4947-8869-1ab279d54546","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cfd73098-ee33-4555-b918-7cdcf95faca9","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cfd73098-ee33-4555-b918-7cdcf95faca9","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"cfd73098-ee33-4555-b918-7cdcf95faca9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a3e1b92e-2ef8-4e33-87ce-ccb8534f90fb","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a3e1b92e-2ef8-4e33-87ce-ccb8534f90fb","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"a3e1b92e-2ef8-4e33-87ce-ccb8534f90fb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"70d772c4-37c0-4cd3-8003-fe6a01ebe081","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"70d772c4-37c0-4cd3-8003-fe6a01ebe081","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"70d772c4-37c0-4cd3-8003-fe6a01ebe081","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"72f2eb5a-8c14-47c9-9766-3f77f6968aaf","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"72f2eb5a-8c14-47c9-9766-3f77f6968aaf","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"72f2eb5a-8c14-47c9-9766-3f77f6968aaf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"68d0a1fb-f092-4f37-afac-a92166924fc2","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"68d0a1fb-f092-4f37-afac-a92166924fc2","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"68d0a1fb-f092-4f37-afac-a92166924fc2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6bf83922-d0e2-41aa-9053-0db5590f3986","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6bf83922-d0e2-41aa-9053-0db5590f3986","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"6bf83922-d0e2-41aa-9053-0db5590f3986","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d5b55477-58a0-4f32-82fa-255d2111476b","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d5b55477-58a0-4f32-82fa-255d2111476b","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"d5b55477-58a0-4f32-82fa-255d2111476b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"834496d7-a822-4a66-806c-c873bb629150","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"834496d7-a822-4a66-806c-c873bb629150","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"834496d7-a822-4a66-806c-c873bb629150","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7bd02f6c-d6a4-496f-9d58-20125144cd61","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7bd02f6c-d6a4-496f-9d58-20125144cd61","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"7bd02f6c-d6a4-496f-9d58-20125144cd61","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6f01dfae-cfee-451c-b2fa-f7f830fcd9f3","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6f01dfae-cfee-451c-b2fa-f7f830fcd9f3","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"6f01dfae-cfee-451c-b2fa-f7f830fcd9f3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"05c7219e-7726-4a5a-9c5f-caa77490e534","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"05c7219e-7726-4a5a-9c5f-caa77490e534","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"05c7219e-7726-4a5a-9c5f-caa77490e534","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ded90eb0-1432-4904-9b6f-9aa774078350","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ded90eb0-1432-4904-9b6f-9aa774078350","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"ded90eb0-1432-4904-9b6f-9aa774078350","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0c7c54a1-5851-43e4-8584-70543839d174","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0c7c54a1-5851-43e4-8584-70543839d174","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"0c7c54a1-5851-43e4-8584-70543839d174","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c1f9ba9c-3c0c-4b74-9b81-b7170ba93fe4","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c1f9ba9c-3c0c-4b74-9b81-b7170ba93fe4","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"c1f9ba9c-3c0c-4b74-9b81-b7170ba93fe4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fa85e929-bb6e-42d4-a13b-138687112ede","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fa85e929-bb6e-42d4-a13b-138687112ede","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"fa85e929-bb6e-42d4-a13b-138687112ede","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"daca560d-7b88-459c-be73-59f418f2cd2a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"daca560d-7b88-459c-be73-59f418f2cd2a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"daca560d-7b88-459c-be73-59f418f2cd2a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c8bb66ab-bc88-48ee-a469-f76f2a8974fd","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c8bb66ab-bc88-48ee-a469-f76f2a8974fd","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"c8bb66ab-bc88-48ee-a469-f76f2a8974fd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bff7bfa0-e2bb-4cc7-a182-1fae85ef8a8e","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bff7bfa0-e2bb-4cc7-a182-1fae85ef8a8e","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"bff7bfa0-e2bb-4cc7-a182-1fae85ef8a8e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bf9a4684-4119-487b-8f8b-f99901d5673f","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bf9a4684-4119-487b-8f8b-f99901d5673f","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"bf9a4684-4119-487b-8f8b-f99901d5673f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4ee99fb8-56d6-4554-978c-2cfa7910380d","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4ee99fb8-56d6-4554-978c-2cfa7910380d","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"4ee99fb8-56d6-4554-978c-2cfa7910380d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7490a3bd-66ea-4021-8fd3-a99c9963caef","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7490a3bd-66ea-4021-8fd3-a99c9963caef","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"7490a3bd-66ea-4021-8fd3-a99c9963caef","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0e969e4e-7371-4ac9-a84c-513605989d2a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0e969e4e-7371-4ac9-a84c-513605989d2a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"0e969e4e-7371-4ac9-a84c-513605989d2a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b976499e-e592-41cd-b330-82a4bea3f42e","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b976499e-e592-41cd-b330-82a4bea3f42e","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"b976499e-e592-41cd-b330-82a4bea3f42e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"63510e3e-99f1-4770-9edf-dca1c6bcbfff","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"63510e3e-99f1-4770-9edf-dca1c6bcbfff","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"LayerId":{"name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","name":"63510e3e-99f1-4770-9edf-dca1c6bcbfff","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 32.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"1a3fb18a-96f9-4c9f-b7c0-173bce8d0974","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d354b9f6-313a-458e-99c1-3ca9be3d669f","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"48326ee5-74d4-49ce-a6f0-87180ab179ac","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2fc1ddb8-ddcc-491b-b8a1-47c7e8bfb4ed","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7bf82361-1961-4d0e-a2b9-f0548181c0d2","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5c40643c-610a-4069-ae27-8ec8e9639eb8","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d27b0ee8-feae-4917-bb6d-85f83d09920a","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8690fad7-9e4e-4b01-a567-84bf850f4f44","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fd669b71-e35d-4970-923f-bb8e03b2c99b","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1935997f-c995-4681-bb8a-2d2fa6a1028d","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"be752098-9d05-4cda-add9-661976c5eb13","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"426cbde9-1453-47d1-a2ee-0f50ce7cf813","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d891dbf3-fb58-40dd-9534-b708bc3be2f5","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"559e6670-f835-4476-bad6-506c95158722","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fc6cec25-5c4b-4c9b-a4d1-e210853cadf3","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"94624d3a-6050-4947-8869-1ab279d54546","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6542a735-f5fb-4632-81bd-089ad7603148","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cfd73098-ee33-4555-b918-7cdcf95faca9","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3d3bec33-4ae3-4875-8c80-71d3cf40cc3a","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a3e1b92e-2ef8-4e33-87ce-ccb8534f90fb","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6851bd99-a3a2-4da6-9248-2c302d0d9df7","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"70d772c4-37c0-4cd3-8003-fe6a01ebe081","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e36eb787-95c3-4a96-aeb9-7e671b2bda00","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"72f2eb5a-8c14-47c9-9766-3f77f6968aaf","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3ce66452-8320-43b7-9837-55a809d1f0a3","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"68d0a1fb-f092-4f37-afac-a92166924fc2","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"880aa559-fb2b-4017-836b-4f2e212e10ad","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6bf83922-d0e2-41aa-9053-0db5590f3986","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cc0b10b7-fa40-4b1c-a103-42bef5b50111","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d5b55477-58a0-4f32-82fa-255d2111476b","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bfe5c33a-5077-42fc-b5eb-3730846edc38","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"834496d7-a822-4a66-806c-c873bb629150","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8536440b-2d0c-4f1c-8c28-bbfc9cf953c8","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7bd02f6c-d6a4-496f-9d58-20125144cd61","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"53fcb1ea-5ae1-46bf-86ae-313d5db5cef7","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6f01dfae-cfee-451c-b2fa-f7f830fcd9f3","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cfd630c3-5747-44cd-b27f-13a2d6b5e6f6","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"05c7219e-7726-4a5a-9c5f-caa77490e534","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"660e492e-4629-4c67-82bb-72355fd21603","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ded90eb0-1432-4904-9b6f-9aa774078350","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"69af468d-2ef8-45e0-9606-a75ec2ec811e","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0c7c54a1-5851-43e4-8584-70543839d174","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3b5d6ab1-dd43-429a-89c5-1ee50a24fa13","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c1f9ba9c-3c0c-4b74-9b81-b7170ba93fe4","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3318bede-f404-4397-a337-f017394d32c5","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fa85e929-bb6e-42d4-a13b-138687112ede","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3ad050d7-99d0-4246-8a6d-40bbb3e75190","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"daca560d-7b88-459c-be73-59f418f2cd2a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"87341017-7fc9-477d-bd8f-1dfa48b83e89","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c8bb66ab-bc88-48ee-a469-f76f2a8974fd","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2e5259b7-a45b-4156-92a2-29ce3ac11b03","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bff7bfa0-e2bb-4cc7-a182-1fae85ef8a8e","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1854556f-0291-4d6d-a464-4c4d7a857a61","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bf9a4684-4119-487b-8f8b-f99901d5673f","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e473be14-1f37-4d14-91d8-5e2e74072451","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4ee99fb8-56d6-4554-978c-2cfa7910380d","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"32e12006-eb5c-4bfd-96f9-a89d71302a57","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7490a3bd-66ea-4021-8fd3-a99c9963caef","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3e681720-d9ad-499a-a193-0b03b34c5eed","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0e969e4e-7371-4ac9-a84c-513605989d2a","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3bc214a5-803c-4f75-b0b7-d7bd289ac2f7","Key":30.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b976499e-e592-41cd-b330-82a4bea3f42e","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0c315c08-de7a-4ffe-92f8-b7f146ab2626","Key":31.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"63510e3e-99f1-4770-9edf-dca1c6bcbfff","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 32,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Food_3_Crate","path":"sprites/spr_Food_3_Crate/spr_Food_3_Crate.yy",},
    "resourceVersion": "1.3",
    "name": "spr_Food_3_Crate",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"27d8b478-b1e0-4d04-b3a2-2bcdc73dcb1a","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Trees and Crops",
    "path": "folders/Sprites/Trees and Crops.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Food_3_Crate",
  "tags": [],
  "resourceType": "GMSprite",
}