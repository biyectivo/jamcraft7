{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 3,
  "bbox_right": 1148,
  "bbox_top": 27,
  "bbox_bottom": 632,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 1152,
  "height": 672,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"8f690096-2a1e-463d-acd3-52c767fe9f58","path":"sprites/slice_Hairstyle_13_48x48_01/slice_Hairstyle_13_48x48_01.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8f690096-2a1e-463d-acd3-52c767fe9f58","path":"sprites/slice_Hairstyle_13_48x48_01/slice_Hairstyle_13_48x48_01.yy",},"LayerId":{"name":"c2c3a02e-ba1f-493d-a344-9897b762d78e","path":"sprites/slice_Hairstyle_13_48x48_01/slice_Hairstyle_13_48x48_01.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"slice_Hairstyle_13_48x48_01","path":"sprites/slice_Hairstyle_13_48x48_01/slice_Hairstyle_13_48x48_01.yy",},"resourceVersion":"1.0","name":"8f690096-2a1e-463d-acd3-52c767fe9f58","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"slice_Hairstyle_13_48x48_01","path":"sprites/slice_Hairstyle_13_48x48_01/slice_Hairstyle_13_48x48_01.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"dba0e9a8-b830-42b1-8599-4eeb3d98ac28","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8f690096-2a1e-463d-acd3-52c767fe9f58","path":"sprites/slice_Hairstyle_13_48x48_01/slice_Hairstyle_13_48x48_01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 576,
    "yorigin": 336,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"slice_Hairstyle_13_48x48_01","path":"sprites/slice_Hairstyle_13_48x48_01/slice_Hairstyle_13_48x48_01.yy",},
    "resourceVersion": "1.3",
    "name": "slice_Hairstyle_13_48x48_01",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"c2c3a02e-ba1f-493d-a344-9897b762d78e","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "People",
    "path": "folders/Sprites/People.yy",
  },
  "resourceVersion": "1.0",
  "name": "slice_Hairstyle_13_48x48_01",
  "tags": [],
  "resourceType": "GMSprite",
}