if (!surface_exists(surface_old_room)) {
	surface_old_room = surface_create(Game.adjusted_window_width, Game.adjusted_window_height);
	surface_copy(surface_old_room, 0, 0, application_surface);
}