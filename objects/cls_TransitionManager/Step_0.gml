if (room_exists(destination_room)) {
	// Set alarm
	if (alarm[0] == -1) {
		alarm[0] = transition_time;	
	}	
	// Go to destination room
	if (room != destination_room) {
		room_goto(destination_room);
	}
	else {
		// Process transition
		if (!surface_exists(surface_new_room)) {
			surface_new_room = surface_create(Game.adjusted_window_width, Game.adjusted_window_height);
			surface_copy(surface_new_room, 0, 0, application_surface);
			surface_save(surface_old_room, "old.png");
			surface_save(surface_new_room, "new.png");
		}
		
	}
}