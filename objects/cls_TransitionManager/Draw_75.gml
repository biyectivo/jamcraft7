/*
var _slide_pct = 1-alarm[0]/transition_time;
var _slide = _slide_pct * DISPLAY_WIDTH;
if (surface_exists(surface_old_room)) {
	draw_surface(surface_old_room, 0-_slide, 0);
}
if (surface_exists(surface_new_room)) {
	draw_surface(surface_new_room, DISPLAY_WIDTH-_slide, 0);	
}
*/
if (room == destination_room && surface_exists(surface_new_room)) {
	fnc_BackupDrawParams();

	draw_rectangle_color(0, 0, Game.adjusted_window_width, Game.adjusted_window_height, c_black, c_black, c_black, c_black, false);

	if (alarm[0] < transition_time/2) { // Display new room	
		var _alpha = clamp(abs(alarm[0]/(transition_time/2) - 1), 0, 1);
		draw_set_alpha(_alpha);
		show_debug_message("Draw new at "+string(_alpha));
		if (surface_exists(surface_new_room)) {
			draw_surface(surface_new_room, 0, 0);
		}	
	}
	else { // Display old room
		var _alpha = clamp(alarm[0]/(transition_time/2) - 1, 0, 1);
		draw_set_alpha(_alpha);
		show_debug_message("Draw old at "+string(_alpha));
		if (surface_exists(surface_old_room)) {
			draw_surface(surface_old_room, 0, 0);
		}	
	}
	fnc_RestoreDrawParams();
}