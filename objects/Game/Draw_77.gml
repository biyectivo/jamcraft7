/// @description Draw Game

// Disable alpha blending 
gpu_set_blendenable(false);

shader_set(shd_BriConSatGamma);
var _shd_bri = shader_get_uniform(shd_BriConSatGamma, "brightness");
var _shd_con = shader_get_uniform(shd_BriConSatGamma, "contrast");
var _shd_sat = shader_get_uniform(shd_BriConSatGamma, "saturation");
var _shd_gamma = shader_get_uniform(shd_BriConSatGamma, "gamma");
var _shd_monochrome = shader_get_uniform(shd_BriConSatGamma, "monochrome");
shader_set_uniform_f(_shd_bri, values[0]);
shader_set_uniform_f(_shd_con, values[1]);
shader_set_uniform_f(_shd_sat, values[2]);
shader_set_uniform_f(_shd_gamma, values[3]);
shader_set_uniform_f(_shd_monochrome, is_monochrome);

// Draw app surface
if (SELECTED_SCALING == SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW) {
	//if (window_get_fullscreen()) {
	if (is_fullscreen()) {
		Game.gui_offset_x = (DISPLAY_WIDTH - adjusted_resolution_width)/2;
		Game.gui_offset_y = (DISPLAY_HEIGHT - adjusted_resolution_height)/2;
		Game.application_surface_scaling = 1;
		var _factorw = DISPLAY_WIDTH / adjusted_resolution_width;
		var _factorh = DISPLAY_HEIGHT / adjusted_resolution_height;		
	}
	else {
		Game.gui_offset_x = (adjusted_window_width - adjusted_resolution_width)/2;
		Game.gui_offset_y = (adjusted_window_height - adjusted_resolution_height)/2;	
		Game.application_surface_scaling = 1;
		var _factorw = adjusted_window_width / adjusted_resolution_width;
		var _factorh = adjusted_window_height / adjusted_resolution_height;
	}
	draw_surface_ext(application_surface, Game.gui_offset_x, Game.gui_offset_y, Game.application_surface_scaling, Game.application_surface_scaling, 0, c_white, 1);	

	display_set_gui_size(adjusted_window_width, adjusted_window_height);
	display_set_gui_maximize(1, 1, 0, 0);
	
}
else if (SELECTED_SCALING == SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW_SCALED) {
	//if (window_get_fullscreen()) {
	if (is_fullscreen()) {
		var _factorw = DISPLAY_WIDTH / adjusted_resolution_width;
		var _factorh = DISPLAY_HEIGHT / adjusted_resolution_height;	
		if (RESOLUTION_MAXIMIZED_SCALING_FACTOR == -1) {
			Game.application_surface_scaling = min(_factorw, _factorh);
		}
		else {
			Game.application_surface_scaling = min(RESOLUTION_MAXIMIZED_SCALING_FACTOR, _factorw, _factorh);
		}
		Game.gui_offset_x = (DISPLAY_WIDTH - adjusted_resolution_width*Game.application_surface_scaling)/2;
		Game.gui_offset_y = (DISPLAY_HEIGHT - adjusted_resolution_height*Game.application_surface_scaling)/2;		
	}
	else {
		var _factorw = adjusted_window_width / adjusted_resolution_width;
		var _factorh = adjusted_window_height / adjusted_resolution_height;
		if (RESOLUTION_MAXIMIZED_SCALING_FACTOR == -1) {
			Game.application_surface_scaling = min(_factorw, _factorh);
		}
		else {
			Game.application_surface_scaling = min(RESOLUTION_MAXIMIZED_SCALING_FACTOR, _factorw, _factorh);
		}
		Game.gui_offset_x = (adjusted_window_width - adjusted_resolution_width*Game.application_surface_scaling)/2;
		Game.gui_offset_y = (adjusted_window_height - adjusted_resolution_height*Game.application_surface_scaling)/2;		
	}
	
	draw_surface_ext(application_surface, Game.gui_offset_x, Game.gui_offset_y, Game.application_surface_scaling, Game.application_surface_scaling, 0, c_white, 1);		
	
	// GUI layer - workaround!!! TODO: fix this
	display_set_gui_maximize();	
	
}
else {
	Game.gui_offset_x = 0;
	Game.gui_offset_y = 0;
	//if (window_get_fullscreen()) {
	if (is_fullscreen()) {
		var _factorw = DISPLAY_WIDTH / adjusted_resolution_width;
		var _factorh = DISPLAY_HEIGHT / adjusted_resolution_height;		
		Game.application_surface_scaling = min(_factorw, _factorh);		
	}
	else {
		var _factorw = adjusted_window_width / adjusted_resolution_width;
		var _factorh = adjusted_window_height / adjusted_resolution_height;
		Game.application_surface_scaling = min(_factorw, _factorh);		
	}
	display_set_gui_size(adjusted_window_width, adjusted_window_height);
	display_set_gui_maximize();
}


// Re-enable alpha blending 
gpu_set_blendenable(true);

if (ENABLE_LIVE && live_call()) return live_result;

if (room == room_Game_1) {
	// Day-night cycle
	var _w = adjusted_window_width;
	var _h = adjusted_window_height;

	var _ambient_light = [];
	array_push(_ambient_light, [6, $000033, 0.8]);
	array_push(_ambient_light, [10, c_aqua, 0.5]);
	array_push(_ambient_light, [14, c_yellow, 0.25]);
	array_push(_ambient_light, [20, c_orange, 0.25]);
	array_push(_ambient_light, [24, c_black, 0.8]);

	var _idx = 0;
	var _last_idx = array_length(_ambient_light)-1;
	while (time > _ambient_light[_idx][0]) {
		_idx++;
		_last_idx = _idx-1;
		if (_idx == array_length(_ambient_light)) {
			_idx = 0;	
			_last_idx = array_length(_ambient_light)-1;
		}
	}

	var _denominator = abs(_ambient_light[_last_idx][0] % 24 - _ambient_light[_idx][0]);
	var _numerator = time - _ambient_light[_last_idx][0] % 24;

	// Interpolate
	var _alpha = _ambient_light[_last_idx][2]*(1-_numerator/_denominator)+_ambient_light[_idx][2]*_numerator/_denominator;
	draw_set_alpha(_alpha);
	draw_set_color(merge_color(_ambient_light[_last_idx][1], _ambient_light[_idx][1], _numerator/_denominator));
	draw_rectangle(0, 0, _w, _h, false);

	//show_debug_message("Interpolating between "+string(_ambient_light[_last_idx][0] % 24)+" and "+string(_ambient_light[_idx][0])+", time is "+string(time)+" so "+string(_numerator)+"/"+string(_denominator)+" = "+string(_numerator/_denominator)+ " ... alpha="+string(_alpha));
}
draw_set_alpha(1);

shader_reset();


