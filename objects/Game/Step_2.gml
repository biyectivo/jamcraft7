//*****************************************************************************
// Handle camera
//*****************************************************************************

if (ENABLE_LIVE && live_call()) return live_result;

if (!Game.paused) {	
	
	// Depth
	with (all) {
		depth = -y;	
	}
	
	
	if (room == room_Game_1 || room == room_IceCreamShop_Small || room == room_IceCreamShop_Medium || room == room_IceCreamShop_Large) {
		if (instance_exists(camera_target)) {
			
			// Get current camera position
			var _current_camera_x = camera_get_view_x(VIEW);
			var _current_camera_y = camera_get_view_y(VIEW);
	
			// Calculate offset for screen shake			
			var _offsetx = 0;
			var _offsety = 0;
			if (camera_shake) {
				var _offsetx = irandom_range(-SCREENSHAKE_STRENGTH,SCREENSHAKE_STRENGTH);
				var _offsety = irandom_range(-SCREENSHAKE_STRENGTH,SCREENSHAKE_STRENGTH);
			}
			
			// Get target camera position, centered around the target and lerped with camera smoothness
			var _destx = clamp(camera_target.x-adjusted_resolution_width/2, 0, room_width-adjusted_resolution_width);
			var _desty = clamp(camera_target.y-adjusted_resolution_height/2, 0, room_height-adjusted_resolution_height);
			
			if (camera_lerp) {
				var _target_camera_x = lerp(_current_camera_x, _destx, camera_smoothness) + _offsetx;
				var _target_camera_y = lerp(_current_camera_y, _desty, camera_smoothness) + _offsety;
			}
			else {
				var _target_camera_x = _destx;
				var _target_camera_y = _desty;				
				camera_lerp = true;
			}
			// Move camera
			camera_set_view_pos(VIEW, _target_camera_x, _target_camera_y);
			
		}

	}
	
}

//*****************************************************************************
// Detect primary gamepad
//*****************************************************************************

var _maxpads = gamepad_get_device_count();
Game.primary_gamepad = -1;
var _i=0; 
while (_i < _maxpads && Game.primary_gamepad == -1) {
	Game.primary_gamepad = gamepad_is_connected(_i) ? _i : -1;
	_i++;
}