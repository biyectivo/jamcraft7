// Hide collision layer	
if (layer_exists(layer_get_id("lyr_Tile_Collision"))) {
	layer_set_visible(layer_get_id("lyr_Tile_Collision"), debug);
}

seconds_elapsed = floor(current_time/1000*GAME_FPS);

// Room-specific code
if (!Game.paused) {
	if (array_length(asset_get_tags(room, asset_room)) == 0) { // Game room
		
		// Register swipe/drag	
		device_mouse_dragswipe_register(0);
		
		// Global step
		if (instance_exists(player_instance)) {
			current_step++;	// Time of day
			
			if (current_step % fifteen_minute_factor == 0) {
				time = time + 0.25;
			}
			
			if (time >= 24) {
				time = 0;	
				day++;
				
				if ((day-1) % days_per_season == 0) {
					current_season = (current_season+1) % array_length(seasons);
				}
				
			}
		}		
		
		// Drag and swipe checks
		
		for (var _i=0; _i<3; _i++) {
			var _x = device_mouse_check_dragswipe_start(_i);
			var _y = device_mouse_check_dragswipe_end(_i);
			var _z = device_mouse_check_dragswipe(_i);
			if (_y) {
				var _d = device_mouse_dragswipe_distance(_i);
				var _t = device_mouse_dragswipe_time(_i);
				var _s = device_mouse_dragswipe_speed(_i);
			}
		}
		
		#region God Mode
		
		// DEBUG MOUSE FUNCTIONS
		if (Game.debug) {
			if (device_mouse_check_button_pressed(0, mb_middle)) index_selected = (index_selected+1) % array_length(values);
			if (mouse_wheel_down()) values[index_selected] = clamp(values[index_selected]+0.05, 0, 1);
			if (mouse_wheel_up()) values[index_selected] = clamp(values[index_selected]-0.05, 0, 1);
			if (device_mouse_check_button_pressed(0, mb_right)) is_monochrome = !is_monochrome;
		
			if (keyboard_check_pressed(ord("0"))) {
				var _id = instance_create_layer(-10, -10, "lyr_Controllers", cls_Transition);
				with (_id) {
					destination_room = room_Game_1;
					event_perform(ev_other, ev_user0);
				}
				player_instance.x = 688;
				player_instance.y = 1296;
			}
			if (keyboard_check_pressed(ord("1"))) {
				/*var _id = instance_create_layer(-10, -10, "lyr_Controllers", cls_TransitionManager);
				_id.destination_room = room_IceCreamShop_Small;*/
				var _id = instance_create_layer(-10, -10, "lyr_Controllers", cls_Transition);
				with (_id) {
					destination_room = room_IceCreamShop_Small;
					event_perform(ev_other, ev_user0);
				}
				player_instance.x = 400;
				player_instance.y = 300;
			}
			if (keyboard_check_pressed(ord("2"))) {			
				var _id = instance_create_layer(-10, -10, "lyr_Controllers", cls_Transition);
				with (_id) {
					destination_room = room_IceCreamShop_Medium;
					event_perform(ev_other, ev_user0);
				}
				player_instance.x = 400;
				player_instance.y = 300;
			}
			if (keyboard_check_pressed(ord("3"))) {
				var _id = instance_create_layer(-10, -10, "lyr_Controllers", cls_Transition);
				with (_id) {
					destination_room = room_IceCreamShop_Large;
					event_perform(ev_other, ev_user0);
				}
				player_instance.x = 400;
				player_instance.y = 300;
			}
			
			
			if (keyboard_check_pressed(ord("T"))) { // tree
				placing_tree = !placing_tree;
			}
			if (keyboard_check_pressed(ord("C"))) { // crop
				placing_crop = !placing_crop;
			}
			
			if (placing_tree) {
				if (TAP) {
					var _id = instance_create_layer(ROOM_MOUSE_X, ROOM_MOUSE_Y, "lyr_Instances", obj_Tree);
					placing_tree = false;
				}
			}
			
			if (placing_crop) {
				if (TAP) {
					var _id = instance_create_layer(ROOM_MOUSE_X, ROOM_MOUSE_Y, "lyr_Instances", obj_Crop);
					placing_crop = false;
				}
			}
			
			
			if (keyboard_check_pressed(vk_space)) {
				with (obj_Player) {
					randomize_favorite();
				}
			}
			
			if (keyboard_check_pressed(vk_enter)) {
				var _id = instance_create_layer(ROOM_MOUSE_X, ROOM_MOUSE_Y, "lyr_Instances", obj_Player);
				with (_id) {
					is_ai = true;
				}
			}
		}	
		
		#endregion
		
	}
	
	if (keyboard_check_pressed(ord("U"))) { // sample UI
		draw_toolbar = !draw_toolbar;				
	}
}



