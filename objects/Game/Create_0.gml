//*****************************************************************************
// Global variable declarations
//*****************************************************************************

debug = false;
show_debug_info = false;
show_debug_pathfinding = false;
//username = "Player";

//*****************************************************************************
// Randomize
//*****************************************************************************
randomize();
//random_set_seed(1234);

//*****************************************************************************
// Set camera target
//*****************************************************************************

player_instance = noone;
camera_target = noone;
camera_shake = false;
camera_smoothness = 0.08;
camera_lerp = true;

//*****************************************************************************
// Menu item definitions
//*****************************************************************************

menu_items = array_create(4);
menu_items[0] = "Start game";
menu_items[1] = "Help";
menu_items[2] = "Options";
menu_items[3] = "Credits";
menu_items[4] = "Quit";

option_items = array_create(4);
option_items[0] = "Music";
option_items[1] = "Sounds";
option_items[2] = "Volume";
option_items[3] = "Fullscreen";
option_items[4] = "Controls";
option_items[5] = "Name";

control_indices = [];
controls = ds_map_create();
control_names = ds_map_create();

control_indices[0] = "left";
control_indices[1] = "right";
control_indices[2] = "up";
control_indices[3] = "down";

controls[? "left"] = ord("A");
controls[? "right"] = ord("D");
controls[? "up"] = ord("W");
controls[? "down"] = ord("S");


control_names[? "left"] = "Move Left";
control_names[? "right"] = "Move Right";
control_names[? "up"] = "Move Up";
control_names[? "down"] = "Move Down";

name_being_modified = false;

wait_for_input = false;
key_being_remapped = noone;

option_type = ds_map_create();
ds_map_add(option_type, option_items[0], "toggle");
ds_map_add(option_type, option_items[1], "toggle");
ds_map_add(option_type, option_items[2], "slider");
ds_map_add(option_type, option_items[3], "toggle");
ds_map_add(option_type, option_items[4], "");
ds_map_add(option_type, option_items[5], "input");

option_value = ds_map_create();
ds_map_add(option_value, option_items[0], true);
ds_map_add(option_value, option_items[1], true);
ds_map_add(option_value, option_items[2], 1.0);
ds_map_add(option_value, option_items[3], false);
ds_map_add(option_value, option_items[4], noone);
ds_map_add(option_value, option_items[5], "Player");

credits[0] = "[fnt_Menu][fa_middle][fa_center][c_white]2021[c_yellow]Programming: [c_white]biyectivo";
credits[1] = "[fa_middle][fa_center][c_white][c_white](Jose Alberto Bonilla Vera)";

game_title = "Game1";
scoreboard_game_id = "Game1";

primary_gamepad = -1;
start_drag_drop = false;

// Particle system and emitter variables - define to avoid not set
particle_system = noone;
particle_emitter_fire = noone;

pause_screenshot = noone;

gui_offset_x = 0;
gui_offset_y = 0;
application_surface_scaling = 1;


//*****************************************************************************
// Data structures
//*****************************************************************************
grid = noone;
grid_ai = noone;

// NPC data structures
Person = function() constructor {
	
	#region Stats
		gender = choose(GENDERS.MALE, GENDERS.FEMALE);
		name = gender == GENDERS.MALE ? fnc_ChooseProb(Game.male_names, []) : fnc_ChooseProb(Game.female_names, []);
	#endregion
	
	#region Sprites
	
		body_sprite = -1;
		outfit_sprite = -1;
		hairstyle_sprite = -1;
		accessory_sprite = -1;
	
		_body_sprite_string = "";
		_outfit_sprite_string = "";
		_hairstyle_sprite_string = "";
		_accessory_sprite_string = "";
		
		do {
			body_spr_index = irandom_range(1, 4);
			_body_sprite_string = "slice_Body_"+string(CHARACTER_SIZE)+"x"+string(CHARACTER_SIZE)+"_0"+string(body_spr_index);				
			//show_debug_message(_body_sprite_string);
		}
		until (asset_get_index(_body_sprite_string) != -1);	
		body_sprite = asset_get_index(_body_sprite_string);
	
		outfit_spr_index = irandom_range(1, 33);
		do {
			outfit_variation_spr_index = irandom_range(1, 9);
			_outfit_sprite_string = "slice_Outfit_"+(outfit_spr_index < 10 ? "0" : "")+string(outfit_spr_index)+"_"+string(CHARACTER_SIZE)+"x"+string(CHARACTER_SIZE)+"_"+(outfit_variation_spr_index<10 ? "0" : "")+string(outfit_variation_spr_index);
		}
		until (asset_get_index(_outfit_sprite_string) != -1);
		outfit_sprite = asset_get_index(_outfit_sprite_string);
		
		hairstyle_spr_index = irandom_range(1, 29);
		do {
			hairstyle_variation_spr_index = irandom_range(1, 9);	
			_hairstyle_sprite_string = "slice_Hairstyle_"+(hairstyle_spr_index < 10 ? "0" : "")+string(hairstyle_spr_index)+"_"+string(CHARACTER_SIZE)+"x"+string(CHARACTER_SIZE)+"_"+(hairstyle_variation_spr_index<10 ? "0" : "")+string(hairstyle_variation_spr_index);		
		}
		until (asset_get_index(_hairstyle_sprite_string) != -1);
		hairstyle_sprite = asset_get_index(_hairstyle_sprite_string);	
	
		if (random(1) < 0.65) {
			accessory_spr_index = choose(3,4,8,15,17);
			do {
				accessory_variation_spr_index = irandom_range(1, 9);	
				_accessory_sprite_string = "slice_Accessory_"+(accessory_spr_index < 10 ? "0" : "")+string(accessory_spr_index)+"_"+string(CHARACTER_SIZE)+"x"+string(CHARACTER_SIZE)+"_"+(accessory_variation_spr_index<10 ? "0" : "")+string(accessory_variation_spr_index);
			}
			until (asset_get_index(_accessory_sprite_string) != -1);
			accessory_sprite = asset_get_index(_accessory_sprite_string);				
		}
	#endregion
	
	#region Order/Favorite
		order = function(_ice_cream, _ice_cream_type, _topping, _syrup, _cone, _popsicle, _order_description) constructor {
			order_icecream = _ice_cream; // Can combine gelato and sorbet
			order_icecream_type = _ice_cream_type;
			order_popsicle = _popsicle;	
			order_topping = _topping;
			order_syrup = _syrup;
			order_cone = _cone;	
			order_description = _order_description;
		}

		function randomize_salutation() {
			salutation = choose("Hey, how's business?", "Hiya!", "How's it going?", "Hi.", "Hola!", "What's up?", "Good day.", "'Sup.");
			order_intro = choose("I'll have ", "I want ", "Can I have ", "Can I get ", "Today I'll have ");
		}
		randomize_salutation();
	
		function randomize_favorite() {
	
			favorite_order_type = irandom_range(0,1); // 0 = helado, 1 = paleta
			var _icecream_order = [-1, -1, -1];
			var _icecream_type_order = [-1, -1, -1];
			var _popsicle_order = -1;
			var _topping_order = -1;
			var _syrup_order = -1;
			var _cone_order = -1;
			var _order_description = "";
			var _part = "";
			var _link_color = "[$F3BA35]";
	
		
	
			if (favorite_order_type == 0) {		
				var _n = fnc_ChooseProb([], [0.4, 0.35, 0.25])+1;
				for (var _i=0; _i<_n; _i++) {
					_order_description += "a scoop of ";
			
					if (random(1) < 0.5) {
						_icecream_order[_i] = irandom_range(0, array_length(Game.gelato_types)-1);
						_icecream_type_order[_i] = ORDER_ELEMENT_TYPES.GELATO;				
						_order_description += _link_color+Game.gelato_types[_icecream_order[_i]].name + " gelato[c_white]";				
					}
					else {
						_icecream_order[_i] = irandom_range(0, array_length(Game.sorbet_types)-1);
						_icecream_type_order[_i] = ORDER_ELEMENT_TYPES.SORBET;
						_order_description += _link_color+Game.sorbet_types[_icecream_order[_i]].name + " sorbet[c_white]";				
					}
					if (_i < _n-2) {
						_order_description += ", ";
					}
					else if (_i < _n-1) {
						_order_description += " and ";
					}
					_order_description += "|";
				}
		
				if (random(1) < 0.6) {
					var _cone_order = irandom_range(0, array_length(Game.cone_types)/2-1);			
				}
				else {
					var _cone_order = array_length(Game.cone_types)/2 + irandom_range(0, array_length(Game.cone_types)/2-1);
				}
				_order_description += "on a "+_link_color+Game.cone_types[_cone_order].name+"[c_white]";
			}
			else {
				_popsicle_order = irandom_range(0, array_length(Game.popsicle_types)-1);
				_order_description += "a "+_link_color+Game.popsicle_types[_popsicle_order].name + " popsicle[c_white]";
			}
	
			var _u = random(1);
			if (_u < 0.4) { // Add syrup and topping
				var _syrup_order = irandom_range(0, array_length(Game.syrup_types)-1);
				var _topping_order = irandom_range(0, array_length(Game.topping_types)-1);
				_order_description += "|with "+_link_color+Game.syrup_types[_syrup_order].name+"[c_white] and|"+_link_color+Game.topping_types[_topping_order].name+"[c_white] on top";
			}
			else if (_u < 0.6) { // Add syrup
				var _syrup_order = irandom_range(0, array_length(Game.syrup_types)-1);
				_order_description += "|with "+_link_color+Game.syrup_types[_syrup_order].name+"[c_white]";
			}
			else if (_u < 0.8) { // Add topping
				var _topping_order = irandom_range(0, array_length(Game.topping_types)-1);
				_order_description += "|with "+_link_color+Game.topping_types[_topping_order].name+"[c_white] on top";
			}
	
			_order_description += choose(",|please!", ".|Thanks.", ",|if you will?");
			favorite_order = new order(_icecream_order, _icecream_type_order, _topping_order, _syrup_order, _cone_order, _popsicle_order, _order_description);
		}
	
		randomize_favorite();	
	#endregion
	
	active_room = noone;
}

people = [];


//*****************************************************************************
// Shader stuff
//*****************************************************************************

#region Shaders
	// Bri/Con/Sat/Gamma
	values = [1, 1, 1, 0.75]
	value_names = ["Brightness", "Contrast", "Saturation", "Gamma"];
	index_selected = 0;
	is_monochrome = false;
#endregion

people_generated = false;


fnc_InitializeGameStartVariables();
show_debug_overlay(Game.debug);

