// Create player
if (!instance_exists(player_instance)) {
	player_instance = instance_create_layer(0, 0, "lyr_Instances", obj_Player);
	camera_target = player_instance;
	camera_lerp = false;
	//player_instance.spawn_point = "HomeOutside";
	player_instance.spawn_point = "IceCreamOutside";	
	//show_debug_message("Creating player in room "+room_get_name(room)+" at "+string(player_instance.x)+","+string(player_instance.y)+"...");
}
else {
	show_debug_message("Player already exists at "+string(player_instance.x)+","+string(player_instance.y));
}

// Enable views and set up graphics
view_enabled = true;
view_visible[0] = true;
fnc_SetGraphics();

// Stop all sounds and set up sound volume
audio_stop_all();
audio_master_gain(option_value[? "Volume"]);	


if (room == room_Game_1 || room == room_IceCreamShop_Small || room_IceCreamShop_Medium || room_IceCreamShop_Large) {
	
	if (!people_generated /*&& seconds_elapsed % 30 == 0*/) {		
		var _n = 100;
		repeat (_n) {
			array_push(people, new Person());
		}
		people_generated = true;
		
		for (var _i=0; _i<_n; _i++) {
			show_debug_message("Person "+str(_i)+" is "+people[_i].name+" of gender "+str(people[_i].gender)+" with order ["+people[_i].favorite_order.order_description+"] and sprites "+people[_i]._body_sprite_string+" "+people[_i]._outfit_sprite_string+" "+people[_i]._hairstyle_sprite_string+" "+people[_i]._accessory_sprite_string);
		}
	}

	// Define pathfinding grid
	
	if (ds_exists(grid, ds_type_grid)) ds_grid_destroy(grid);
	if (ds_exists(grid_ai, ds_type_grid)) ds_grid_destroy(grid_ai);
	
	grid = mp_grid_create(0, 0, room_width/GRID_RESOLUTION, room_height/GRID_RESOLUTION, GRID_RESOLUTION, GRID_RESOLUTION);
	grid_ai = mp_grid_create(0, 0, room_width/GRID_RESOLUTION, room_height/GRID_RESOLUTION, GRID_RESOLUTION, GRID_RESOLUTION);
				
	// Add collision tiles to grid			
	for (var _col = 0; _col < room_width/GRID_RESOLUTION; _col++) {
		for (var _row = 0; _row < room_height/GRID_RESOLUTION; _row++) {				
			// Tiles nobody can walk over
			if (tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), _col*GRID_RESOLUTION, _row*GRID_RESOLUTION) == 1) {
				mp_grid_add_cell(grid, _col, _row);
				mp_grid_add_cell(grid_ai, _col, _row);
			}
			
			// Tiles AI cannot walk over
			if (tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), _col*GRID_RESOLUTION, _row*GRID_RESOLUTION) == 2) {
				mp_grid_add_cell(grid_ai, _col, _row);
			}
		}
	}
	with (cls_Collisionable) {
		mp_grid_add_instances(Game.grid, id, false);
		mp_grid_add_instances(Game.grid_ai, id, false);
	}
	
	// Initialize particle system and emitters
	
	particle_system = part_system_create_layer("lyr_Particles", true);
	particle_emitter_fire = part_emitter_create(particle_system);
	
	// Initialize player
	/*
	if (instance_exists(obj_Player)) {
		with (obj_Player) {
			event_perform(ev_other, ev_user0);
		}
	}
	*/
	
	// (Re)initialize game start variables		
	fnc_InitializeGameStartVariables();
	
	
	// Play music	
	if (option_value[? "Music"]) {
		//music_sound_id = audio_play_sound(snd_Music, 1, true);
	}
		
}
