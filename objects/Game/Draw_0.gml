if (ENABLE_LIVE && live_call()) return live_result;
fnc_BackupDrawParams();

// Debug paths

if (show_debug_pathfinding) {
	draw_set_alpha(0.3);
	mp_grid_draw(Game.grid);
	draw_set_alpha(1);
	for (var _i=0; _i<instance_number(obj_Player); _i++) {	
		var _id = instance_find(obj_Player, _i);
		if (_id.is_ai) { var _col = c_lime; } else { var _col = c_red; }
		draw_circle_color(_id.target_x, _id.target_y, 5, _col, _col, false);
		if (path_exists(_id.path)) draw_path(_id.path, _id.x, _id.y, false);
	}
}

if (placing_tree) {
	draw_sprite_ext(spr_FruitTrees, 0, ROOM_MOUSE_X, ROOM_MOUSE_Y, 2, 2, 0, c_white, 0.3);
}
if (placing_crop) {
	draw_sprite_ext(spr_Crops, 2, ROOM_MOUSE_X, ROOM_MOUSE_Y, 2, 2, 0, c_white, 0.3);
}
	
	
fnc_RestoreDrawParams();
