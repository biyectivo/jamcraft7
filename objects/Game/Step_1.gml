//  Handle fullscreen change
if (!BROWSER && keyboard_check(vk_lalt) && keyboard_check_pressed(vk_enter)) {
	fnc_SetGraphics();
}

if (BROWSER && keyboard_check_pressed(vk_f10)) {
	game_is_fullscreen = !game_is_fullscreen;
	fnc_SetGraphics();	
}
