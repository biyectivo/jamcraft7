draw_self();

// Tree name/data on mouseover
if (position_meeting(ROOM_MOUSE_X, ROOM_MOUSE_Y, self)) {
	type_formatted(ROOM_MOUSE_X+1, ROOM_MOUSE_Y-10+1, "[fnt_GUI][scale, 0.25][c_black][fa_center][fa_bottom]"+tree_struct.name+" Tree");	
	type_formatted(ROOM_MOUSE_X, ROOM_MOUSE_Y-10, "[fnt_GUI][scale, 0.25][c_white][fa_center][fa_bottom]"+tree_struct.name+" Tree");	
}