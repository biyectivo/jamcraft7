if (!Game.paused) {
	// Update image based on stage
	var _n_growth = array_length(tree_struct.growth_stage_idx);
	var _n_yield = array_length(tree_struct.yield_stage_idx);
	var _n_die = array_length(tree_struct.die_stage_idx);
	var _n_current;
	switch (stage) {
		case STAGES.GROWTH:
			image_index = tree_struct.growth_stage_idx[stage_index];
			_n_current = _n_growth;
			break;
		case STAGES.YIELD:
			image_index = tree_struct.yield_stage_idx[stage_index];
			_n_current = _n_yield;
			break;
		case STAGES.DIE:
			image_index = tree_struct.die_stage_idx[stage_index];
			_n_current = _n_die;
			break;
	}
	
	if (keyboard_check_pressed(vk_space)) {
		stage_index = stage_index+1;
		if (stage_index >= _n_current) {
			stage = min(stage+1, 2);
			stage_index = 0;
		}
	}
}