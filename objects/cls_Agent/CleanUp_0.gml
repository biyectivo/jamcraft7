/// @description Clean up the structures
ai_decision_fsm.cleanup();
fsm.cleanup();

if (ds_exists(fsm_state_animation_speed, ds_type_map)) ds_map_destroy(fsm_state_animation_speed);
if (ds_exists(fsm_state_animation_length, ds_type_map)) ds_map_destroy(fsm_state_animation_length);
if (ds_exists(fsm_state_sprite_row, ds_type_map)) ds_map_destroy(fsm_state_sprite_row);
