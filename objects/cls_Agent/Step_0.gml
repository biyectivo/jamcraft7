/// @description Execute FSM
if (!Game.paused) {
	//if (ENABLE_LIVE && live_call()) return live_result;
	
	// FSM
	if (is_ai) {
		ai_decision_fsm.step();
		ai_decision_fsm.transition();
	}
	fsm.step();
	fsm.transition();
	
	// Animation, Facing
	step++;	
	
	#region Lighting	
		/*
		occluder.x     = x;
		occluder.y     = y;
		occluder.angle = image_angle;


		with (flashlight) {
			x = other.x+10;
			y = other.y;
			angle = other.image_angle;	
		}
		if (keyboard_check_pressed(ord("F"))) {
			flashlight.visible = !flashlight.visible;
		}
		*/
	#endregion
	
	
}



