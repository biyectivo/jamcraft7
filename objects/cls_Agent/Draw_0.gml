/// @description Draw player
if (ENABLE_LIVE && live_call()) return live_result;
if (!Game.paused) {
	fnc_BackupDrawParams();
	
	// Shadow
	draw_set_alpha(0.35);
	var _shadow_width = (bbox_right-bbox_left)/2 + 3;
	draw_ellipse_color(x-_shadow_width, y-6, x+_shadow_width, y+6, c_black, c_black, false);
	draw_set_alpha(1);
	
	// Draw character
	
	var _offset_w = CHARACTER_SIZE/2;
	var _offset_h = 2*CHARACTER_SIZE;
	var _scale = TILE_SIZE/CHARACTER_SIZE;
	
	draw_sprite_part_ext(body_sprite, 0, CHARACTER_SIZE*img_index, 2*CHARACTER_SIZE*fsm_state_sprite_row[? fsm.current_state_name], CHARACTER_SIZE, 2*CHARACTER_SIZE, x - _offset_w, y - _offset_h, _scale, _scale, c_white, 1);
	draw_sprite_part_ext(outfit_sprite, 0, CHARACTER_SIZE*img_index, 2*CHARACTER_SIZE*fsm_state_sprite_row[? fsm.current_state_name], CHARACTER_SIZE, 2*CHARACTER_SIZE, x - _offset_w, y - _offset_h, _scale, _scale, c_white, 1);
	draw_sprite_part_ext(hairstyle_sprite, 0, CHARACTER_SIZE*img_index, 2*CHARACTER_SIZE*fsm_state_sprite_row[? fsm.current_state_name], CHARACTER_SIZE, 2*CHARACTER_SIZE, x - _offset_w, y - _offset_h, _scale, _scale, c_white, 1);
	if (sprite_exists(accessory_sprite)) {
		draw_sprite_part_ext(accessory_sprite, 0, CHARACTER_SIZE*img_index, 2*CHARACTER_SIZE*fsm_state_sprite_row[? fsm.current_state_name], CHARACTER_SIZE, 2*CHARACTER_SIZE, x - _offset_w, y - _offset_h, _scale, _scale, c_white, 1);
	}
	
	
	// Character name/data on mouseover
	if (point_in_rectangle(ROOM_MOUSE_X, ROOM_MOUSE_Y, bbox_left, bbox_top, bbox_right, bbox_bottom)) {
		//type_formatted(x+1, y - CHARACTER_SIZE+1, "[fnt_GUI][scale, 0.25][c_black][fa_center][fa_bottom]"+name);	
		//type_formatted(x, y - CHARACTER_SIZE, "[fnt_GUI][scale, 0.25][c_white][fa_center][fa_bottom]"+name);	
		type_formatted(ROOM_MOUSE_X+1, ROOM_MOUSE_Y+1, "[fnt_GUI][scale, 0.25][c_black][fa_center][fa_bottom]"+name);	
		type_formatted(ROOM_MOUSE_X, ROOM_MOUSE_Y, "[fnt_GUI][scale, 0.25][c_white][fa_center][fa_bottom]"+name);	
	}

	// Debug
	if (Game.debug) {
		if (is_ai) {
			type_formatted(x, y-10, "[fnt_Debug][fa_center][c_red]"+ai_decision_fsm.current_state_name+" --> "+fsm.current_state_name+"/"+string(facing)+"/"+string(img_index)+" ("+fsm.previous_state_name()+")");
		}
		else {
			type_formatted(x, y-10, "[fnt_Debug][fa_center][c_white]"+fsm.current_state_name+"/"+string(facing)+"/"+string(img_index)+" ("+fsm.previous_state_name()+")");
		}
		
		draw_set_alpha(0.5);
		draw_rectangle_color(bbox_left, bbox_top, bbox_right, bbox_bottom, c_red, c_red, c_red, c_red, false);
		draw_set_alpha(1);		
	}
	
	
	// Messages
	
	
	fnc_RestoreDrawParams();
}