/// @description Create player
event_inherited();


#region Stats
	step = 0;
	
	gender = choose(GENDERS.MALE, GENDERS.FEMALE);
	name = gender == GENDERS.MALE ? fnc_ChooseProb(Game.male_names, []) : fnc_ChooseProb(Game.female_names, []);
	
	player_speed = 6;	
	is_ai = false;
	controllable = true;
	
	move_confirmed = false;
	move_angle = 0;
	path = noone;
	target_x = -1;
	target_y = -1;
	
	facing = "Down";
	last_facing = "";
	
	spawn_point = "";
	previous_facing = "Down";
	
#endregion

#region Order/Favorite

	// icecream: array(3)
	order = function(_ice_cream, _ice_cream_type, _topping, _syrup, _cone, _popsicle, _order_description) constructor {
		order_icecream = _ice_cream; // Can combine gelato and sorbet
		order_icecream_type = _ice_cream_type;
		order_popsicle = _popsicle;	
		order_topping = _topping;
		order_syrup = _syrup;
		order_cone = _cone;	
		order_description = _order_description;
	}

	my_order = new order([-1, -1, -1], [-1,-1,-1], -1, -1, -1, -1, "");	
	
	function randomize_salutation() {
		salutation = choose("Hey, how's business?", "Hiya!", "How's it going?", "Hi.", "Hola!", "What's up?", "Good day.", "'Sup.");
		order_intro = choose("I'll have ", "I want ", "Can I have ", "Can I get ", "Today I'll have ");
	}
	randomize_salutation();
	
	function randomize_favorite() {
	
		favorite_order_type = irandom_range(0,1); // 0 = helado, 1 = paleta
		var _icecream_order = [-1, -1, -1];
		var _icecream_type_order = [-1, -1, -1];
		var _popsicle_order = -1;
		var _topping_order = -1;
		var _syrup_order = -1;
		var _cone_order = -1;
		var _order_description = "";
		var _part = "";
		var _link_color = "[$F3BA35]";
	
		
	
		if (favorite_order_type == 0) {		
			var _n = fnc_ChooseProb([], [0.4, 0.35, 0.25])+1;
			for (var _i=0; _i<_n; _i++) {
				_order_description += "a scoop of ";
			
				if (random(1) < 0.5) {
					_icecream_order[_i] = irandom_range(0, array_length(Game.gelato_types)-1);
					_icecream_type_order[_i] = ORDER_ELEMENT_TYPES.GELATO;				
					_order_description += _link_color+Game.gelato_types[_icecream_order[_i]].name + " gelato[c_white]";				
				}
				else {
					_icecream_order[_i] = irandom_range(0, array_length(Game.sorbet_types)-1);
					_icecream_type_order[_i] = ORDER_ELEMENT_TYPES.SORBET;
					_order_description += _link_color+Game.sorbet_types[_icecream_order[_i]].name + " sorbet[c_white]";				
				}
				if (_i < _n-2) {
					_order_description += ", ";
				}
				else if (_i < _n-1) {
					_order_description += " and ";
				}
				_order_description += "|";
			}
		
			if (random(1) < 0.6) {
				var _cone_order = irandom_range(0, array_length(Game.cone_types)/2-1);			
			}
			else {
				var _cone_order = array_length(Game.cone_types)/2 + irandom_range(0, array_length(Game.cone_types)/2-1);
			}
			_order_description += "on a "+_link_color+Game.cone_types[_cone_order].name+"[c_white]";
		}
		else {
			_popsicle_order = irandom_range(0, array_length(Game.popsicle_types)-1);
			_order_description += "a "+_link_color+Game.popsicle_types[_popsicle_order].name + " popsicle[c_white]";
		}
	
		var _u = random(1);
		if (_u < 0.4) { // Add syrup and topping
			var _syrup_order = irandom_range(0, array_length(Game.syrup_types)-1);
			var _topping_order = irandom_range(0, array_length(Game.topping_types)-1);
			_order_description += "|with "+_link_color+Game.syrup_types[_syrup_order].name+"[c_white] and|"+_link_color+Game.topping_types[_topping_order].name+"[c_white] on top";
		}
		else if (_u < 0.6) { // Add syrup
			var _syrup_order = irandom_range(0, array_length(Game.syrup_types)-1);
			_order_description += "|with "+_link_color+Game.syrup_types[_syrup_order].name+"[c_white]";
		}
		else if (_u < 0.8) { // Add topping
			var _topping_order = irandom_range(0, array_length(Game.topping_types)-1);
			_order_description += "|with "+_link_color+Game.topping_types[_topping_order].name+"[c_white] on top";
		}
	
		_order_description += choose(",|please!", ".|Thanks.", ",|if you will?");
		favorite_order = new order(_icecream_order, _icecream_type_order, _topping_order, _syrup_order, _cone_order, _popsicle_order, _order_description);
	}
	
	randomize_favorite();
	
	
#endregion
	
#region Animation
	
	fsm_state_animation_speed = ds_map_create();
	ds_map_add(fsm_state_animation_speed, "Idle", 6);
	ds_map_add(fsm_state_animation_speed, "Cellphone", 6);
	ds_map_add(fsm_state_animation_speed, "Book", 6);
	ds_map_add(fsm_state_animation_speed, "Move", 6);
		
	fsm_state_animation_length = ds_map_create();
	ds_map_add(fsm_state_animation_length, "Idle", 6);
	ds_map_add(fsm_state_animation_length, "Cellphone", 12);
	ds_map_add(fsm_state_animation_length, "Book", 12);
	ds_map_add(fsm_state_animation_length, "Move", 6);
	
	fsm_state_sprite_row = ds_map_create();
	ds_map_add(fsm_state_sprite_row, "Idle", 0);
	ds_map_add(fsm_state_sprite_row, "Cellphone", 5);
	ds_map_add(fsm_state_sprite_row, "Book", 6);
	ds_map_add(fsm_state_sprite_row, "Move", 1);
	
	fsm_state_animation_loop = ds_map_create();
	ds_map_add(fsm_state_animation_loop, "Idle", [-1, -1, -1, -1]);
	ds_map_add(fsm_state_animation_loop, "Cellphone", [3, 8, 2, 6]);
	ds_map_add(fsm_state_animation_loop, "Book", [0, 5, 1, 4]);
	ds_map_add(fsm_state_animation_loop, "Move", [-1, -1, -1, -1]);
	
	// Helper functions
	fnc_FSM_GetSubstateFromAngle = function(_move_angle) {
		var _substate;
		if (_move_angle >= 0 && _move_angle <= 45 || _move_angle > 315 && _move_angle < 360) _substate = "Right";
		else if (_move_angle > 45 && _move_angle <= 135) _substate = "Up";
		else if (_move_angle > 135 && _move_angle <= 225) _substate = "Left";
		else _substate = "Down";
			
		if (last_facing != facing) {
			last_facing = facing;
		}
		return _substate;
	}

	fnc_FSM_IndexOfSubstate = function(_substate) {
		var _index;
		if (fsm.current_state_name == "Cellphone" || fsm.current_state_name == "Book") {
			_index = 0;
		}
		else {			
			switch (_substate) {
				case "Right":	_index = 0; break;
				case "Up":		_index = 1; break;
				case "Left":	_index = 2; break;
				case "Down":	_index = 3; break;
				default:		_index = 3; break;
			}
		}
		return _index;
	}

	fnc_FSM_ResetAnimation = function() {
		img_index = fnc_FSM_IndexOfSubstate(facing) * fsm_state_animation_length[? fsm.current_state_name];
		step = 0;
		if (last_facing != facing) {
			last_facing = facing;
		}
	}

	fnc_FSM_Animate = function() {		
		var _speed = fsm_state_animation_speed[? fsm.current_state_name];
		var _length = fsm_state_animation_length[? fsm.current_state_name];
		if (step % _speed == 0) {
			img_index = img_index + 1;
			var _index = fnc_FSM_IndexOfSubstate(facing);
			if (img_index == (_index+1)*_length) {
				img_index = _index * _length;
			}
		}
	}
#endregion

#region Finite State Machine

	#region FSM
		
		fsm = new StateMachine("BehaviorFSM");
		
		#region Idle
		
			var _state = new State("Idle");
			with (_state) {
				enter = function() {
					with (other) {
						fnc_FSM_ResetAnimation();
					}
				}
				step = function() {
					with (other) {
						if (last_facing != facing) {
							fnc_FSM_ResetAnimation();
						}
						else {
							fnc_FSM_Animate();
						}
						fnc_GetInputsOrSensors();
					}
				}
			}
			fsm.add(_state);
		
		#endregion
		
		#region Cellphone
			var _state = new State("Cellphone");
			with (_state) {
				enter = function() {
					with (other) {
						previous_facing = facing;
						facing = "Down";
						fnc_FSM_ResetAnimation();
										
						if (alarm[1] <= 0) {
							alarm[1] = fsm_state_animation_length[? fsm.current_state_name] * fsm_state_animation_speed[? fsm.current_state_name];
						}
					}
				}
				step = function() {
					with (other) {
						if (last_facing != facing) {
							fnc_FSM_ResetAnimation();
						}
						else {
							fnc_FSM_Animate();
						}
						fnc_GetInputsOrSensors();
					}
				}
				leave = function() {
					with (other) {
						facing = previous_facing;
					}
				}
			}
			fsm.add(_state);
		#endregion
		
		#region Book
		
			var _state = new State("Book");
			with (_state) {
				enter = function() {
					with (other) {
						previous_facing = facing;
						facing = "Down";
						fnc_FSM_ResetAnimation();
										
						if (alarm[1] <= 0) {
							alarm[1] = fsm_state_animation_length[? fsm.current_state_name] * fsm_state_animation_speed[? fsm.current_state_name];
							print(str(fsm_state_animation_length[? fsm.current_state_name])+" "+str(fsm_state_animation_speed[? fsm.current_state_name])+" "+str(fsm_state_animation_length[? fsm.current_state_name] * fsm_state_animation_speed[? fsm.current_state_name]));
						}
					}
				}
				step = function() {
					with (other) {
						if (last_facing != facing) {
							fnc_FSM_ResetAnimation();
						}
						else {
							fnc_FSM_Animate();
						}
						fnc_GetInputsOrSensors();
					}
				}
				leave = function() {
					with (other) {
						facing = previous_facing;
					}
				}
			}
			fsm.add(_state);
		
		#endregion
		
		#region Move
		
			var _state = new State("Move");
			with (_state) {
				enter = function() {					
					// Start the path if coming from other state
					with (other) {
						fnc_FSM_ResetAnimation();
						path_start(path, player_speed, path_action_stop, false);
					}
				}
			
				step = function() { 
					with (other) {
						if (path_position != 1) { // If moving, update facing
							move_angle = point_direction(x, y, target_x, target_y);
							facing = fnc_FSM_GetSubstateFromAngle(move_angle);					
						}
						if (last_facing != facing) {
							fnc_FSM_ResetAnimation();
						}
						else {
							fnc_FSM_Animate();
						}
						fnc_GetInputsOrSensors();
					}
				}
			
				leave = function() {
					// Update facing to match spawn point, if standing on one
					with (other) {
						var _id = instance_place(x, y, obj_SpawnPoint);
						if (_id != noone) {
							facing = _id.facing;
						}
								
						// Reset path variables to transition to Idle
						move_confirmed = false;
						target_x = -1;
						target_y = -1;
						path_position = 0;
						if (path_exists(path)) {
							path_delete(path);
						}
					}
				}
			}
			fsm.add(_state);
		
		#endregion
		
		#region Transitions
		
			fsm.add_transition(new Transition("Idle", "Move", function() {
				// Transition if a move has been confirmed (new target)
				return move_confirmed;
			}));
		 
			fsm.add_transition(new Transition("Move", "Idle", function() {
				// Transition if current move path has finished
				return path_position == 1;
			}));
		
			fsm.add_transition(new Transition("Idle", "Cellphone", function() {
				return is_ai && ai_decision_fsm.current_state_name == "WaitInLine" && ai_decision_fsm.time_since_current_state % irandom_range(GAME_FPS*3,GAME_FPS*5) == 0 && random(1) < 0.5;
			}));
		
			fsm.add_transition(new Transition("Cellphone", "Idle", function() {
				return is_ai && alarm[1] <= 0;
			}));
		
			fsm.add_transition(new Transition("Idle", "Book", function() {
				return is_ai && ai_decision_fsm.current_state_name == "WaitInLine" && ai_decision_fsm.time_since_current_state % irandom_range(GAME_FPS*3,GAME_FPS*5) == 0 && random(1) < 0.5;
			}));
		
			fsm.add_transition(new Transition("Book", "Idle", function() {
				return is_ai && alarm[1] <= 0;
			}));
		
		#endregion
		
		fsm.init("Idle");
	
	#endregion
		
	#region Input and Helper Functions - These get called from the state functions, so keep an eye on the OTHER keyword!
		
		function fnc_GetInputsOrSensors() {
			if (is_ai) {
			}
			else {
				if (TAP) {  // Decide to move
					if (!position_meeting(ROOM_MOUSE_X, ROOM_MOUSE_Y, cls_Activable)) { // Click to set target on regular ground
						fnc_PerformMove(ROOM_MOUSE_X, ROOM_MOUSE_Y);
					}
					// ...else the activable object will call PerformMove to set target
				}
			}
		}
			
		function fnc_PerformMove(_x, _y) {
			// Create path
			if (path_exists(path)) {			
				path_delete(path);
			}	
			path = path_add();
				
			if (is_ai) {
				// Select grid
				var _grid = Game.grid_ai;
			}
			else {					
				// Create tap image
				var _id = instance_create_layer(_x, _y, "lyr_Tap", obj_Click);
				// Select grid
				var _grid = Game.grid;
			}				
				
			// Find path to selected target - if not possible, move in the direction of player		
			var _new_x = _x;
			var _new_y = _y;
			var _tries = 10;
			var _num_try = 0;
			move_confirmed = mp_grid_path(_grid, path, x, y, _new_x, _new_y, true);
			while (!move_confirmed && _num_try <= _tries) {
				_num_try++;
				var _dir = point_direction(_x, _y, x, y);
				var _new_x = _new_x + lengthdir_x(1, _dir) * TILE_SIZE;
				var _new_y = _new_y + lengthdir_y(1, _dir) * TILE_SIZE;
				move_confirmed = mp_grid_path(_grid, path, x, y, _new_x, _new_y, true);
			}
			if (move_confirmed) { // Update target x and y
				target_x = _new_x;
				target_y = _new_y;
				if (fsm.current_state_name == "Move") { // Already in MOVE state, so start the path from here (the enter() function of Move will not be called)
					path_start(path, player_speed, path_action_stop, false);					
				}
			}
		}
			
	#endregion

	#region AI Decision FSM
		
		ai_decision_fsm = new StateMachine("AIDecisionFSM");		
		
		var _state = new State("EnterShop");
		with (_state) {
			enter = function() {
				with (other) {
					alarm[0] = irandom_range(GAME_FPS * 2, GAME_FPS * 3);
				}
			}
		}
		ai_decision_fsm.add(_state);
		
		var _state = new State("LeaveIceCreamShop");
		with (_state) {
			enter = function() {
				with (other) {
					alarm[0] = GAME_FPS;
				}
			}
			step = function() {
				with (other) {
					if (alarm[0] == -1) {
						// Exit shop TBD
						visible = false;
					}
					else {
						show_debug_message("too many people");	
					}
				}
			}
		}
		ai_decision_fsm.add(_state);
		
		var _state = new State("WalkToLine");
		with (_state) {
			enter = function() {
				// Set target
				with (other) {
					var _i=0;
					var _target_x = path_get_point_x(pth_LineSmall, _i);
					var _target_y = path_get_point_y(pth_LineSmall, _i);
					while (place_meeting(_target_x, _target_y, obj_Player) && _i<path_get_number(pth_LineSmall)) {
						_i++;
						var _target_x = path_get_point_x(pth_LineSmall, _i);
						var _target_y = path_get_point_y(pth_LineSmall, _i);
					}
					// Perform move
					fnc_PerformMove(_target_x, _target_y)					
				}
			}
		}
		ai_decision_fsm.add(_state);
		
		var _state = new State("WaitInLine");
		with (_state) {			
		}
		
		ai_decision_fsm.add_transition(new Transition("EnterShop", "LeaveIceCreamShop", function() {
			// Leave if more than x people
			return alarm[0] <= 0 && instance_number(obj_Player) > min(6 /* Personal preference */, path_get_number(pth_LineSmall));
		}));
		
		ai_decision_fsm.add_transition(new Transition("EnterShop", "WalkToLine", function() {
			// Walk to line if less than or equal to x people
			return alarm[0] <= 0 && instance_number(obj_Player) <= min(6 /* Personal preference */, path_get_number(pth_LineSmall));
		}));
		
		ai_decision_fsm.add_transition(new Transition("WalkToLine", "WaitInLine", function() {
			// Once reached, wait there...
			return path_position == 1;
		}));
		
		ai_decision_fsm.init("EnterShop")
		
	#endregion

#endregion
	
#region Sprites
	
	// Sprites
	
	body_sprite = -1;
	outfit_sprite = -1;
	hairstyle_sprite = -1;
	accessory_sprite = -1;
	
	do {
		body_spr_index = irandom_range(1, 4);
		var _body_sprite_string = "slice_Body_"+string(CHARACTER_SIZE)+"x"+string(CHARACTER_SIZE)+"_0"+string(body_spr_index);				
		//show_debug_message(_body_sprite_string);
	}
	until (asset_get_index(_body_sprite_string) != -1);
	show_debug_message(_body_sprite_string);
	body_sprite = asset_get_index(_body_sprite_string);
	
	outfit_spr_index = irandom_range(1, 33);
	do {
		outfit_variation_spr_index = irandom_range(1, 9);
		var _outfit_sprite_string = "slice_Outfit_"+(outfit_spr_index < 10 ? "0" : "")+string(outfit_spr_index)+"_"+string(CHARACTER_SIZE)+"x"+string(CHARACTER_SIZE)+"_"+(outfit_variation_spr_index<10 ? "0" : "")+string(outfit_variation_spr_index);		
		//show_debug_message(_outfit_sprite_string);
	}
	until (asset_get_index(_outfit_sprite_string) != -1);
	outfit_sprite = asset_get_index(_outfit_sprite_string);
		
	hairstyle_spr_index = irandom_range(1, 29);
	do {
		hairstyle_variation_spr_index = irandom_range(1, 9);	
		var _hairstyle_sprite_string = "slice_Hairstyle_"+(hairstyle_spr_index < 10 ? "0" : "")+string(hairstyle_spr_index)+"_"+string(CHARACTER_SIZE)+"x"+string(CHARACTER_SIZE)+"_"+(hairstyle_variation_spr_index<10 ? "0" : "")+string(hairstyle_variation_spr_index);		
		//show_debug_message(_hairstyle_sprite_string);
	}
	until (asset_get_index(_hairstyle_sprite_string) != -1);
	hairstyle_sprite = asset_get_index(_hairstyle_sprite_string);	
		
	
	if (random(1) < 0.65) {
		accessory_spr_index = choose(3,4,8,15,17);
		do {
			accessory_variation_spr_index = irandom_range(1, 9);	
			var _accessory_sprite_string = "slice_Accessory_"+(accessory_spr_index < 10 ? "0" : "")+string(accessory_spr_index)+"_"+string(CHARACTER_SIZE)+"x"+string(CHARACTER_SIZE)+"_"+(accessory_variation_spr_index<10 ? "0" : "")+string(accessory_variation_spr_index);			
			//show_debug_message(_accessory_sprite_string);
		}
		until (asset_get_index(_accessory_sprite_string) != -1);
		accessory_sprite = asset_get_index(_accessory_sprite_string);				
	}
	
	img_index = 0;

#endregion

#region Talk
	talk_node = function(_id, _message, _response_option_ids) constructor {
		id = _id;
		dialog_text = _message;
		response_id = _response_option_ids;
	
		toString = function() {
			return string(id)+": "+dialog_text+" "+array_to_string(response_id);
		}
	}
#endregion

#region Dialog

	active_message = [];
	message_index = -1;
	
	// Multiple strings in the dialog text array can NOT be used when it's an option for the player
	dialog = [];
	dialog_actions = [];
	dialog_read = [];
	dialog_color = "[c_white]";
	
	array_push(dialog,	new talk_node(cls_Agent, // 0
						function() { return [""]; },
						function() { return [1]; }));

	dialog_read = array_create(array_length(dialog), false);

	// Dialog actions

	dialog_actions = array_create(array_length(dialog), function() {});
	

#endregion


#region Lighting

// Lighting
/*
occluder       = new BulbDynamicOccluder(obj_BulbRenderer.renderer);
occluder.x     = x;
occluder.y     = y;
occluder.angle = image_angle;

var _p1 = new Vec2(	-0.5*sprite_get_width(sprite_index), 0.5*sprite_get_height(sprite_index) );
var _p2 = new Vec2(	-3,0.5*sprite_get_height(sprite_index)-2 );
var _p3 = new Vec2(	0, 0.5*sprite_get_height(sprite_index)-4 );
var _p4 = new Vec2(	3, 0.5*sprite_get_height(sprite_index)-2 );
var _p5 = new Vec2(	0.5*sprite_get_width(sprite_index), 0.5*sprite_get_height(sprite_index) );

occluder.AddEdge( _p1.x, _p1.y, _p2.x, _p2.y );
occluder.AddEdge( _p2.x, _p2.y, _p3.x, _p3.y );
occluder.AddEdge( _p3.x, _p3.y, _p4.x, _p4.y );
occluder.AddEdge( _p4.x, _p4.y, _p5.x, _p5.y );
occluder.AddEdge( _p5.x, _p5.y, _p1.x, _p1.y );

//flashlight = new BulbLight(obj_BulbRenderer.renderer, spr_glr_light_mask_6, 0, x+10, y);

with (flashlight) {
	angle = other.image_angle;
	blend = c_white;
	xscale = 1;
	yscale = 1;
	visible = false;
}

*/
#endregion

/// Initial Code

fnc_FSM_ResetAnimation();

if (!is_ai) {
	Game.people_generated = false;	
}
