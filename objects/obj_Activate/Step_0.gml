if (!Game.paused) {
	if (point_in_rectangle(ROOM_MOUSE_X, ROOM_MOUSE_Y, bbox_left, bbox_top, bbox_right, bbox_bottom)) {
		if (TAP) {
			if (point_distance(Game.player_instance.x, Game.player_instance.y, x, y) < MIN_DISTANCE_ACTIVATE) {				
				Game.player_instance.spawn_point = spawn_point;
				
				if (script_exists(asset_get_index(activation_function))) {
					var _function = asset_get_index(activation_function);
					_function();
				}
				else if (room_exists(destination_room)) {
					room_goto(destination_room);
				}
			}
			else {
				//show_debug_message("Too far, distance is "+string(point_distance(Game.player_instance.x, Game.player_instance.y, x, y))+" vs "+string(3*TILE_SIZE));	
				var _id = instance_nearest(x, y, obj_SpawnPoint);
				Game.player_instance.fnc_PerformMove(_id.x, _id.y);
			}
		}
	}
} 