if (!Game.paused) {
	// Namedata on mouseover
	if (point_in_rectangle(ROOM_MOUSE_X, ROOM_MOUSE_Y, bbox_left, bbox_top, bbox_right, bbox_bottom)) {
		if (point_distance(Game.player_instance.x, Game.player_instance.y, x, y) < MIN_DISTANCE_ACTIVATE) {
			type_formatted(ROOM_MOUSE_X+1, ROOM_MOUSE_Y+1, "[fnt_GUI][scale, 0.25][c_black][fa_center][fa_bottom]"+activation_mouseover_near);	
			type_formatted(ROOM_MOUSE_X, ROOM_MOUSE_Y, "[fnt_GUI][scale, 0.25][c_lime][fa_center][fa_bottom]"+activation_mouseover_near);
		}
		else {
			type_formatted(ROOM_MOUSE_X+1, ROOM_MOUSE_Y+1, "[fnt_GUI][scale, 0.25][c_black][fa_center][fa_bottom]"+activation_mouseover_far);	
			type_formatted(ROOM_MOUSE_X, ROOM_MOUSE_Y, "[fnt_GUI][scale, 0.25][c_white][fa_center][fa_bottom]"+activation_mouseover_far);
		}
	}
}