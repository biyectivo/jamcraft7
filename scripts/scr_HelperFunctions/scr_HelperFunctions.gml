// Function to (re)initialize game variables
function fnc_InitializeGameStartVariables() {
	paused = false;
	lost = false;
	
	level = 1;
	total_score = 0;
	death_message_chosen = "";
	
	current_step = 0;	
	
	scoreboard_queried = false;
	scoreboard = [];
	scoreboard_html5 = [];
		
	http_get_id_query = noone;
	http_return_status_query = noone;
	http_return_result_query = noone;
	
	http_get_id_update = noone;
	http_return_status_update = noone;
	http_return_result_update = noone;
		
	current_scoreboard_updates = 0;
	max_scoreboard_updates = 1;
	timer_scoreboard_updates = 180;
		
	// Reset alarms (except for 0, which is center screen), since Game is persistent
	for (var _i=1; _i<=11; _i++) {
		Game.alarm[_i] = -1;
	}
	
	fullscreen_change = false;
	
	swiping = array_create(4, false);
	swipe_start = array_create(4, -1);
	swipe_end = array_create(4, -1);
	swipe_distance = array_create(4, -1);
	swipe_time = array_create(4, -1);
	swipe_started_now = array_create(4, false);
	swipe_ended_now = array_create(4, false);
	swipe_in_progress = array_create(4, false);
	swipe_points = array_create(4, []);
	swipe_debug = false;
	
	
	

	//*****************************************************************************
	// Ice cream
	//*****************************************************************************
	
	// Player names
	
	male_names = [];
	female_names = [];
	
	// Read names
	var _fid = file_text_open_read(working_directory+"\\Characters\\names_male.txt");
	while (!file_text_eof(_fid)) {
		array_push(male_names, file_text_readln(_fid));	
	}
	file_text_close(_fid);

	var _fid = file_text_open_read(working_directory+"\\Characters\\names_female.txt");
	while (!file_text_eof(_fid)) {
		array_push(female_names, file_text_readln(_fid));	
	}
	file_text_close(_fid);

	// Time of day

	day = 1;
	time = 8;
	real_minutes_per_day = 0.1;
	
	current_season = 0;
	seasons = ["Spring", "Summer", "Autumn", "Winter"];
	days_per_season = 3;
	
	fifteen_minute_factor = real_minutes_per_day/(24*4) * power(GAME_FPS,2);


	bank = 50000;

	// Trees and Crops
	trees = [];
	crops = [];
	
	tree = function(_name, _season, _growth_stage_idx, _yield_stage_idx, _die_stage_idx, _days_to_live, _fruit_type, _yield) constructor {
		name = _name;
		season = _season;
		growth_stage_idx = _growth_stage_idx;
		yield_stage_idx = _yield_stage_idx;
		die_stage_idx = _die_stage_idx;
		days_to_live = _days_to_live;
		/*fruit_type = _fruit_type;
		yield = _yield;*/
	}
	
	crop = function(_name, _season, _growth_stage_idx, _yield_stage_idx, _die_stage_idx, _days_to_live, _fruit_type, _yield) constructor {
		name = _name;
		season = _season;
		growth_stage_idx = _growth_stage_idx;
		yield_stage_idx = _yield_stage_idx;
		die_stage_idx = _die_stage_idx;
		days_to_live = _days_to_live;
		/*fruit_type = _fruit_type;
		yield = _yield;*/
	}
	
	// First four indices are tree growth, next three are fruit growth and final (1-3) are "die" state
	array_push(trees, new tree("Red Apple",				SEASONS.AUTUMN,	[0,1,2,3], [4,5,6], [19], 12));
	array_push(trees, new tree("Granny Smith Apple",	SEASONS.AUTUMN, [0,1,2,3], [7,8,9], [19], 12));
	array_push(trees, new tree("Golden Apple",			SEASONS.AUTUMN, [0,1,2,3], [10,11,12], [19], 12));
	array_push(trees, new tree("Gala Apple",			SEASONS.AUTUMN, [0,1,2,3], [13,14,15], [19], 12));
	array_push(trees, new tree("Yellow Apple",			SEASONS.AUTUMN, [0,1,2,3], [16,17,18], [19], 12));
	array_push(trees, new tree("Lemon",					SEASONS.WINTER, [20,21,22,23], [24,25,26], [36], 12));
	array_push(trees, new tree("Lime",					SEASONS.SUMMER, [20,21,22,23], [27,28,29], [36], 12));
	array_push(trees, new tree("Tangerine",				SEASONS.WINTER, [20,21,22,23], [30,31,32], [36], 12));
	array_push(trees, new tree("Orange",				SEASONS.WINTER, [20,21,22,23], [33,34,35], [36], 12));
	array_push(trees, new tree("Peach",					SEASONS.SUMMER,	[37,38,39,40], [41,42,43], [50], 12));
	array_push(trees, new tree("Manila Mango",			SEASONS.SPRING, [37,38,39,40], [44,45,46], [50], 12));
	array_push(trees, new tree("Haden Mango",			SEASONS.SPRING, [37,38,39,40], [47,48,49], [50], 12));
	array_push(trees, new tree("Cherry",				SEASONS.SUMMER, [51,52,53,54], [55,56,57], [58,59,60], 12));
	array_push(trees, new tree("Bartlett Pear",			SEASONS.AUTUMN, [61,62,63,64], [65,66,67], [74], 12));
	array_push(trees, new tree("Bosc Pear",				SEASONS.AUTUMN, [61,62,63,64], [68,69,70], [74], 12));
	array_push(trees, new tree("Anjou Pear",			SEASONS.AUTUMN, [61,62,63,64], [71,72,73], [74], 12));
	array_push(trees, new tree("Avocado",				SEASONS.SPRING, [75,76,77,78], [79,80,81], [82], 12));
	array_push(trees, new tree("Plum",					SEASONS.SUMMER, [83,84,85,86], [87,88,89], [90], 12));
	array_push(trees, new tree("Coconut",				SEASONS.ALL,	[91,92,93,94], [95,96,97], [94], 12));
	array_push(trees, new tree("Banana",				SEASONS.ALL,	[98,99,100,101], [102,103,104], [101], 12));
	
	array_push(crops, new crop("Chili Pepper",			SEASONS.SPRING, [2,3], [4,5], [6], 12));
	array_push(crops, new crop("Watermelon",			SEASONS.SUMMER, [7], [8,9,10], [11], 12));
	array_push(crops, new crop("Cantaloupe",			SEASONS.SUMMER, [12], [13,14,15], [16], 12));
	array_push(crops, new crop("Pumpkin",				SEASONS.AUTUMN, [17], [18,19,20], [21], 12));
	array_push(crops, new crop("Corn",					SEASONS.SUMMER, [22,23,24], [25], [26], 12));
	array_push(crops, new crop("Cucumber",				SEASONS.SUMMER, [27,28], [29,30], [31], 12));
	array_push(crops, new crop("Strawberry",			SEASONS.SPRING, [32], [33,34,35], [36], 12));
	array_push(crops, new crop("Blackberry",			SEASONS.SUMMER, [37], [38,39,40], [41], 12));
	array_push(crops, new crop("Raspberry",				SEASONS.SUMMER, [42], [43,44,45], [46], 12));
	array_push(crops, new crop("Blueberry",				SEASONS.SUMMER, [47], [48,49,50], [51], 12));
	array_push(crops, new crop("Purple Grape",			SEASONS.AUTUMN, [52], [53,54,55], [56], 12));
	array_push(crops, new crop("Green Grape",			SEASONS.AUTUMN, [57], [58,59,60], [61], 12));
	array_push(crops, new crop("Cranberry",				SEASONS.WINTER, [62,63], [64,65], [66], 12));
	array_push(crops, new crop("Pineapple",				SEASONS.SPRING, [67], [68,69,70,71], [72], 12));
	
	sorbet_prices = [];
	gelato_prices = [];
	popsicle_prices = [];
	
	sorbet_prices[PRICE_CLASSES.REGULAR] = [1.5, 2, 2.5];
	sorbet_prices[PRICE_CLASSES.PREMIUM] = [2.5, 3, 3.5];
	
	gelato_prices[PRICE_CLASSES.REGULAR] = [2, 3, 4];
	gelato_prices[PRICE_CLASSES.PREMIUM] = [4, 5, 6];
	
	popsicle_prices[PRICE_CLASSES.REGULAR] = 1;	
	popsicle_prices[PRICE_CLASSES.PREMIUM] = 2;
	
	topping_prices = 0.5;
	syrup_prices = 0.25;
	cone_prices = [];
	cone_prices = [0.99, 0.5];	// Cone y Cup
	
	
	// Ice cream
	
	order_element = function(_order_element_type, _index, _name, _friendly_name, _price_class) constructor {
		order_element_type = _order_element_type
		index = _index;
		name = _name;
		friendly_name = _friendly_name;
		price_class = _price_class;
	}
	
	gelato_types = [];
	sorbet_types = [];
	popsicle_types = [];
	topping_types = [];
	syrup_types = [];
	cone_types = [];
	
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 0, "Almonds", "Almonds", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 1, "Amaretto-Cherry-Almond", "Amaretto-Cherry-Almond", PRICE_CLASSES.PREMIUM));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 2, "Avocado", "Avocado", PRICE_CLASSES.PREMIUM));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 3, "Bailey's", "Bailey's", PRICE_CLASSES.PREMIUM));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 4, "Banana", "Banana", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 5, "Black Forest", "Black Forest", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 6, "Blackberry Chocolate", "Blackberry Chocolate", PRICE_CLASSES.PREMIUM));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 7, "Caramel", "Caramel", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 8, "Cherry-Vanilla", "Cherry-Vanilla", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 9, "Chococoffee", "Chococoffee", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 10, "Chocolate", "Chocolate", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 11, "Chocolate Chip Cookies", "Chocolate Chip Cookies", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 12, "Coconut", "Coconut", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 13, "Coffee", "Coffee", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 14, "Cotton Candy", "Cotton Candy", PRICE_CLASSES.PREMIUM));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 15, "Dark Chocolate", "Dark Chocolate", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 16, "Happy Halloween", "Happy Halloween", PRICE_CLASSES.PREMIUM));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 17, "Hazelnut", "Hazelnut", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 18, "Lemon-Lime", "Lemon-Lime", PRICE_CLASSES.PREMIUM));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 19, "Mango", "Mango", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 20, "Merry Christmas", "Merry Christmas", PRICE_CLASSES.PREMIUM));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 21, "Mint", "Mint", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 22, "Mint Chocolate", "Mint Chocolate", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 23, "Neapolitan", "Neapolitan", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 24, "Nutella", "Nutella", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 25, "Orange Chocolate", "Orange Chocolate", PRICE_CLASSES.PREMIUM));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 26, "Peach", "Peach", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 27, "Pear", "Pear", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 28, "Pineapple", "Pineapple", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 29, "Pistachio", "Pistachio", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 30, "Raspberry", "Raspberry", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 31, "Raspberry Ripple", "Raspberry Ripple", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 32, "Spicy Chocolate", "Spicy Chocolate", PRICE_CLASSES.PREMIUM));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 33, "Strawberry", "Strawberry", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 34, "Sweet Corn", "Sweet Corn", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 35, "Triple chocolate", "Triple chocolate", PRICE_CLASSES.PREMIUM));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 36, "Tutti Frutti", "Tutti Frutti", PRICE_CLASSES.PREMIUM));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 37, "Vanilla", "Vanilla", PRICE_CLASSES.REGULAR));
	array_push(gelato_types, new order_element(ORDER_ELEMENT_TYPES.GELATO, 38, "White Chocolate", "White Chocolate", PRICE_CLASSES.REGULAR));




	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 39, "Cherry", "Cherry", PRICE_CLASSES.REGULAR));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 40, "Fig", "Fig", PRICE_CLASSES.REGULAR));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 41, "Five Apples", "Five Apples", PRICE_CLASSES.PREMIUM));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 42, "Frutti di Bosco", "Frutti di Bosco", PRICE_CLASSES.PREMIUM));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 43, "Grapefruit", "Grapefruit", PRICE_CLASSES.REGULAR));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 44, "Grapes", "Grapes", PRICE_CLASSES.REGULAR));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 45, "Guava", "Guava", PRICE_CLASSES.PREMIUM));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 46, "Kiwi", "Kiwi", PRICE_CLASSES.REGULAR));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 47, "Lime", "Lime", PRICE_CLASSES.REGULAR));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 48, "Mango Mix", "Mango Mix", PRICE_CLASSES.REGULAR));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 49, "Mint", "Mint", PRICE_CLASSES.REGULAR));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 50, "Pear Mix", "Pear Mix", PRICE_CLASSES.PREMIUM));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 51, "Plum", "Plum", PRICE_CLASSES.PREMIUM));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 52, "Red Wine", "Red Wine", PRICE_CLASSES.PREMIUM));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 53, "Sour Apple", "Sour Apple", PRICE_CLASSES.REGULAR));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 54, "Spicy Cucumber", "Spicy Cucumber", PRICE_CLASSES.PREMIUM));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 55, "Spicy Mango", "Spicy Mango", PRICE_CLASSES.REGULAR));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 56, "Strawberry-Starfruit", "Strawberry-Starfruit", PRICE_CLASSES.PREMIUM));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 57, "Strawberry-Lemon", "Strawberry-Lemon", PRICE_CLASSES.REGULAR));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 58, "Tamarind", "Tamarind", PRICE_CLASSES.PREMIUM));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 59, "Tangerine", "Tangerine", PRICE_CLASSES.REGULAR));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 60, "Tangerine Orange", "Tangerine Orange", PRICE_CLASSES.REGULAR));
	array_push(sorbet_types, new order_element(ORDER_ELEMENT_TYPES.SORBET, 61, "Watermelon", "Watermelon", PRICE_CLASSES.REGULAR));



	array_push(popsicle_types, new order_element(ORDER_ELEMENT_TYPES.POPSICLE, 62, "Cantaloupe", "Cantaloupe", PRICE_CLASSES.REGULAR));
	array_push(popsicle_types, new order_element(ORDER_ELEMENT_TYPES.POPSICLE, 63, "Cherry", "Cherry", PRICE_CLASSES.REGULAR));
	array_push(popsicle_types, new order_element(ORDER_ELEMENT_TYPES.POPSICLE, 64, "Coconut-Pineapple", "Coconut-Pineapple", PRICE_CLASSES.PREMIUM));
	array_push(popsicle_types, new order_element(ORDER_ELEMENT_TYPES.POPSICLE, 65, "Cranberry", "Cranberry", PRICE_CLASSES.REGULAR));
	array_push(popsicle_types, new order_element(ORDER_ELEMENT_TYPES.POPSICLE, 66, "Guava", "Guava", PRICE_CLASSES.PREMIUM));
	array_push(popsicle_types, new order_element(ORDER_ELEMENT_TYPES.POPSICLE, 67, "Lemon", "Lemon", PRICE_CLASSES.REGULAR));
	array_push(popsicle_types, new order_element(ORDER_ELEMENT_TYPES.POPSICLE, 68, "Lime", "Lime", PRICE_CLASSES.REGULAR));
	array_push(popsicle_types, new order_element(ORDER_ELEMENT_TYPES.POPSICLE, 69, "Mango", "Mango", PRICE_CLASSES.REGULAR));
	array_push(popsicle_types, new order_element(ORDER_ELEMENT_TYPES.POPSICLE, 70, "Orange", "Orange", PRICE_CLASSES.REGULAR));
	array_push(popsicle_types, new order_element(ORDER_ELEMENT_TYPES.POPSICLE, 71, "Pineapple", "Pineapple", PRICE_CLASSES.REGULAR));
	array_push(popsicle_types, new order_element(ORDER_ELEMENT_TYPES.POPSICLE, 72, "Plum", "Plum", PRICE_CLASSES.PREMIUM));
	array_push(popsicle_types, new order_element(ORDER_ELEMENT_TYPES.POPSICLE, 73, "Purple Grape", "Purple Grape", PRICE_CLASSES.PREMIUM));
	array_push(popsicle_types, new order_element(ORDER_ELEMENT_TYPES.POPSICLE, 74, "Chocolate-Vanilla", "Chocolate-Vanilla", PRICE_CLASSES.REGULAR));




	array_push(topping_types, new order_element(ORDER_ELEMENT_TYPES.TOPPING, 0, "Chocolate chips", "Chocolate chips", PRICE_CLASSES.REGULAR));
	array_push(topping_types, new order_element(ORDER_ELEMENT_TYPES.TOPPING, 1, "Sprinkles", "Sprinkles", PRICE_CLASSES.REGULAR));
	array_push(topping_types, new order_element(ORDER_ELEMENT_TYPES.TOPPING, 2, "Wafer", "Wafer", PRICE_CLASSES.REGULAR));
	array_push(topping_types, new order_element(ORDER_ELEMENT_TYPES.TOPPING, 3, "Granola", "Granola", PRICE_CLASSES.REGULAR));
	array_push(topping_types, new order_element(ORDER_ELEMENT_TYPES.TOPPING, 4, "Marshmallows", "Marshmallows", PRICE_CLASSES.REGULAR));
	array_push(topping_types, new order_element(ORDER_ELEMENT_TYPES.TOPPING, 5, "Peanuts", "Peanuts", PRICE_CLASSES.REGULAR));
	array_push(topping_types, new order_element(ORDER_ELEMENT_TYPES.TOPPING, 6, "Almonds", "Almonds", PRICE_CLASSES.REGULAR));
	array_push(topping_types, new order_element(ORDER_ELEMENT_TYPES.TOPPING, 7, "Whipped Cream", "Whipped Cream", PRICE_CLASSES.REGULAR));
	// Ojo, índices continúan a pesar que se guarda en otra array
	array_push(syrup_types, new order_element(ORDER_ELEMENT_TYPES.SYRUP, 8, "Chocolate Syrup", "Chocolate Syrup", PRICE_CLASSES.REGULAR));
	array_push(syrup_types, new order_element(ORDER_ELEMENT_TYPES.SYRUP, 9, "Strawberry Syrup", "Strawberry Syrup", PRICE_CLASSES.REGULAR));
	array_push(syrup_types, new order_element(ORDER_ELEMENT_TYPES.SYRUP, 10, "Chamoy Chili Syrup", "Chamoy Chili Syrup", PRICE_CLASSES.REGULAR));



	array_push(cone_types, new order_element(ORDER_ELEMENT_TYPES.CONE, 0, "Waffle Cone", "Waffle Cone", PRICE_CLASSES.REGULAR));
	array_push(cone_types, new order_element(ORDER_ELEMENT_TYPES.CONE, 1, "Chocolate Waffle Cone", "Chocolate Waffle Cone", PRICE_CLASSES.REGULAR));
	array_push(cone_types, new order_element(ORDER_ELEMENT_TYPES.CONE, 2, "Chocolate Dipped Waffle Cone", "Chocolate Dipped Waffle Cone", PRICE_CLASSES.REGULAR));	
	array_push(cone_types, new order_element(ORDER_ELEMENT_TYPES.CONE, 3, "Regular Cup", "Regular Cup", PRICE_CLASSES.REGULAR));
	array_push(cone_types, new order_element(ORDER_ELEMENT_TYPES.CONE, 4, "Halloween Cup", "Halloween Cup", PRICE_CLASSES.REGULAR));
	array_push(cone_types, new order_element(ORDER_ELEMENT_TYPES.CONE, 5, "Christmas Cup", "Christmas Cup", PRICE_CLASSES.REGULAR));


	placing_tree = false;
	placing_crop = false;
	draw_toolbar = false;
	
	game_is_fullscreen = false;

	shop_name = "Ice Cream Shop";
	shop_size = room_IceCreamShop_Small;
}

function is_fullscreen() {
	if (!BROWSER) {
		return window_get_fullscreen();
	}
	else {
		return game_is_fullscreen;	
	}
}


// Update scoreboard
function fnc_UpdateScoreboard() {
	if (ENABLE_SCOREBOARD && Game.lost && Game.total_score > 0) {		
		var _scoreboard_url = "https://www.biyectivo.com/misc/scoreboard/score_insert.php";
		var _hash = sha1_string_utf8(Game.option_value[? Game.option_items[5]]+Game.scoreboard_game_id+string(Game.total_score)+SCOREBOARD_SALT);
		var _params = "user="+Game.option_value[? Game.option_items[5]]+"&game="+Game.scoreboard_game_id+"&score="+string(Game.total_score)+"&h="+_hash;
		
		http_get_id_update = http_get(_scoreboard_url + "?" + _params);
	}
	else {
		http_get_id_update = -1;
	}
}


/// @function fnc_ChooseProb(choose_array, array_probs)
/// @description Chooses a random value from the array with specified probability distribution
/// @param choose_array The array of values to choose from. If empty array, the function will return the index instead.
/// @param array_probs The probability array of each values from the list. If empty, use uniform distribution.
/// @return The chosen element

function fnc_ChooseProb(_choose_array, _array_probs) {
	var _n_choose = array_length(_choose_array);
	var _n_probs = array_length(_array_probs);
	if (_n_probs == 0 && _n_choose == 0) {  // Error
		throw("Error on fnc_ChooseProb, empty arrays provided.");
	}
	else {
		if (_n_probs == 0) {	 // Set uniform distribution
			var _probs = array_create(_n_choose);
			var _k = 0;
			for (var _i=0; _i<_n_choose; _i++) {
				if (_i < _n_choose - 1) {
					_probs[_i] = 1/_n_choose;
					_k=_k+_probs[_i];
				}
				else {
					_probs[_i] = 1-_k;				
				}
			}
		}
		else {	// Use what has been given
			_probs = _array_probs;			
		}
	
		var _rnd = random(1);
		var _i = 0;
		var	_currProb = _probs[_i];
		var _cumProb = _currProb;
		while (_rnd > _cumProb) {
			_i++;
			var	_currProb = _probs[_i];
			var _cumProb = _cumProb + _currProb;
		}
		if (_n_choose == 0) {
			return _i;
		}
		else {		
			return _choose_array[_i];
		}
	}
}

/// @function fnc_ChooseProbList(choose_list, array_probs)
/// @description Chooses a random value from the list with specified probability distribution
/// @param choose_list The list of values to choose from
/// @param array_probs The probability array of each values from the list
/// @return The chosen element

function fnc_ChooseProbList(_choose_list, _array_probs) {
	var _rnd = random(1);
	var _i = 0;
	var	_currProb = _array_probs[_i];
	var _cumProb = _currProb;
	while (_rnd > _cumProb) {
		_i++;
		var	_currProb = _array_probs[_i];
		var _cumProb = _cumProb + _currProb;
	}
	return _choose_list[| _i];
}




/// @function fnc_KeyToString(_key)
/// @arg _key The keycode to name
/// @return The reeadable name of the keycode

function fnc_KeyToString(_key) {
	if (_key >= 48 && _key <= 90) { 
		return chr(_key);
	}
	else {
		switch(_key) {
		    case -1: return "Unassigned";
		    case vk_backspace: return "Backspace";
		    case vk_tab: return "Tab";
		    case vk_enter: return "Enter";
		    case vk_lshift: return "Left Shift";
		    case vk_lcontrol: return "Left Ctrl";
		    case vk_lalt: return "Left Alt";
			case vk_rshift: return "Right Shift";
		    case vk_rcontrol: return "Right Ctrl";
		    case vk_ralt: return "Right Alt";
			case vk_shift: return "Shift";
		    case vk_control: return "Ctrl";
		    case vk_alt: return "Alt";
			case vk_printscreen: return "Print Screen";
		    case vk_pause: return "Pause/Break";
		    case 20: return "Caps Lock";
		    case vk_escape: return "Esc";
			case vk_space: return "Space";
		    case vk_pageup: return "Page Up";
		    case vk_pagedown: return "Page Down";
		    case vk_end: return "End";
		    case vk_home: return "Home";
		    case vk_left: return "Left Arrow";
		    case vk_up: return "Up Arrow";
		    case vk_right: return "Right Arrow";
		    case vk_down: return "Down Arrow";
		    case vk_insert: return "Insert";
		    case vk_delete: return "Delete";
			case vk_divide: return "/";
		    case vk_numpad0: return "Numpad 0";
		    case vk_numpad1: return "Numpad 1";
		    case vk_numpad2: return "Numpad 2";
		    case vk_numpad3: return "Numpad 3";
		    case vk_numpad4: return "Numpad 4";
		    case vk_numpad5: return "Numpad 5";
		    case vk_numpad6: return "Numpad 6";
		    case vk_numpad7: return "Numpad 7";
		    case vk_numpad8: return "Numpad 8";
		    case vk_numpad9: return "Numpad 9";
		    case vk_multiply: return "Numpad *";
		    case vk_add: return "Numpad +";
			case vk_decimal: return "Numpad .";
		    case vk_subtract: return "Numpad -";    
		    case vk_f1: return "F1";
		    case vk_f2: return "F2";
		    case vk_f3: return "F3";
		    case vk_f4: return "F4";
		    case vk_f5: return "F5";
		    case vk_f6: return "F6";
		    case vk_f7: return "F7";
		    case vk_f8: return "F8";
		    case vk_f9: return "F9";
		    case vk_f10: return "F10";
		    case vk_f11: return "F11";
		    case vk_f12: return "F12";
		    case 144: return "Num Lock";
		    case 145: return "Scroll Lock";
		    case ord(";"): return ";";
		    case ord("="): return "=";
		    case ord("\\"): return "\\";    
		    case ord("["): return "[";
		    case ord("]"): return "]";
			default: return "other";
		}
	}
}

/// @function fnc_BackupDrawParams
/// @description Backup the draw parameters to variables

function fnc_BackupDrawParams() {
	tmpDrawFont = draw_get_font();
	tmpDrawHAlign = draw_get_halign();
	tmpDrawVAlign = draw_get_valign();
	tmpDrawColor = draw_get_color();
	tmpDrawAlpha = draw_get_alpha();	
}

/// @function fnc_RestoreDrawParams
/// @description Restore the draw parameters from variables

function fnc_RestoreDrawParams() {
	draw_set_font(tmpDrawFont);
	draw_set_halign(tmpDrawHAlign);
	draw_set_valign(tmpDrawVAlign);
	draw_set_color(tmpDrawColor);
	draw_set_alpha(tmpDrawAlpha);	
}

///@function			fnc_BaseConvert(string, old_base, new_base)
///@description			Converts a number [as as string] from an old base to a new base
///@param	{string}	string		The number expressed as string
///@param				old_base	The old base
///@param				new_base	The new base
///@return				The number [as a string] expressed in the new base
function fnc_BaseConvert(_string_number, _old_base, _new_base) {
	var number, oldbase, newbase, out;
	number = _string_number;
    //number = string_upper(_string_number);
    oldbase = _old_base;
    newbase = _new_base;
    out = "";
 
    var len, tab;
    len = string_length(number);
    tab = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
 
    var i, num;
    for (i=0; i<len; i+=1) {
        num[i] = string_pos(string_char_at(number, i+1), tab) - 1;
    }
 
    do {
        var divide, newlen;
        divide = 0;
        newlen = 0;
        for (i=0; i<len; i+=1) {
            divide = divide * oldbase + num[i];
            if (divide >= newbase) {
                num[newlen] = divide div newbase;
                newlen += 1;
                divide = divide mod newbase;
            } else if (newlen  > 0) {
                num[newlen] = 0;
                newlen += 1;
            }
        }
        len = newlen;
        out = string_char_at(tab, divide+1) + out;
    } until (len == 0);
 
    return out;
}


///@function			in(element, array)
///@description			Returns whether an element is inside an array
///@param				element	The required element
///@param				array	The array to look the element in
///@return				Boolean that says whether the element is in the array or not
function in(_element, _array) {
	if (array_length(_array) == 0) {
		return false;
	}
	else {
		var _i=0;
		var _n = array_length(_array);
		var _found = false;
		while (_i<_n && !_found) {
			if (_array[_i] == _element) {
				_found = true;
			}
			else {
				_i++;
			}
		}
	}
	return _found;
}

/// @struct				Vec3(x,y,z)
/// @description		Creates a struct representing a 3D vector
/// @var	{real}	x	The x coordinate
/// @var	{real}	y	The y coordinate
/// @var	{real}	z	The z coordinate
function Vec3(_x, _y, _z) constructor {
	x = _x;
	y = _y;
	y = _z;
}



#region String functions

/// @function		fnc_StringToList(string, separator, remove_spaces)
/// @description	Takes a string with separators and returns a DS list of the separated string
/// @param			string			Source string to split
/// @param			[separator]		Optional string separator to use. Default: comma
/// @param			[remove_spaces]	Optional boolean to remove leading and trailing spaces on each item of the list. Default: false
/// @return			A DS list with the string split into the parts
function fnc_StringToList(_string, _separator=",", _remove_lead_trail_spaces = false) {
	if (argument_count == 0) {		
		throw ("String argument required and not provided.");
	}
	
	// Process and split
	var _list = ds_list_create();	
	var _substring = _string;
	var _next_separator = string_pos(_separator, _substring);
	while (_next_separator != 0) {
		var _found = string_copy(_substring, 0, _next_separator-1);
		if (_remove_lead_trail_spaces) {			
			_found = fnc_String_lrtrim(_found);
		}
		
		if (string_length(_found) > 0) {
			ds_list_add(_list, _found);		
		}
		
		_substring = string_copy(_substring, _next_separator+1, string_length(_substring));				
		var _next_separator = string_pos(_separator, _substring);
	}
	ds_list_add(_list,_substring);
	
	return _list;
}

///@function			fnc_String_cleanse(string)
///@description			Removes non-printable characters from a string 
///@param				string	The string to process
///@return				The cleansed string
function fnc_String_cleanse(_str) {
	var _newstr = _str;
	for (var _j=0; _j<32; _j++) {
		_newstr = string_replace_all(_newstr, chr(_j), "");
	}
	return _newstr;
}

///@function			fnc_String_lrtrim(string)
///@description			Trims spaces before and after the first and last characters
///@param				string	The string
///@return				The trimmed string
function fnc_String_lrtrim(_str) {	
	var _j=0;
	var _l=string_length(_str);
	while (string_copy(_str, _j, 1)==" " && _j<_l) {
		_j++;
	}
	_str = string_copy(_str, _j, _l);
			
	var _j=string_length(_str);
	while (string_copy(_str, _j, 1)==" " && _j>=0) {
		_j--;
	}
	_str = string_copy(_str, 0, _j);
	return _str;	
}

#endregion

/// @struct				Vec2(x,y)
/// @description		Creates a struct representing a 2D vector
/// @var	{real}	x	The x coordinate
/// @var	{real}	y	The y coordinate

function Vec2(_x, _y) constructor {
	x = _x;
	y = _y;
	
	/// @method						magnitude()
	/// @description				Calculates the 2D vector magnitude divided by a factor
	/// @param	{real}	[factor]	Dividing factor [default=1]
	/// @return	{real}				The normalized 2D vector magnitude
	magnitude = function(factor=1) {
		return sqrt(power(x,2)+power(y,2))/factor;
	}
	
	/// @method						toString()
	/// @description				Prints the 2D vector
	toString = function() {
		return "("+string(x)+","+string(y)+")";	
	}
}



function display_get_gui_w() {
	if (SELECTED_SCALING == SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW_SCALED) {
		return Game.adjusted_window_width;
	}
	else {
		return display_get_gui_width();
	}
}

function display_get_gui_h() {
	if (SELECTED_SCALING == SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW_SCALED) {
		return Game.adjusted_window_height;
	}
	else {
		return display_get_gui_height();
	}
}



/// @function fnc_Ease(_v_ini, _v_target, _t, _curve, _curve_channel)
/// @description Perform easing with an animation curve with the equation: v = v_ini + lambda * (v_target-v_ini) where lambda changes from 0 to 1 according to the animation curve.
/// @param _v_ini initial value (when t=0).
/// @param _v_target target value (when t=1).
/// @param _t target value
/// @param _curve animation curve asset.
/// @param _curve_channel name of the curve channel.
/// @returns the interpolated value at _t.

function fnc_Ease(_v_ini, _v_target, _t, _curve, _curve_channel) {
	var _anim_channel = animcurve_get_channel(_curve, _curve_channel);
	var _lambda = animcurve_channel_evaluate(_anim_channel, _t);	
	return _v_ini + _lambda * (_v_target - _v_ini);
}


///@function				array_find(array, element)
///@description				finds element inside array and returns the position, or -1 if not found
///@param		{array}		array		the array
///@param		{*}			element		the element to look for
///@return		{integer}	the 0-based index of element within array, or -1 if it was not found
function array_find(_things, _array) {
	if (!is_array(_things)) {
		var _lookup_array = [_things];
	}
	else {
		var _lookup_array = _things;
	}
	if (!is_array(_array)) {
		var _target_array = [_array];
	}
	else {
		var _target_array = _array;
	}
		
	var _n = array_length(_target_array);
	var _m = array_length(_lookup_array);
	var _j = 0;
	var _found = false;
	while (_j<_m && !_found) {
		var _i=0;			
		while (_i<_n && !_found) {
			if (_target_array[_i] == _lookup_array[_j]) {
				_found = true;
			}
			else {
				_i++;	
			}
		}
		if (!_found) {
			_j++;
		}
	}
		
	if (_found) {
		return _i;
	}
	else {
		return -1;
	}
}


function array_resize_from_end(_array, _new_size) {
	var _n = array_length(_array);
	if (_new_size < _n) {		
		for (var _i=0; _i<_new_size; _i++) {
			_array[@_i] = _array[_i+_n-_new_size];
		}
		array_resize(_array, _new_size);
	}
}

function convert_gui_to_room(_vec2_gui) {
	// TODO						
}

function convert_room_to_gui(_vec2_room) {
	if (ENABLE_LIVE and live_call()) return live_result;
	var _gw = display_get_gui_w();
	var _gh = display_get_gui_h();
	var _rw = room_width;
	var _rh = room_height;
			
	var _sw = surface_get_width(application_surface) * Game.application_surface_scaling;
	var _sh = surface_get_height(application_surface) * Game.application_surface_scaling;
	
	var _cw = camera_get_view_width(VIEW);
	var _ch = camera_get_view_height(VIEW);
	var _cx = camera_get_view_x(VIEW);
	var _cy = camera_get_view_y(VIEW);
	
	var _x_rel_cam = (_vec2_room.x - _cx)/_cw;
	var _y_rel_cam = (_vec2_room.y - _cy)/_ch;

	return new Vec2(_x_rel_cam * _sw + Game.gui_offset_x, _y_rel_cam * _sh + Game.gui_offset_y);

}


function device_mouse_dragswipe_register(_device) {
	Game.swipe_started_now[_device] = false;
	Game.swipe_ended_now[_device] = false;
	
	if (!Game.swiping[_device]) {
		Game.swiping[_device] = device_mouse_check_button_pressed(_device, mb_left);
		if (Game.swiping[_device]) { // Swipe started
			Game.swipe_start[_device] = new Vec2(device_mouse_x(_device), device_mouse_y(_device));
			Game.swipe_end[_device] = -1;
			Game.swipe_started_now[_device] = true;
			Game.swipe_distance[_device] = -1;
			Game.swipe_time[_device] = get_timer();		
			Game.swipe_points[_device] = [];
			Game.swipe_in_progress[_device] = true;
		}
	}
	else if (device_mouse_check_button_released(_device, mb_left)) { // Swipe ended
		Game.swiping[_device] = false;				
		Game.swipe_end[_device] = new Vec2(device_mouse_x(_device), device_mouse_y(_device));
		Game.swipe_ended_now[_device] = true;
		Game.swipe_distance[_device] = point_distance(Game.swipe_start[_device].x, Game.swipe_start[_device].y, Game.swipe_end[_device].x, Game.swipe_end[_device].y);
		Game.swipe_time[_device] = (get_timer() - Game.swipe_time[_device])/1000000;		
		Game.swipe_in_progress[_device] = false;
	}
	else {
		// Swiping in progress		
		array_push(Game.swipe_points[_device], new Vec2(device_mouse_x(_device), device_mouse_y(_device)));
		Game.swipe_in_progress[_device] = true;
	}
}

function device_mouse_check_dragswipe_start(_device) {
	var _ret = Game.swipe_started_now[_device];
	if (_ret && swipe_debug) show_debug_message("Device "+string(_device)+" Swiping started at "+string(Game.swipe_start[_device]));
	return _ret;	
}

function device_mouse_check_dragswipe_end(_device) {	
	var _ret = Game.swipe_ended_now[_device] && Game.swipe_distance[_device] > SWIPE_DETECTION_TOLERANCE;
	if (_ret && swipe_debug) show_debug_message("Device "+string(_device)+" Swiping ended at "+string(Game.swipe_end[_device]));
	return _ret;
}

function device_mouse_check_dragswipe(_device) {
	var _ret = Game.swipe_in_progress[_device];
	if (_ret && swipe_debug) show_debug_message("Device "+string(_device)+" Swiping in progress");
	return _ret;
}

function device_mouse_dragswipe_distance(_device) {
	var _ret = Game.swipe_distance[_device];
	if (swipe_debug) show_debug_message("Device "+string(_device)+" distance swiped: "+string(_ret));
	return _ret;
}

function device_mouse_dragswipe_time(_device) {
	var _ret = Game.swipe_time[_device];
	if (swipe_debug) show_debug_message("Device "+string(_device)+" time swiped: "+string(_ret));
	return _ret;	
}

function device_mouse_dragswipe_speed(_device) {
	var _ret = Game.swipe_distance[_device]/Game.swipe_time[_device];
	if (swipe_debug) show_debug_message("Device "+string(_device)+" speed swiped: "+string(_ret));
	return _ret;
}



///@function				foreach(object, code)
///@description				Simulates a For Each loop. Iterates over the elements of an array/list/map and executes the passed function, in which the argument name will be used to refer to each item in the loop.
///@param		{*}			object		The array/list/map
///@param		{function}	code		A function to be executed, needs an argument and you can use that to refer to the current item.
function foreach(object, code) {
	var _n, _keys, _thing_;
	
	if (is_string(object)) {
		_n = 1;
	}
	else if (is_array(object)) {
		_n = array_length(object);		
	}
	else if (ds_exists(object, ds_type_list)) {
		_n = ds_list_size(object);
	}
	else if (ds_exists(object, ds_type_map)) {
		_n = ds_map_size(object);
		_keys = ds_map_keys_to_array(object);
	}
	else {
		_n = 1;
	}

	for (var _i=0; _i<_n; _i++) {
		_thing_ = is_string(object) ? object : (is_array(object) ? object[_i] : (ds_exists(object, ds_type_list) ? object[| _i] : (ds_exists(object, ds_type_map) ? object[? _keys[_i]] : object)));
		code(_thing_);
	}
}

function array_to_string(_array) {
		if (!is_array(_array)) {
			return string(_array);
		}
		else {
			var _n = array_length(_array);
			var _str = "["
			for (var _i=0; _i<_n; _i++) {
				if (is_array(_array[_i])) {
					_str = _str + array_string(_array[_i]);
				}
				else {
					_str = _str + _array[_i];	
				}
				if (_i<_n-1) {
					_str = _str + ",";
				}
			}
			_str = _str + "]"
			return _str;
		}
	}

function number_commas(_num) {
	var _str = string(_num);
	var _n = string_length(_str);
	for (var _i=_n-2; _i>1; _i=_i-3) {
		_str = string_insert(",", _str, _i);
	}
	return _str;
}


function gmcallback_fullscreen() {
	ToggleFullScreen();	
}

function custom_game_restart() {
	// This will destroy all instances. 
	// Yes, this will run their cleanup events as well as their destroy event.
	with(all) {
		if (id != Game.id)
			instance_destroy();	
	}

	audio_stop_all();
	draw_texture_flush();
	
	// Go to the very first room, as per room order
	room_goto(room_first);
	
	// Call the "game start" code
	game_start();
}


function game_start() {
	
	// Mouse cursor
	window_set_cursor(cr_none);
	cursor_sprite = spr_Cursor;

	// Avoid automatic drawing of the application surface in order to draw it manually using the desired game resolution (independent of the window resolution)
	if (SELECTED_SCALING != SCALING_TYPE.RESOLUTION_ADAPTED_TO_WINDOW) {
		application_surface_draw_enable(false);
	}


	// Anti-aliasing and vsync
	//display_reset(8, false);

	// Go to next room
	//room_goto(room_UI_Title);
	room_goto(room_Game_1);
	
}


function fnc_ActivateShop() {
	Game.draw_toolbar = !Game.draw_toolbar;	
}