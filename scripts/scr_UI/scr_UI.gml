
#region Menu
	
	/// @function fnc_DrawMenu
	/// @description Draw the menu to the screen and perform mouseover/click handlers
	function fnc_DrawMenu() {
		
		var _title_color = "[c_green]";
		var _main_text_color = "[c_white]";
		var _link_color = "[c_white]";
		var _link_hover_color = "[c_yellow]";
		
		var _w = GUI_WIDTH;
		var _h = GUI_HEIGHT;
		
		var _y_title = 60;
		
		type_formatted(_w/2, _y_title, "[fnt_Title][fa_middle][fa_center][scale,1.2]"+_title_color+game_title);
		
		var _n = array_length(menu_items);
		var _startY = _y_title + 100;
		var _spacing = 40;
		for (var _i = 0; _i<_n; _i++) {	
			fnc_Link(_w/2, _startY + _i*_spacing, "[fa_middle][fa_center]"+_link_color+menu_items[_i], "[fa_middle][fa_center]"+_link_hover_color+menu_items[_i], fnc_ExecuteMenu, _i);
		}
	
			
	}


	function fnc_Menu_0 () {
		transition_id = instance_create_layer(0, 0, layer_get_id("lyr_Instances"), cls_Transition);
		with  (transition_id) {
			destination_room = room_Game_1;
			transition_type = TRANSITION.SQUARES;
			max_time = GAME_FPS;
			event_perform(ev_other, ev_user0);
		}
	}
	
	function fnc_Menu_1 () {
		room_goto(room_UI_HowToPlay);
	}


	function fnc_Menu_2 () {
		room_goto(room_UI_Options);
	}

	
	function fnc_Menu_3 () {
		room_goto(room_UI_Credits);
	}

	function fnc_Menu_4 () {
		game_end();
	}


#endregion

#region Options

	/// @function fnc_DrawOptions
	/// @description Draw the options to screen and perform mouseover/click handlers

	function fnc_DrawOptions() {
		var _w = GUI_WIDTH;
		var _h = GUI_HEIGHT;

		var _title_color = "[c_green]";
		var _main_text_color = "[c_white]";
		var _link_color = "[c_white]";
		var _link_hover_color = "[c_yellow]";
	
		var _slider_color = $fffdff;
		var _slider_handle_color = $fffdff;
		var _slider_handle_drag_color = $e6ff0b;
				
		var _y_title = 60;
		type_formatted( _w/2, _y_title, "[fa_middle][fa_center][fnt_Title]"+_title_color+"Options");
		var _startY = _y_title+100;
		var _spacing = 40;
		
		var _n = array_length(option_items);

		for (var _i=0; _i<_n; _i++) {
			if (option_type[? option_items[_i]] == "checkbox" || option_type[? option_items[_i]] == "toggle") {
				var _sprite = asset_get_index("spr_"+string_upper(string_copy(option_type[? option_items[_i]], 1, 1))+string_copy(option_type[? option_items[_i]],2,string_length(option_type[? option_items[_i]])));
				draw_sprite_ext(_sprite, option_value[? option_items[_i]], _w/2-100, _startY+_i*_spacing, 1, 1, 0, c_white, 1);
				var _mousex = GUI_MOUSE_X;
				var _mousey = GUI_MOUSE_Y;
				var _mouseover = _mousex >= _w/2-100 - sprite_get_width(_sprite)/2 && _mousex <= _w/2-100 + sprite_get_width(_sprite)/2 && _mousey >= _startY+_i*_spacing - sprite_get_height(_sprite)/2 && _mousey <= _startY+_i*_spacing+sprite_get_height(_sprite)/2;
				if (device_mouse_check_button_pressed(0, mb_left) && _mouseover) {
					fnc_ExecuteOptions(_i);
				}
				fnc_Link(_w/2-100+sprite_get_width(_sprite), _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i], fnc_ExecuteOptions, _i);
			}
			else if (option_type[? option_items[_i]] == "slider") {
				fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i], fnc_ExecuteOptions, _i);
				var _temp_struct = type_formatted(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], false);
				
				var _slider_start = _w/2-100 + _temp_struct.bbox_width + 20;
				var _slider_end = _slider_start+60+20;
				var _mousex = device_mouse_x_to_gui(0);
				var _mousey = device_mouse_y_to_gui(0);
				var _handle_radius = 9;
				var _mouseover_circle = point_in_circle(_mousex, _mousey, _slider_start + (_slider_end-_slider_start) * option_value[? option_items[_i]], _startY+_i*_spacing+6/3, _handle_radius);
				
				// Draw slider
				draw_rectangle_color(_slider_start, _startY+_i*_spacing-3, _slider_end, _startY+_i*_spacing+3, _slider_color, _slider_color, _slider_color, _slider_color, false);
				
				// Draw handle
				if (_mouseover_circle || start_drag_drop) {
					var _color_circle = _slider_handle_drag_color;
				}
				else {
					var _color_circle = _slider_handle_color;
				}
				draw_circle_color(_slider_start + (_slider_end-_slider_start) * option_value[? option_items[_i]], _startY+_i*_spacing, _handle_radius, _color_circle, _color_circle, false);
				
				// Handle drag & drop				
				if (device_mouse_check_button(0, mb_left) && (_mouseover_circle || start_drag_drop)) {							
					option_value[? option_items[_i]] = (clamp(_mousex, _slider_start, _slider_end) - _slider_start) / (_slider_end - _slider_start);					
					start_drag_drop = true;
				}
				else {
					start_drag_drop = false;
				}
				
				// Display %
				if (start_drag_drop) {					
					type_formatted(_slider_start + (_slider_end-_slider_start) * option_value[? option_items[_i]], _startY+_i*_spacing-30, "[fa_center]"+_link_color+string(round(option_value[? option_items[_i]]*100))+"%");
				}
				
			}
			else if (option_type[? option_items[_i]] == "input") {	
				if (name_being_modified) {
					fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_hover_color+option_items[_i]+": [fnt_MiniText] "+option_value[? option_items[_i]], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i]+": [fnt_MiniText] "+_link_hover_color+option_value[? option_items[_i]], noone, 0);
					if (keyboard_lastkey == vk_enter) { // finalize
						option_value[? option_items[_i]] = keyboard_string == "" ? "Player" : string_copy(keyboard_string,1,16);
						name_being_modified = false;
					}
					else {
						keyboard_string = string_copy(keyboard_string,1,16);
						option_value[? option_items[_i]] = keyboard_string;
					}
				}
				else {					
					fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i]+": [fnt_MiniText] "+option_value[? option_items[_i]], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i]+": [fnt_MiniText] "+option_value[? option_items[_i]], fnc_ExecuteOptions, _i);
				}
			}
			else { // Regular link
				fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], "[fa_middle][fa_left][c_green]"+_link_hover_color+option_items[_i], fnc_ExecuteOptions, _i);	
			}
		}
		fnc_Link(_w/2, _h-60, "[fa_middle][fa_center]"+_link_color+"Return to Main Menu", "[fa_middle][fa_center]"+_link_hover_color+"Return to Main Menu", fnc_ReturnToMainMenu, 0);	
	}


	/// @function fnc_Options_0
	/// @description Perform the click of the corresponding option 

	function fnc_Options_0() {
		fnc_Options_Checkbox(0);
	}


	/// @function fnc_Options_1
	/// @description Perform the click of the corresponding option 

	function fnc_Options_1() {
		fnc_Options_Checkbox(1);
	}


	/// @function fnc_Options_2
	/// @description Perform the click of the corresponding option 

	function fnc_Options_2() {
		fnc_Options_Checkbox(2);
	}

	/// @function fnc_Options_3
	/// @description Perform the click of the corresponding option 

	function fnc_Options_3() {
		fnc_Options_Checkbox(3);
		fullscreen_change = true;
	}

	/// @function fnc_Options_4
	/// @description Perform the click of the corresponding option 

	function fnc_Options_4() {
		room_goto(room_UI_Options_Controls);
	}
	
	/// @function fnc_Options_5
	/// @description Perform the click of the corresponding option 

	function fnc_Options_5() {
		name_being_modified = true;
		keyboard_string = option_value[? option_items[5]];
	}
	
	function fnc_DrawOptionsControls() {
		var _w = GUI_WIDTH;
		var _h = GUI_HEIGHT;
		
		var _title_color = "[c_green]";
		var _main_text_color = "[c_white]";
		var _link_color = "[c_white]";
		var _link_hover_color = "[c_yellow]";
		
		var _y_title = 60;
		type_formatted(_w/2, _y_title, "[fa_middle][fa_center][fnt_Title]"+_title_color+"Controls");
		
		var _n = ds_map_size(controls);
		for (var _i=0; _i<_n; _i++) {
			if (wait_for_input && key_being_remapped == control_indices[_i]) {
				type_formatted(_w/2, _h-30, _link_color+"[fa_middle][fa_center][fnt_Menu]PRESS ANY KEY TO REMAP");
				fnc_Link(_w/2-150, _y_title+60+_i*30, "[fa_middle][fa_left][fnt_MiniText]"+_link_hover_color+control_names[? control_indices[_i]]+": ", "[fa_middle][fa_left][fnt_MiniText]"+_link_hover_color+control_names[? control_indices[_i]]+": ", noone, 0);
				fnc_AssignControls(control_indices[_i]);
			}
			else {
				//show_debug_message(control_names[? control_indices[_i]]);
				fnc_Link(_w/2-150, _y_title+60+_i*30, "[fa_middle][fa_left][fnt_MiniText]"+_link_color+control_names[? control_indices[_i]]+": "+fnc_KeyToString(controls[? control_indices[_i]]), "[fa_middle][fa_left][fnt_MiniText]"+_link_hover_color+control_names[? control_indices[_i]]+": "+fnc_KeyToString(controls[? control_indices[_i]]), fnc_AssignControls, control_indices[_i]);
			}
		}
		
		
		fnc_Link(_w/2, _h-60, "[fa_middle][fa_center]"+_link_color+"Return to Options", "[fa_middle][fa_center]"+_link_hover_color+"Return to Options", fnc_Menu_2, 0);
	}
	
	function fnc_AssignControls(_key) {			
		if (wait_for_input) {
			if (key_being_remapped == noone) {
				keyboard_lastkey = noone;
				key_being_remapped = _key;
			}
			else if (keyboard_lastkey != noone) {
				if (keyboard_lastkey != vk_escape) {
					controls[? _key] = keyboard_lastkey;					
				}
				key_being_remapped = noone;
				wait_for_input = false;
			}			
		}
		else {
			wait_for_input = true;
		}
	}
	
	/// @function fnc_ReturnToMainMenu
	/// @description Return to Main Menu

	function fnc_ReturnToMainMenu() {
		room_goto(room_UI_Title);
	}
	

	/// @function fnc_Options_Checkbox
	/// @description Auxiliary function to enable/disable checkbox

	function fnc_Options_Checkbox(_i) {
		option_value[? option_items[_i]] = !option_value[? option_items[_i]];
	}

#endregion	

#region Credits

function fnc_DrawCredits() {
	var _w = GUI_WIDTH;
	var _h = GUI_HEIGHT;
	
	var _title_color = "[c_green]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	var _y_title = 60;
	type_formatted(_w/2, _y_title, _title_color+"[fnt_Title][fa_middle][fa_center]Credits");
	var _startY = _y_title+120;
	var _spacing = 30;
		
	for (var _i=0; _i<array_length(credits); _i++) {
		type_formatted(_w/2, _startY + _i*_spacing, credits[_i]);
	}	
	
	fnc_Link(_w/2, _h-60, _link_color+"[fa_middle][fa_center]Return to Main Menu", _link_hover_color+"[fa_middle][fa_center][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);	
}

#endregion

#region Instructions

function fnc_DrawHowToPlay() {
	var _w = GUI_WIDTH;
	var _h = GUI_HEIGHT;
	
	var _title_color = "[c_green]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	var _y_title = 60;
	type_formatted(_w/2, _y_title, _title_color+"[fnt_Title][fa_middle][fa_center]Help");
	var _startY = _y_title+80;
	var _spacing = 30;
		
	// Draw text...
	type_formatted(_w/2, _startY, _main_text_color+"[fnt_MiniText][fa_center][fa_middle]You are [spr_Player]");
	
	fnc_Link(_w/2, _h-60, _link_color+"[fa_middle][fa_center]Return to Main Menu", _link_hover_color+"[fa_middle][fa_center][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);	
}

#endregion

#region HUD/Lost/Won/Pause Menu

function fnc_DrawToolbar() {
	if (ENABLE_LIVE && live_call()) return live_result;
	// Toolbar outline
	var _w = adjusted_window_width;
	var _h = adjusted_window_height;
	var _toolbar_top = 240;
	/*var _x1 = 10;
	var _y1 = _h-210;
	var _x2 = _w-10;
	var _y2 = _h-10;*/
	var _x1 = _w/2-470;
	var _y1 = _h-_toolbar_top;
	var _x2 = _w/2+470;
	var _y2 = _h-10;
	var _radius = 16;
	var _color = make_color_rgb(114, 75, 44);
	draw_set_color(_color);
	draw_roundrect_ext(_x1, _y1, _x2, _y2, _radius, _radius, false);
	// Inner Border
	/*var _x1 = 18;
	var _y1 = _h-202;
	var _x2 = _w-18;
	var _y2 = _h-18;*/
	var _x1 = _w/2-470+8;
	var _y1 = _h-_toolbar_top+8;
	var _x2 = _w/2+470-8;
	var _y2 = _h-10-8;
	var _radius = 16;
	var _color = make_color_rgb(69, 42, 27);
	var _color2 = make_color_rgb(127, 91, 62);
	draw_set_color(_color2);
	draw_rectangle(_x1, _y1, _x2, _y2, false);
	draw_set_color(_color);
	draw_rectangle(_x1, _y1, _x2, _y2, true);
	// Title background
	var _text_string = "[fnt_GUI][scale,0.25][fa_center][fa_middle][c_white]"+shop_name;
	var _text_width = type_formatted(_w/2, _h-215, _text_string, false).bbox_width + 30;
	var _x1 = _w/2-_text_width/2;
	var _y1 = _h-_toolbar_top-10;
	var _x2 = _w/2+_text_width/2;
	var _y2 = _h-_toolbar_top+20;
	var _color = make_color_rgb(39, 31, 27);
	draw_set_color(_color);
	draw_roundrect_ext(_x1, _y1, _x2, _y2, _radius, _radius, false);
	// Title
	type_formatted(_w/2, _h-_toolbar_top+5, _text_string);
	
	// Buttons
	var _button_width = 120;
	var _button_height = sprite_get_height(spr_Button);	
	var _h_margin = 40;
	var _format = "[fnt_GUI][scale,0.2][fa_left][fa_middle][#DDDDDD]";
	var _format_mouseover = "[fnt_GUI][scale,0.2][fa_left][fa_middle][c_white]";	
	var _radius = 8;
	
	var _text_string = "Gelatos";
	var _text_width = type_formatted(_w/2+200, _h-165, _format+_text_string, false).bbox_width + 20;	
	//var _x1 = _w-_h_margin-2*_button_width-1*10;
	var _x1 = _w/2+470-_h_margin-2*_button_width-1*10;
	var _y1 = _h-_toolbar_top+20 + 0*50;
	
	if (point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x1+_button_width, _y1+_button_height)) {		
		draw_sprite_stretched_ext(spr_Button_Pressed, 0, _x1, _y1, _button_width, _button_height, c_white, 1);
		//draw_sprite_ext(spr_Cones, 0, _x1+25, _y1+_button_height/2+5, 1, 1, 0, c_white, 1);
		draw_sprite_ext(spr_IceCreams, 0, _x1+24, _y1+_button_height/2+5+5, 1, 1, 0, c_white, 1);
		type_formatted(_x1+48, _y1+_button_height/2+5, _format_mouseover+_text_string);
		if (TAP) {
			player_instance.path_position = 1;
		}
	}
	else {
		draw_sprite_stretched_ext(spr_Button, 0, _x1, _y1, _button_width, _button_height, c_white, 1);
		//draw_sprite_ext(spr_Cones, 0, _x1+25, _y1+_button_height/2, 1, 1, 0, c_white, 1);
		draw_sprite_ext(spr_IceCreams, 0, _x1+24, _y1+_button_height/2+5, 1, 1, 0, c_white, 1);
		type_formatted(_x1+48, _y1+_button_height/2, _format+_text_string);
	}
	
	
	var _text_string = "Sorbets";
	var _text_width = type_formatted(_w/2+200, _h-165, _format+_text_string, false).bbox_width + 20;
	//var _x1 = _w-_h_margin-2*_button_width-1*10;
	var _x1 = _w/2+470-_h_margin-2*_button_width-1*10;
	var _y1 = _h-_toolbar_top+20 + 1*50;
	
	if (point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x1+_button_width, _y1+_button_height)) {		
		draw_sprite_stretched_ext(spr_Button_Pressed, 0, _x1, _y1, _button_width, _button_height, c_white, 1);
		//draw_sprite_ext(spr_Cones, 0, _x1+25, _y1+_button_height/2+5, 1, 1, 0, c_white, 1);
		draw_sprite_ext(spr_IceCreams, 39, _x1+24, _y1+_button_height/2+5+5, 1, 1, 0, c_white, 1);
		type_formatted(_x1+48, _y1+_button_height/2+5, _format_mouseover+_text_string);
		if (TAP) {
			player_instance.path_position = 1;
		}
	}
	else {
		draw_sprite_stretched_ext(spr_Button, 0, _x1, _y1, _button_width, _button_height, c_white, 1);
		//draw_sprite_ext(spr_Cones, 0, _x1+25, _y1+_button_height/2, 1, 1, 0, c_white, 1);
		draw_sprite_ext(spr_IceCreams, 39, _x1+24, _y1+_button_height/2+5, 1, 1, 0, c_white, 1);
		type_formatted(_x1+48, _y1+_button_height/2, _format+_text_string);
	}
	
	var _text_string = "Popsicles";
	var _text_width = type_formatted(_w/2+200, _h-165, _format+_text_string, false).bbox_width + 20;
	//var _x1 = _w-_h_margin-2*_button_width-1*10;
	var _x1 = _w/2+470-_h_margin-2*_button_width-1*10;
	var _y1 = _h-_toolbar_top+20 + 2*50;
	
	if (point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x1+_button_width, _y1+_button_height)) {		
		draw_sprite_stretched_ext(spr_Button_Pressed, 0, _x1, _y1, _button_width, _button_height, c_white, 1);
		//draw_sprite_ext(spr_Cones, 0, _x1+25, _y1+_button_height/2+5, 1, 1, 0, c_white, 1);
		draw_sprite_ext(spr_IceCreams, 62, _x1+24, _y1+_button_height/2+5, 1, 1, 0, c_white, 1);
		type_formatted(_x1+48, _y1+_button_height/2+5, _format_mouseover+_text_string);
		if (TAP) {
			player_instance.path_position = 1;
		}
	}
	else {
		draw_sprite_stretched_ext(spr_Button, 0, _x1, _y1, _button_width, _button_height, c_white, 1);
		//draw_sprite_ext(spr_Cones, 0, _x1+25, _y1+_button_height/2, 1, 1, 0, c_white, 1);
		draw_sprite_ext(spr_IceCreams, 62, _x1+24, _y1+_button_height/2, 1, 1, 0, c_white, 1);
		type_formatted(_x1+48, _y1+_button_height/2, _format+_text_string);
	}
	
	//---
	
	var _text_string = "Cones";
	var _text_width = type_formatted(_w/2+200, _h-165, _format+_text_string, false).bbox_width + 20;
	//var _x1 = _w-_h_margin-1*_button_width-0*10;
	var _x1 = _w/2+470-_h_margin-1*_button_width-0*10;
	var _y1 = _h-_toolbar_top+20 + 0*50;
	
	if (point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x1+_button_width, _y1+_button_height)) {		
		draw_sprite_stretched_ext(spr_Button_Pressed, 0, _x1, _y1, _button_width, _button_height, c_white, 1);
		draw_sprite_ext(spr_Cones, 0, _x1+25, _y1+_button_height/2+5, 1, 1, 0, c_white, 1);
		//draw_sprite_ext(spr_IceCreams, 0, _x1+24, _y1+_button_height/2+5, 1, 1, 0, c_white, 1);
		type_formatted(_x1+48, _y1+_button_height/2+5, _format_mouseover+_text_string);
		if (TAP) {
			player_instance.path_position = 1;
		}
	}
	else {
		draw_sprite_stretched_ext(spr_Button, 0, _x1, _y1, _button_width, _button_height, c_white, 1);
		draw_sprite_ext(spr_Cones, 0, _x1+25, _y1+_button_height/2, 1, 1, 0, c_white, 1);
		//draw_sprite_ext(spr_IceCreams, 0, _x1+24, _y1+_button_height/2, 1, 1, 0, c_white, 1);
		type_formatted(_x1+48, _y1+_button_height/2, _format+_text_string);
	}
	
	
	var _text_string = "Toppings";
	var _text_width = type_formatted(_w/2+200, _h-165, _format+_text_string, false).bbox_width + 20;
	//var _x1 = _w-_h_margin-1*_button_width-0*10;
	var _x1 = _w/2+470-_h_margin-1*_button_width-0*10;
	var _y1 = _h-_toolbar_top+20 + 1*50;
	
	if (point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x1+_button_width, _y1+_button_height)) {		
		draw_sprite_stretched_ext(spr_Button_Pressed, 0, _x1, _y1, _button_width, _button_height, c_white, 1);
		//draw_sprite_ext(spr_Cones, 0, _x1+25, _y1+_button_height/2+5, 1, 1, 0, c_white, 1);
		draw_sprite_ext(spr_Toppings, 2, _x1+24-4, _y1+_button_height/2+5+18, 2, 2, 0, c_white, 1);
		type_formatted(_x1+48, _y1+_button_height/2+5, _format_mouseover+_text_string);
		if (TAP) {
			player_instance.path_position = 1;
		}
	}
	else {
		draw_sprite_stretched_ext(spr_Button, 0, _x1, _y1, _button_width, _button_height, c_white, 1);
		//draw_sprite_ext(spr_Cones, 0, _x1+25, _y1+_button_height/2, 1, 1, 0, c_white, 1);
		draw_sprite_ext(spr_Toppings, 2, _x1+24-4, _y1+_button_height/2+18, 2, 2, 0, c_white, 1);
		type_formatted(_x1+48, _y1+_button_height/2, _format+_text_string);
	}
	
	var _text_string = "Syrups";
	var _text_width = type_formatted(_w/2+200, _h-165, _format+_text_string, false).bbox_width + 20;
	//var _x1 = _w-_h_margin-1*_button_width-0*10;
	var _x1 = _w/2+470-_h_margin-1*_button_width-0*10;
	var _y1 = _h-_toolbar_top+20 + 2*50;
	
	if (point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x1+_button_width, _y1+_button_height)) {		
		draw_sprite_stretched_ext(spr_Button_Pressed, 0, _x1, _y1, _button_width, _button_height, c_white, 1);
		//draw_sprite_ext(spr_Cones, 0, _x1+25, _y1+_button_height/2+5, 1, 1, 0, c_white, 1);
		draw_sprite_ext(spr_Syrups, 0, _x1+24, _y1+_button_height/2+5-12, 1, 1, 0, c_white, 1);
		type_formatted(_x1+48, _y1+_button_height/2+5, _format_mouseover+_text_string);
		if (TAP) {
			player_instance.path_position = 1;
		}
	}
	else {
		draw_sprite_stretched_ext(spr_Button, 0, _x1, _y1, _button_width, _button_height, c_white, 1);
		//draw_sprite_ext(spr_Cones, 0, _x1+25, _y1+_button_height/2, 1, 1, 0, c_white, 1);
		draw_sprite_ext(spr_Syrups, 0, _x1+24, _y1+_button_height/2-12, 1, 1, 0, c_white, 1);
		type_formatted(_x1+48, _y1+_button_height/2, _format+_text_string);
	}
	
	// Serve button
	var _button_width = 250;
	var _text_string = "Serve!";
	var _format = "[fnt_GUI][scale,0.25][fa_center][fa_middle][c_white]";
	var _format_shadow = "[fnt_GUI][scale,0.25][fa_center][fa_middle][c_black]";
	var _format_mouseover = "[fnt_GUI][scale,0.25][fa_center][fa_middle][c_white]";	
	var _text_width = type_formatted(_w/2+200, _h-165, _format+_text_string, false).bbox_width + 20;
	//var _x1 = _w-_h_margin-1*_button_width-0*10;
	var _x1 = _w/2+470-_h_margin-_button_width;
	var _y1 = _h-_toolbar_top+20 + 3*50;
	
	if (point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x1+_button_width, _y1+_button_height)) {		
		draw_sprite_stretched_ext(spr_Button_Pressed, 1, _x1, _y1, _button_width, _button_height, c_white, 1);
		//draw_sprite_ext(spr_Cones, 0, _x1+25, _y1+_button_height/2+5, 1, 1, 0, c_white, 1);
		type_formatted(_x1+_button_width/2+1, _y1+_button_height/2+5+1, _format_shadow+_text_string);
		type_formatted(_x1+_button_width/2, _y1+_button_height/2+5, _format_mouseover+_text_string);
		if (TAP) {
			player_instance.path_position = 1;
		}
	}
	else {
		draw_sprite_stretched_ext(spr_Button, 1, _x1, _y1, _button_width, _button_height, c_white, 1);
		//draw_sprite_ext(spr_Cones, 0, _x1+25, _y1+_button_height/2, 1, 1, 0, c_white, 1);
		type_formatted(_x1+_button_width/2+1, _y1+_button_height/2+1, _format_shadow+_text_string);
		type_formatted(_x1+_button_width/2, _y1+_button_height/2, _format+_text_string);
	}
	
	
	
	// Character
	draw_set_alpha(1);
	var _offset_w = CHARACTER_SIZE/2;
	var _offset_h = 2*CHARACTER_SIZE;
	var _scale = 2*TILE_SIZE/CHARACTER_SIZE;
	var _x = _w/2-470+_h_margin;
	var _y = _h-230;
	draw_sprite_part_ext(player_instance.body_sprite, 0, 20*CHARACTER_SIZE, 0, CHARACTER_SIZE, 2*CHARACTER_SIZE, _x, _y, _scale, _scale, c_white, 1);
	draw_sprite_part_ext(player_instance.outfit_sprite, 0, 20*CHARACTER_SIZE, 0, CHARACTER_SIZE, 2*CHARACTER_SIZE, _x, _y, _scale, _scale, c_white, 1);
	draw_sprite_part_ext(player_instance.hairstyle_sprite, 0, 20*CHARACTER_SIZE, 0, CHARACTER_SIZE, 2*CHARACTER_SIZE, _x, _y, _scale, _scale, c_white, 1);
	if (sprite_exists(player_instance.accessory_sprite)) {
		draw_sprite_part_ext(player_instance.accessory_sprite, 0, 20*CHARACTER_SIZE, 0, CHARACTER_SIZE, 2*CHARACTER_SIZE, _x, _y, _scale, _scale, c_white, 1);
	}
	
	
	// Order text
	var _split = fnc_StringToList(player_instance.favorite_order.order_description, "|");
	var _nsplit = ds_list_size(_split);
	
	var _x = _w/2-470+_h_margin + CHARACTER_SIZE*_scale + 20;
	var _y = _h-_toolbar_top+30+(7-_nsplit)*10;
	
	var _text_scale  = window_get_fullscreen() ? 0.2 : 0.2;
	var _format = "[fnt_GUI][scale,"+string(_text_scale)+"][$4BCD73][fa_left][fa_middle]";
	
	var _text_scale  = window_get_fullscreen() ? 0.2 : 0.2;
	var _name = player_instance.name;
	var _line = type_formatted(_x, _y, _format+_name);
	
	var _format = "[fnt_GUI][scale,"+string(_text_scale)+"][c_white][fa_left][fa_middle]";
	var _ice_cream_order = player_instance.salutation+" "+player_instance.order_intro;
	var _line = type_formatted(_x, _y+1*_line.bbox_height, _format+_ice_cream_order);
	
	//var _line = type_formatted(_x, _y+2.4*_line.bbox_height, _format+player_instance.favorite_order.order_description);
	
	for (var _i=0; _i<_nsplit; _i++) {
		var _line = type_formatted(_x, _y+_line.bbox_height+(_i+1)*20, _format+_split[|_i]);	
	}
	
	
	// Ice cream tray
	var _tray_width = 130;
	draw_sprite_stretched_ext(spr_Button_Pressed, 2,		_w/2, _h-_toolbar_top+40, _tray_width, 160, c_white, 1);
	
	// Ice creams
	var _scale = 3;
	fnc_DrawOrder(player_instance.favorite_order, _w/2+_tray_width/2, _h-_toolbar_top+140, _scale);	
}


function fnc_DrawOrder(_order, _x, _y, _scale, _show_cone=true, _show_scoops=true, _show_topping=true, _show_syrup=true, _show_popsicle=true) {
	if (ENABLE_LIVE && live_call()) return live_result;
	/*
	show_debug_message("Inside Draw");
	show_debug_message("Order " + array_to_string(_order.order_icecream));
	show_debug_message("Order type " + array_to_string(_order.order_icecream_type));
	show_debug_message("Popsicle "+string(_order.order_popsicle));
	show_debug_message("Topping "+string(_order.order_topping));
	show_debug_message("Syrup "+string(_order.order_syrup));
	show_debug_message("Cone "+string(_order.order_cone));
	show_debug_message("Description "+string(_order.order_description));
	*/
	if (_order.order_popsicle == -1) { // Ice cream
		if (_show_cone) draw_sprite_ext(spr_Cones, Game.cone_types[_order.order_cone].index, _x+3, _y, _scale, _scale, 0, c_white, 1);
		
		if (_show_scoops) {
			var _n = array_length(_order.order_icecream_type);
			var _n_real = 0;
			for (var _i=0; _i<_n; _i++) {
				if (_order.order_icecream[_i] != -1) {
					_n_real++;
					if (_order.order_icecream_type[_i] == ORDER_ELEMENT_TYPES.GELATO) {
						draw_sprite_ext(spr_IceCreams, Game.gelato_types[_order.order_icecream[_i]].index, _x, _y-_i*5*_scale, _scale, _scale, 0, c_white, 1);
					}
					else {
						draw_sprite_ext(spr_IceCreams, Game.sorbet_types[_order.order_icecream[_i]].index, _x, _y-_i*5*_scale, _scale, _scale, 0, c_white, 1);
					}
				}
			}
		}
		
		if (_show_syrup && _order.order_syrup != -1) {			
			draw_sprite_ext(spr_Toppings, Game.syrup_types[_order.order_syrup].index,	_x, _y-(_n_real-1)*5*_scale, _scale, _scale, 0, c_white, 1);	
		}
		if (_show_topping && _order.order_topping != -1) {
			draw_sprite_ext(spr_Toppings, Game.topping_types[_order.order_topping].index,	_x, _y-(_n_real-1)*5*_scale, _scale, _scale, 0, c_white, 1);	
		}
	}
	else { // Popsicle
		if (_show_popsicle) draw_sprite_ext(spr_IceCreams, Game.popsicle_types[_order.order_popsicle].index, _x, _y, _scale, _scale, 0, c_white, 1);
		var _offset = sprite_get_number(spr_Toppings)/2;
		if (_show_syrup && _order.order_syrup != -1) {			
			draw_sprite_ext(spr_Toppings, _offset + Game.syrup_types[_order.order_syrup].index,	_x, _y, _scale, _scale, 0, c_white, 1);	
		}
		if (_show_topping && _order.order_topping != -1) {
			draw_sprite_ext(spr_Toppings, _offset + Game.topping_types[_order.order_topping].index,	_x, _y, _scale, _scale, 0, c_white, 1);	
		}
	}
}


function fnc_DrawPauseMenu() {
	var _w = GUI_WIDTH;
	var _h = GUI_HEIGHT;
	
	var _title_color = "[c_green]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	if (sprite_exists(pause_screenshot)) {
		draw_sprite_ext(pause_screenshot, 0, 0, 0, 1, 1, 0, c_white, 1);
	}
	draw_set_alpha(0.7);	
	draw_rectangle_color(0, 0, _w, _h, c_black, $111111, $121212, $222222, false);
	type_formatted(_w/2, 30, _title_color+"[fa_center][fnt_Menu]Game Paused");
	type_formatted(_w/2, 60, _main_text_color+"[fa_center][fnt_Menu]Press ESC to resume");
	
	
	fnc_Link(_w/2, _h-60, _link_color+"[fa_center][fa_middle]Return to Main Menu", _link_hover_color+"[fa_center][fa_middle][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);
}

function fnc_DrawYouLost() {
	var _w = GUI_WIDTH;
	var _h = GUI_HEIGHT;
	
	var _title_color = "[c_green]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	draw_set_alpha(0.7);
	draw_set_color(c_black);
	draw_rectangle(0, 0, _w, _h, false);
	
	audio_stop_all();
	type_formatted(_w/2, 40, _title_color+"[fa_center][fnt_Menu][scale,2]+");
	if (death_message_chosen == "") {
		death_message_chosen = choose("Requiescat in Pace", "You died", "Goodbye cruel world", "You bit the dust", "You met your maker", "You passed away", "Boom.");
	}
	type_formatted(_w/2, 80, _main_text_color+"[fa_center][fnt_Menu]"+death_message_chosen);
	type_formatted(_w/2, 110, _link_hover_color+"[fa_center][fnt_Menu]Score: "+string(Game.total_score));
	
	if (ENABLE_SCOREBOARD) {
		// Scoreboard
		if (!scoreboard_queried) {
			//http_call = "query_scoreboard";
			var _scoreboard_url = "https://www.biyectivo.com/misc/scoreboard/scoreboard.php?game="+scoreboard_game_id+"&limit=5";
			http_get_id_query = http_get(_scoreboard_url);
			scoreboard_queried = true;
			if (current_scoreboard_updates < max_scoreboard_updates) {				
				Game.alarm[2] = timer_scoreboard_updates;
				current_scoreboard_updates++;
			}
		}
		else {
			if (http_return_status_query == 200) {
				
							
				var _list = ds_map_find_value(scoreboard_html5, "default");
				var _n = ds_list_size(_list);
				for (var _i=0; _i<_n; _i++) {
					var _map = ds_list_find_value(_list, _i);
					//show_debug_message(string(_i)+": "+string(_map[? "username"])+" = "+string(_map[? "game_score"]));
					draw_set_color(c_white);
						
					if (_i==0) {
						var _total_high_score = _map[? "game_score"];
					}
					else if (_i==4) {
						var _number_5_score	= _map[? "game_score"]; 
					}
					
					type_formatted(80, 400+_i*50, "[fa_right][scale,0.6]#"+string(_i+1));
					type_formatted(120, 400+_i*50, "[fa_left][scale,0.6]"+_map[? "username"]);
					type_formatted(500, 400+_i*50, "[fa_right][scale,0.6]"+_map[? "game_score"]);
				}
				
				
				/*particle_type_highscore =	part_type_create();		
				part_type_scale(particle_type_highscore, 1, 1);
				part_type_size(particle_type_highscore, 0.25, 0.35, 0, 0);
				part_type_life(particle_type_highscore, 5, 15);			
				part_type_shape(particle_type_highscore, pt_shape_star);
				
				part_type_alpha2(particle_type_highscore, 0.8, 0.0);
				part_type_speed(particle_type_highscore, 2, 5, 0, 0);
				part_type_direction(particle_type_highscore, 0, 360, 0, 20);
				part_type_orientation(particle_type_highscore, -10, 10, 0, 0, false);
				part_type_blend(particle_type_highscore, true);
		
		
				// High score indication
				if (Game.total_score >= _total_high_score) {
					var _txt = "YOU BEAT THE WORLD'S HIGH SCORE!!!";
					var _msgW = string_width(_txt);
					var _msgH = string_height(_txt);
					draw_text_transformed(_w/2, 340, _txt, 0.4, 0.4, 0);
					
					var _color = $00ccff;
					part_type_color1(particle_type_highscore, _color);
		
					// Emit
					part_emitter_region(Game.particle_system, Game.particle_emitter, _w/2-_msgW/2, _w/2+_msgW/2, 340-_msgH/2, 340+_msgH/2, ps_shape_ellipse, ps_distr_gaussian);
					part_emitter_burst(Game.particle_system, Game.particle_emitter, particle_type_highscore, 40);	
				}
				else if (Game.total_score >= _number_5_score) {
					var _txt = "YOU REACHED THE WORLD's TOP 5!!!"
					var _msgW = string_width(_txt);
					var _msgH = string_height(_txt);
					draw_text_transformed(_w/2+2, 340, _txt, 0.4, 0.4, 0);
					
					var _color = c_silver;					
					part_type_color1(particle_type_highscore, _color);
		
					// Emit
					part_emitter_region(Game.particle_system, Game.particle_emitter, _w/2-_msgW/2, _w/2+_msgW/2, 340-_msgH/2, 340+_msgH/2, ps_shape_ellipse, ps_distr_gaussian);
					part_emitter_burst(Game.particle_system, Game.particle_emitter, particle_type_highscore, 40);	
				}
				*/
				
			}
			else if (http_return_status_query == noone) {
				//draw_text_transformed(_w/2, 400, "Loading scoreboard...", 0.7, 0.7, 0);
				type_formatted(_w/2, 400, "[fa_center][fa_middle][scale,0.7]Loading scoreboard...");
			}
			/*
			else  {
				draw_set_halign(fa_center);
				draw_text_transformed(_w/2, 400, "No/bad connection", 0.7, 0.7, 0);				
				draw_text_transformed(_w/2, 440, "Cannot show scoreboard", 0.7, 0.7, 0);				
			}*/
		}
	}
	
	fnc_Link(_w/2, _h-120, _link_color+"[fa_center][fa_middle]ENTER to restart", _link_hover_color+"[fa_center][fa_middle][$0BFFE6]ENTER to restart", fnc_TryAgain, 0);
	fnc_Link(_w/2, _h-60, _link_color+"[fa_center][fa_middle]Return to Main Menu", _link_hover_color+"[fa_center][fa_middle][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);
}

function fnc_DrawHUD() {
	fnc_BackupDrawParams();
	// Draw the HUD
	type_formatted(10, 10, "[c_blue][fa_left][fa_top]"+value_names[index_selected]+" = "+string(values[index_selected]));
	
	if (ENABLE_LIVE && live_call()) return live_result;
	
	// Right
	
	// Outline
	var _w = adjusted_window_width;
	var _h = adjusted_window_height;
				
	draw_set_alpha(0.6);
	
	var _x1 = _w-150;
	var _y1 = 30;
	var _x2 = _w+30;
	var _y2 = 120;
	var _radius = 16;
	var _color = make_color_rgb(114, 75, 44);
	draw_set_color(_color);
	draw_roundrect_ext(_x1, _y1, _x2, _y2, _radius, _radius, false);
	
	// Inner Border	
	
	var _x1 = _w-150+8;
	var _y1 = 30+8;
	var _x2 = _w+30-8;
	var _y2 = 120-8;
	var _radius = 16;
	var _color = make_color_rgb(69, 42, 27);
	var _color2 = make_color_rgb(127, 91, 62);
	draw_set_color(_color2);
	draw_rectangle(_x1, _y1, _x2, _y2, false);
	draw_set_color(_color);
	draw_rectangle(_x1, _y1, _x2, _y2, true);
	
	
	
	var _format = "[alpha,0.9][fnt_GUI][scale,0.2][c_white][fa_left][fa_top]";
	var _text = "[$4BCD73]$ [c_white]"+string(number_commas(bank));
	var _line = type_formatted(_x1+8, _y1+8, _format+_text);
	_text = "[$4BCD73]Energy";
	type_formatted(_x1+8, _y1+8+1*_line.bbox_height, _format+_text);
	_text = "[$4BCD73]Reputation";
	type_formatted(_x1+8, _y1+8+2*_line.bbox_height, _format+_text);
	
	// Left
	draw_set_alpha(0.6);
	
	var _x1 = -30;
	var _y1 = 30;
	var _x2 = 150;
	var _y2 = 120;
	var _radius = 16;
	var _color = make_color_rgb(114, 75, 44);
	draw_set_color(_color);
	draw_roundrect_ext(_x1, _y1, _x2, _y2, _radius, _radius, false);
	
	// Inner Border	
	
	var _x1 = -30+8;
	var _y1 = 30+8;
	var _x2 = 150-8;
	var _y2 = 120-8;
	var _radius = 16;
	var _color = make_color_rgb(69, 42, 27);
	var _color2 = make_color_rgb(127, 91, 62);
	draw_set_color(_color2);
	draw_rectangle(_x1, _y1, _x2, _y2, false);
	draw_set_color(_color);
	draw_rectangle(_x1, _y1, _x2, _y2, true);
	
	var _format = "[alpha,0.9][fnt_GUI][scale,0.2][c_white][fa_left][fa_top]";
	var _text = "[$4BCD73]Day: [c_white]"+string(day);
	var _line = type_formatted(_x1+30+8, _y1+8, _format+_text);
	
	var _hours = floor(time);
	var _mins = frac(time) * GAME_FPS;
	_text = "[$4BCD73]Time: [c_white]"+(_hours<10 ? "0" : "")+string(_hours)+":"+(_mins < 10 ? "0" : "")+string(_mins);
	type_formatted(_x1+30+8, _y1+8+1*_line.bbox_height, _format+_text);
	_text = "[$4BCD73]Season: [c_white]"+seasons[current_season];
	type_formatted(_x1+30+8, _y1+8+2*_line.bbox_height, _format+_text);
	
	
	
	draw_set_alpha(1);
	fnc_RestoreDrawParams();
}

#endregion

#region Utility/Other

function fnc_Link(_x, _y, _text, _text_mouseover, _callback, _param) {
	var _draw_data_normal = type_formatted(_x, _y, _text, false);
	//var _draw_data_mouseover = type_formatted(_x, _y, _text_mouseover, false);

	var _bbox_coords = _draw_data_normal.bbox(_x, _y);

	var _mouseover = GUI_MOUSE_X >= _bbox_coords[0] && GUI_MOUSE_Y >= _bbox_coords[1] && GUI_MOUSE_X <= _bbox_coords[2] && GUI_MOUSE_Y <= _bbox_coords[3];
	//var _mouseover = GUI_MOUSE_X >= _draw_data_normal.bbox_x1 && GUI_MOUSE_Y >= _draw_data_normal.bbox_y1 && GUI_MOUSE_X <= _draw_data_normal.bbox_x2 && GUI_MOUSE_Y <= _draw_data_normal.bbox_y2;
	var _click = device_mouse_check_button_pressed(0, mb_left);
	
	if (Game.debug) {
		draw_set_color(c_green);
		//draw_rectangle(_draw_data_normal.bbox_x1, _draw_data_normal.bbox_y1, _draw_data_normal.bbox_x2, _draw_data_normal.bbox_y2, false);
		draw_rectangle(_bbox_coords[0], _bbox_coords[1], _bbox_coords[2], _bbox_coords[3], false);
	}
	
	if (_click && _mouseover) {
		type_formatted(_x, _y, _text_mouseover);
		if (_callback != noone) {
			//script_execute(_callback, _param);
			_callback(_param);
		}
	}
	else if (_mouseover) {
		type_formatted(_x, _y, _text_mouseover);
	}
	else {
		type_formatted(_x, _y, _text);
	}

}

function fnc_ExecuteMenu(_param) {
	var _function = (asset_get_index("fnc_Menu_"+string(_param)));
	_function();
}

function fnc_ExecuteOptions(_param) {
	var _function = script_execute(asset_get_index("fnc_Options_"+string(_param)));
	_function();
}

function fnc_TryAgain() {
	paused = false;
	room_restart();	
}
	
#endregion

#region Debug

function fnc_DrawDebug() {
	if (Game.debug) {
		draw_set_color(c_black);
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);	
		draw_set_font(fnt_Debug);
		
		draw_set_color(c_white);
		
		var _spacing = 50;
		
		var _debug_data = [];
		
		array_push(_debug_data, "Room: "+string(room_width)+"x"+string(room_height)+" ("+string(round(room_width/room_height * 100)/100)+")");
		array_push(_debug_data, "Room speed: "+string(room_speed)+" Game speed: "+string(gamespeed_fps));
		array_push(_debug_data, "Requested scaling type: "+string(SELECTED_SCALING)+" ("+
						(SELECTED_SCALING==SCALING_TYPE.RESOLUTION_ADAPTED_TO_WINDOW ? "Normal mode (resolution scaled to window)" : 
						(SELECTED_SCALING==SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW ? "Resolution independent of window, unscaled" : "Resolution independent of of window, scaled")+")"));
		array_push(_debug_data, "Requested base resolution: "+string(BASE_RESOLUTION_W)+"x"+string(BASE_RESOLUTION_H));
		array_push(_debug_data, "Actual base resolution: "+string(adjusted_resolution_width)+"x"+string(adjusted_resolution_height));
		array_push(_debug_data, "Requested window size: "+string(BASE_WINDOW_SIZE_W)+"x"+string(BASE_WINDOW_SIZE_H));
		array_push(_debug_data, "Actual window size: "+string(adjusted_window_width)+"x"+string(adjusted_window_height));
		array_push(_debug_data, "App Surface: "+string(surface_get_width(application_surface))+"x"+string(surface_get_height(application_surface))+" ("+string(round(surface_get_width(application_surface)/surface_get_height(application_surface) * 100)/100)+")");
		array_push(_debug_data, "Window: "+string(window_get_width())+"x"+string(window_get_height())+" ("+string(round(window_get_width()/window_get_height() * 100)/100)+")");
		array_push(_debug_data, "Display: "+string(display_get_width())+"x"+string(display_get_height())+" ("+string(round(display_get_width()/display_get_height() * 100)/100)+")");
		array_push(_debug_data, "Camera: "+string(camera_get_view_x(VIEW))+","+string(camera_get_view_y(VIEW)));
		array_push(_debug_data, "GUI Layer (with GM function): "+string(display_get_gui_width())+"x"+string(display_get_gui_height())+" ("+string(round(display_get_gui_width()/display_get_gui_height() * 100)/100)+")");
		array_push(_debug_data, "GUI Layer (with helper functions!): "+string(GUI_WIDTH)+"x"+string(GUI_HEIGHT)+" ("+string(round(GUI_WIDTH/GUI_HEIGHT * 100)/100)+")");
		array_push(_debug_data, "Mouse: "+string(mouse_x)+","+string(mouse_y));
		array_push(_debug_data, "Device mouse 0: "+string(GUI_MOUSE_X)+","+string(GUI_MOUSE_Y));
		array_push(_debug_data, "Device mouse 0 GUI: "+string(device_mouse_x_to_gui(0))+","+string(device_mouse_y_to_gui(0)));
		array_push(_debug_data, "Paused: "+string(paused));
		array_push(_debug_data, "FPS: "+string(fps_real) + "/" + string(fps));
		array_push(_debug_data, "GMLive enabled: "+string(ENABLE_LIVE));
		
		
		var _n = array_length(_debug_data);
		
		draw_text(20, 20,	"DEBUG MODE [TAB TO TOGGLE INFO / G TO TOGGLE PATHFINIDNG VIZ]");
		if (keyboard_check_pressed(vk_tab)) {
			show_debug_info = !show_debug_info;
		}
		if (show_debug_info) {
			if (keyboard_check_pressed(ord("G"))) {
				show_debug_pathfinding = !show_debug_pathfinding;
			}
			draw_set_alpha(0.5);
			draw_rectangle_color(0, 0, adjusted_window_width, _spacing+(_n+1)*15, c_black, c_black, c_black, c_black, false);
			draw_set_alpha(1);		
			if (instance_exists(Game.player_instance)) {
				draw_text(20, 35, "Player: "+string(Game.player_instance.x)+","+string(Game.player_instance.y)+" / State="+obj_Player.fsm.current_state_name);
			}
			else {
				draw_text(20, 35, "Player not in room");	
			}		
		
			for (var _i=0; _i<_n; _i++) {
				draw_text(20, _spacing+_i*15, _debug_data[_i]);
				//show_debug_message(_debug_data[_i]);
			}
		}
	}
}

#endregion
