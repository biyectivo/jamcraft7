//
// Brightness/Contrast/Saturation/Gamma shader
// Based on Romain Dura's code
// 
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float brightness;
uniform float contrast;
uniform float saturation;
uniform float gamma;
uniform float monochrome;

vec3 fnc_BriConSat(vec3 _color, float _brightness, float _contrast, float _saturation) {
	// NTSC:
	const vec3 luminance_coefficients = vec3(0.299, 0.587, 0.114);
	// Romain's values:
	//const vec3 LuminanceCoefficient = vec3(0.2125, 0.7154, 0.0721);
	
	// Push colors up from black
	vec3 brightness_color = _color * _brightness;
	// Convert color to grayscale
	vec3 intensity_color = vec3(dot(brightness_color, luminance_coefficients));
	
	vec3 contrast_color;
	if (monochrome < 0.5) {
		// Manage saturation
		vec3 saturation_color = mix(intensity_color, brightness_color, _saturation);
		// Manage contrast
		contrast_color = mix(vec3(0.5), saturation_color, _contrast);
	}
	else {
		// Skip saturation and manage contrast
		contrast_color = mix(vec3(0.5), intensity_color, _contrast);
	}
	// Return the color tuned by brightness, contrast and saturation
	return contrast_color;
}

void main() {
    vec4 base_col = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	vec3 out_color = fnc_BriConSat(base_col.rgb, brightness, contrast, saturation);	
	out_color = pow(out_color.rgb, vec3(1.0/gamma));
	gl_FragColor = vec4(out_color, base_col.a);
}
